#!/usr/bin/env bash

ENABLE_CHECKS=\
cppcoreguidelines-*,\
clang-analyzer-*,\
performance-*,\
readability-*,\
portability-*,\
modernize-*,\
bugprone-*,\
hicpp-*,\
cert-*,\
misc-*,\

DISABLE_CORE_CHECKS=\
-cppcoreguidelines-pro-bounds-array-to-pointer-decay,\
-cppcoreguidelines-pro-bounds-constant-array-index,\
-cppcoreguidelines-pro-bounds-pointer-arithmetic,\
-cppcoreguidelines-pro-type-reinterpret-cast,\
-cppcoreguidelines-pro-type-union-access,\
-cppcoreguidelines-owning-memory,\
-cppcoreguidelines-avoid-magic-numbers,\
-cppcoreguidelines-pro-type-vararg,\

DISABLE_HI_CHECKS=\
-hicpp-signed-bitwise,\
-hicpp-no-array-decay,\
-hicpp-braces-around-statements,\
-hicpp-uppercase-literal-suffix,\

DISABLE_MISC_CHECKS=\
-readability-implicit-bool-conversion,\
-readability-braces-around-statements,\
-bugprone-too-small-loop-variable,\
-modernize-redundant-void-arg,\

echo 'Running clang-tidy...'

/usr/share/clang/run-clang-tidy.py -p . -checks=${ENABLE_CHECKS}${DISABLE_CORE_CHECKS}${DISABLE_HI_CHECKS}${DISABLE_MISC_CHECKS}

echo 'Finished clang-tidy'
