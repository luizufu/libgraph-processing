#pragma once

#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/magic.hxx>
#include <cstdint>


namespace stg
{

struct edge_list_header
{
    const uint32_t magic = EDGE_LIST_MAGIC;
    graph_type type;
    uint32_t n_vertices;
    uint32_t n_edges;
};

struct adj_list_header
{
    const uint32_t magic = ADJ_LIST_MAGIC;
    graph_type type;
    uint32_t n_vertices;
    uint32_t n_edges;
    direction_t direction;
};

struct edge_grid_header
{
    const uint32_t magic = EDGE_GRID_MAGIC;
    graph_type type;
    uint32_t n_vertices;
    uint32_t n_edges;
    uint32_t grid_size;
    uint32_t max;
};

} /* stg */
