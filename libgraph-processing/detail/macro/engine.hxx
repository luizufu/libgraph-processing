#pragma once

#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/disk-dynamic-adj-list.hxx>
#include <libgraph-processing/disk-dynamic-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#define INSTATIATE_ENGINE_TEMPLATES(engine) \
    template class engine<disk_edge_list<edge>>; \
    template class engine<disk_edge_list<weighted_edge>>; \
    template class engine<disk_adj_list<edge>>; \
    template class engine<disk_adj_list<weighted_edge>>; \
    template class engine<disk_edge_grid<edge>>; \
    template class engine<disk_edge_grid<weighted_edge>>; \
    template class engine<disk_dynamic_adj_list<edge>>; \
    template class engine<disk_dynamic_adj_list<weighted_edge>>; \
    template class engine<disk_dynamic_edge_grid<edge>>; \
    template class engine<disk_dynamic_edge_grid<weighted_edge>>;

#define INSTATIATE_ENGINE_TEMPLATES_EXTERN(engine) \
    extern template class stg::engine<stg::disk_edge_list<stg::edge>>; \
    extern template class stg::engine<stg::disk_edge_list<stg::weighted_edge>>; \
    extern template class stg::engine<stg::disk_adj_list<stg::edge>>; \
    extern template class stg::engine<stg::disk_adj_list<stg::weighted_edge>>; \
    extern template class stg::engine<stg::disk_edge_grid<stg::edge>>; \
    extern template class stg::engine<stg::disk_edge_grid<stg::weighted_edge>>; \
    extern template class stg::engine<stg::disk_dynamic_adj_list<stg::edge>>; \
    extern template class stg::engine<stg::disk_dynamic_adj_list<stg::weighted_edge>>; \
    extern template class stg::engine<stg::disk_dynamic_edge_grid<stg::edge>>; \
    extern template class stg::engine<stg::disk_dynamic_edge_grid<stg::weighted_edge>>;
