#pragma once

#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/super-block.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>

#define INSTATIATE_LV0_STRUCT_TEMPLATES(struct) \
    template class struct<uint32_t, neighbor, \
        disk_block_allocator>; \
    template class struct<uint32_t, weighted_neighbor, \
        disk_block_allocator>; \
    template class struct<uint32_t, edge, \
        disk_block_allocator>; \
    template class struct<uint32_t, weighted_edge, \
        disk_block_allocator>;

#define INSTATIATE_LV0_STRUCT_TEMPLATES_EXTERN(struct) \
    extern template class stg::detail::struct<uint32_t, stg::neighbor, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<uint32_t, stg::weighted_neighbor, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<uint32_t, stg::edge, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<uint32_t, stg::weighted_edge, \
        stg::detail::disk_block_allocator>;
