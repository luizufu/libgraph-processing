#pragma once

#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/disk-dynamic-adj-list.hxx>
#include <libgraph-processing/disk-dynamic-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#define INSTATIATE_ALGO_TEMPLATES(algo) \
    template class algo<disk_edge_list<edge>>; \
    template class algo<disk_edge_list<weighted_edge>>; \
    template class algo<disk_adj_list<edge>>; \
    template class algo<disk_adj_list<weighted_edge>>; \
    template class algo<disk_edge_grid<edge>>; \
    template class algo<disk_edge_grid<weighted_edge>>; \
    template class algo<disk_dynamic_adj_list<edge>>; \
    template class algo<disk_dynamic_adj_list<weighted_edge>>; \
    template class algo<disk_dynamic_edge_grid<edge>>; \
    template class algo<disk_dynamic_edge_grid<weighted_edge>>;

#define INSTATIATE_ALGO_TEMPLATES_EXTERN(algo) \
    extern template class stg::algo<stg::disk_edge_list<stg::edge>>; \
    extern template class stg::algo<stg::disk_edge_list<stg::weighted_edge>>; \
    extern template class stg::algo<stg::disk_adj_list<stg::edge>>; \
    extern template class stg::algo<stg::disk_adj_list<stg::weighted_edge>>; \
    extern template class stg::algo<stg::disk_edge_grid<stg::edge>>; \
    extern template class stg::algo<stg::disk_edge_grid<stg::weighted_edge>>; \
    extern template class stg::algo<stg::disk_dynamic_adj_list<stg::edge>>; \
    extern template class stg::algo<stg::disk_dynamic_adj_list<stg::weighted_edge>>; \
    extern template class stg::algo<stg::disk_dynamic_edge_grid<stg::edge>>; \
    extern template class stg::algo<stg::disk_dynamic_edge_grid<stg::weighted_edge>>;
