#pragma once

#include <libgraph-processing/edge.hxx>

#define INSTATIATE_STATIC_STRUCT_TEMPLATES(struct) \
    template class struct<edge>; \
    template class struct<weighted_edge>; \
    template class struct<neighbor>; \
    template class struct<weighted_neighbor>; \
    template class struct<off_t>;

#define INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(struct) \
    extern template class stg::detail::struct<stg::edge>; \
    extern template class stg::detail::struct<stg::weighted_edge>; \
    extern template class stg::detail::struct<stg::neighbor>; \
    extern template class stg::detail::struct<stg::weighted_neighbor>; \
    extern template class stg::detail::struct<off_t>;
