
#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/disk-dynamic-adj-list.hxx>
#include <libgraph-processing/disk-dynamic-edge-grid.hxx>
#include <libgraph-processing/edge.hxx>

#define INSTATIATE_ALGO_FUNC_TEMPLATES(res, func, ...) \
    template res func<disk_edge_list<edge>>( \
            const disk_edge_list<edge>&, __VA_ARGS__); \
    template res func<disk_edge_list<weighted_edge>>( \
            const disk_edge_list<weighted_edge>&, __VA_ARGS__); \
    template res func<disk_adj_list<edge>>( \
            const disk_adj_list<edge>&, __VA_ARGS__); \
    template res func<disk_adj_list<weighted_edge>>( \
            const disk_adj_list<weighted_edge>&, __VA_ARGS__); \
    template res func<disk_edge_grid<edge>>( \
            const disk_edge_grid<edge>&, __VA_ARGS__); \
    template res func<disk_edge_grid<weighted_edge>>( \
            const disk_edge_grid<weighted_edge>&, __VA_ARGS__); \
    template res func<disk_dynamic_adj_list<edge>>( \
            const disk_dynamic_adj_list<edge>&, __VA_ARGS__); \
    template res func<disk_dynamic_adj_list<weighted_edge>>( \
            const disk_dynamic_adj_list<weighted_edge>&, __VA_ARGS__); \
    template res func<disk_dynamic_edge_grid<edge>>( \
            const disk_dynamic_edge_grid<edge>&, __VA_ARGS__); \
    template res func<disk_dynamic_edge_grid<weighted_edge>>( \
            const disk_dynamic_edge_grid<weighted_edge>&, __VA_ARGS__);


#define INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(res, func, ...) \
    extern template res stg::func<stg::disk_edge_list<stg::edge>>( \
            const stg::disk_edge_list<stg::edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_edge_list<stg::weighted_edge>>( \
            const stg::disk_edge_list<stg::weighted_edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_adj_list<stg::edge>>( \
            const stg::disk_adj_list<stg::edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_adj_list<stg::weighted_edge>>( \
            const stg::disk_adj_list<stg::weighted_edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_edge_grid<stg::edge>>( \
            const stg::disk_edge_grid<stg::edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_edge_grid<stg::weighted_edge>>( \
            const stg::disk_edge_grid<stg::weighted_edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_dynamic_adj_list<stg::edge>>( \
            const stg::disk_dynamic_adj_list<stg::edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_dynamic_adj_list<stg::weighted_edge>>( \
            const stg::disk_dynamic_adj_list<stg::weighted_edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_dynamic_edge_grid<stg::edge>>( \
            const stg::disk_dynamic_edge_grid<stg::edge>&, __VA_ARGS__); \
    extern template res stg::func<stg::disk_dynamic_edge_grid<stg::weighted_edge>>( \
            const stg::disk_dynamic_edge_grid<stg::weighted_edge>&, __VA_ARGS__);
