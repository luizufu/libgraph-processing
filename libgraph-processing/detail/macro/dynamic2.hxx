#pragma once

#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>

#define INSTATIATE_LV2_STRUCT_TEMPLATES(struct) \
    template class struct<neighbor, \
        disk_block_allocator>; \
    template class struct<weighted_neighbor, \
        disk_block_allocator>; \
    template class struct<edge, \
        disk_block_allocator>; \
    template class struct<weighted_edge, \
        disk_block_allocator>;


#define INSTATIATE_LV2_STRUCT_TEMPLATES_EXTERN(struct) \
    extern template class stg::detail::struct<stg::neighbor, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<stg::weighted_neighbor, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<stg::edge, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<stg::weighted_edge, \
        stg::detail::disk_block_allocator>;
