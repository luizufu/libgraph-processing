#pragma once

#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/super-block.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>
#include <libgraph-processing/detail/dynamic/key-node.hxx>

#define INSTATIATE_LV1_STRUCT_TEMPLATES(struct) \
    template class struct<uint32_t, key_node, \
        disk_block_allocator>; \
    template class struct<uint32_t, unordered_key_node, \
        disk_block_allocator>; \
    template class struct<uint32_t, edge, \
        disk_block_allocator>; \
    template class struct<uint32_t, neighbor, \
        disk_block_allocator>;

#define INSTATIATE_LV1_STRUCT_TEMPLATES_EXTERN(struct) \
    extern template class stg::detail::struct<uint32_t, stg::detail::key_node, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<uint32_t, stg::detail::unordered_key_node, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<uint32_t, stg::edge, \
        stg::detail::disk_block_allocator>; \
    extern template class stg::detail::struct<uint32_t, stg::neighbor, \
        stg::detail::disk_block_allocator>;
