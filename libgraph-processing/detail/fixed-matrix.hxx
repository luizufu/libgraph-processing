#pragma once

#include <vector>


namespace stg::detail
{

template <typename T>
class fixed_matrix
{
    std::vector<T> _container;
    size_t _n_rows;

public:

    fixed_matrix(size_t n, size_t m)
        : _container(n * m)
        , _n_rows(n)
    {
    }

    fixed_matrix(size_t n, size_t m, T default_value)
        : _container(n * m, default_value)
        , _n_rows(n)
    {
    }

    T read(size_t i, size_t j) const
    {
        return _container[i * _n_rows + j];
    }

    void write(size_t i, size_t j, T val)
    {
        _container[i * _n_rows + j] = val;
    }

    size_t n_elements() const
    {
        return _container.size();
    }

    size_t n_rows() const
    {
        return _n_rows;
    }

    size_t n_colums() const
    {
        return _container.size() / _n_rows;
    }
};

} /* stg */
