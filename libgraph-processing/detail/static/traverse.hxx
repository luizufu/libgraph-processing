#pragma once

#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/zlib_source.hxx>

#include <cppcoro/generator.hpp>
#include <type_traits>
#include <sys/types.h>

namespace stg::detail
{


template <typename T>
struct default_traverse_filter
{
    bool operator() (T) { return true; }
};

template <typename T>
struct default_traverse_mapper
{
    T operator() (T t) { return t; }
};

template <typename T,
    typename Filter = default_traverse_filter<T>,
    typename Map= default_traverse_mapper<T>>
auto traverse(int fd, off_t begin, const graph_props& props,
        Filter f = default_traverse_filter<T>(),
        Map m = default_traverse_mapper<T>())
    -> cppcoro::generator<std::invoke_result_t<Map, T>>
{
    T t;

    auto pos_before = lseek(fd, 0, SEEK_CUR);

    if(props.compressed)
    {
        zlib_source<T> source(fd, begin);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
    else
    {
        source<T> source(fd, begin);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }

    lseek(fd, pos_before, SEEK_SET);
}

template <typename T,
    typename Filter = default_traverse_filter<T>,
    typename Map= default_traverse_mapper<T>>
auto traverse(std::istream& stream, std::streampos begin, const graph_props& props,
        Filter f = default_traverse_filter<T>(),
        Map m = default_traverse_mapper<T>())
    -> cppcoro::generator<std::invoke_result_t<Map, T>>
{
    T t;

    if(props.compressed)
    {
        zlib_stream_source<T> source(stream, begin);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
    else
    {
        stream_source<T> source(stream, begin);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
}


template <typename T,
    typename Filter = default_traverse_filter<T>,
    typename Map= default_traverse_mapper<T>>
auto traverse(int fd, off_t begin, off_t end, const graph_props& props,
        Filter f = default_traverse_filter<T>(),
        Map m = default_traverse_mapper<T>())
    -> cppcoro::generator<std::invoke_result_t<Map, T>>
{
    T t;

    auto pos_before = lseek(fd, 0, SEEK_CUR);

    if(props.compressed)
    {
        zlib_source<T> source(fd, begin, end);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
    else
    {
        source<T> source(fd, begin, end);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }

    lseek(fd, pos_before, SEEK_SET);
}

template <typename T,
    typename Filter = default_traverse_filter<T>,
    typename Map= default_traverse_mapper<T>>
auto traverse(std::istream& stream, std::streampos begin, std::streampos end, const graph_props& props,
        Filter f = default_traverse_filter<T>(),
        Map m = default_traverse_mapper<T>())
    -> cppcoro::generator<std::invoke_result_t<Map, T>>
{
    T t;

    if(props.compressed)
    {
        zlib_stream_source<T> source(stream, begin, end);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
    else
    {
        stream_source<T> source(stream, begin, end);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
}



template <typename T,
    typename Filter = default_traverse_filter<T>,
    typename Map= default_traverse_mapper<T>>
auto traverse(char* begin, char* end, const graph_props& props,
        Filter f = default_traverse_filter<T>(),
        Map m = default_traverse_mapper<T>())
    -> cppcoro::generator<std::invoke_result_t<Map, T>>
{
    T t;

    if(props.compressed)
    {
        zlib_pointer_source<T> source(begin, end);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
    else
    {
        pointer_source<T> source(begin, end);
        while(source.read(t))
            if(f(t))
                co_yield m(t);
    }
}


} /* stg */
