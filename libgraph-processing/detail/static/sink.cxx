#include <libgraph-processing/detail/static/sink.hxx>

#include <unistd.h>

namespace stg::detail
{

// fd sink
template <typename T>
sink<T>::sink(int fd, off_t begin, off_t end)
    : _fd(fd)
    , _end(end)
    , _total_written(0)
{
    lseek(_fd, begin, SEEK_SET);
}

template <typename T>
void sink<T>::reset(off_t begin, off_t end)
{
    _end = end;
    _total_written = 0;
    lseek(_fd, begin, SEEK_SET);
}

template <typename T>
bool sink<T>::write(const T& e)
{
    if(lseek(_fd, 0, SEEK_CUR) < _end)
    {
        _total_written += ::write(
            _fd, reinterpret_cast<const char*>(&e), sizeof(e));

        return true;
    }

    return false;
}

template <typename T>
void sink<T>::flush()
{
}

template <typename T>
size_t sink<T>::total_written() const
{
    return _total_written;
}

// stream sink
template <typename T>
stream_sink<T>::stream_sink(std::ostream& stream,
        off_t begin, off_t end)
    : _stream(stream)
    , _end(end)
    , _total_written(0)
{
    _stream.seekp(begin, std::ios::beg);
}

template <typename T>
void stream_sink<T>::reset(off_t begin, off_t end)
{
    _end = end;
    _total_written = 0;
    _stream.seekp(begin, std::ios::beg);
}

template <typename T>
bool stream_sink<T>::write(const T& e)
{
    if(_stream.operator bool() && _stream.tellp() < _end)
    {
        _stream.write(reinterpret_cast<const char*>(&e), sizeof(e));
        _total_written += sizeof(e);
        return true;
    }

    return false;
}

template <typename T>
void stream_sink<T>::flush()
{
}

template <typename T>
size_t stream_sink<T>::total_written() const
{
    return _total_written;
}


// pointer sink
template <typename T>
pointer_sink<T>::pointer_sink(char* begin, char* end)
    : _pointer(begin)
    , _end(end)
    , _total_written(0)
{
}

template <typename T>
void pointer_sink<T>::reset(char* begin, char* end)
{
    _pointer = begin;
    _end = end;
    _total_written = 0;
}

template <typename T>
bool pointer_sink<T>::write(const T& e)
{
    if(_pointer != _end)
    {
        size_t n = sizeof(e);
        const char* addr = reinterpret_cast<const char*>(&e);
        _pointer = std::copy_n(addr, n, _pointer);
        _total_written += n;

        return true;
    }

    return false;
}

template <typename T>
void pointer_sink<T>::flush()
{
}


template <typename T>
size_t pointer_sink<T>::total_written() const
{
    return _total_written;
}

INSTATIATE_STATIC_STRUCT_TEMPLATES(sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES(stream_sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES(pointer_sink)

} /* stg */


