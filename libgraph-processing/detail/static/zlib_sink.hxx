#pragma once

#include <libgraph-processing/detail/macro/static.hxx>
#include <ostream>
#include <limits>
#include <vector>
#include <zlib.h>
#include <sys/types.h>

namespace stg::detail
{

template <typename T>
class zlib_sink
{
    int _fd;
    off_t _end;

    z_stream _z_stream;

    int _compression_level;
    std::vector<char> _in_buffer;
    std::vector<char> _out_buffer;

    size_t _in_i;
    size_t _total_written;

    void zlib_init();
    void zlib_end();
    bool try_flush_to_stream(int flush);

public:

    zlib_sink(int fd,
        int compression_level,
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max(),
        size_t buffer_size = 1 << 16);

    void reset(
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max());

    bool write(const T& e);
    void flush();
    size_t total_written() const;

    bool has_remaining() const;
    void get_remaining(const T* edges, size_t& n) const;
};

template <typename T>
class zlib_stream_sink
{
    std::ostream& _stream;
    off_t _end;

    z_stream _z_stream;

    int _compression_level;
    std::vector<char> _in_buffer;
    std::vector<char> _out_buffer;

    size_t _in_i;
    size_t _total_written;

    void zlib_init();
    void zlib_end();
    bool try_flush_to_stream(int flush);

public:

    zlib_stream_sink(std::ostream& stream,
        int compression_level,
        off_t begin = 0,
        off_t end = std::numeric_limits<std::streamoff>::max(),
        size_t buffer_size = 1 << 16);

    void reset(off_t begin = 0,
        off_t end = std::numeric_limits<std::streamoff>::max());

    bool write(const T& e);
    void flush();
    size_t total_written() const;

    bool has_remaining() const;
    void get_remaining(const T* edges, size_t& n) const;
};

template <typename T>
class zlib_pointer_sink
{
    char* _pointer;
    char* _end;

    z_stream _z_stream;

    int _compression_level;
    std::vector<char> _in_buffer;
    std::vector<char> _out_buffer;

    size_t _in_i;
    size_t _total_written;

    void zlib_init();
    void zlib_end();
    bool try_flush_to_pointer(int flush);

public:

    zlib_pointer_sink(
        int compression_level,
        char* begin, char* end,
        size_t buffer_size = 1 << 16);

    void reset(char* begin, char* end);

    bool write(const T& e);
    void flush();
    size_t total_written() const;

    bool has_remaining() const;
    void get_remaining(const T* edges, size_t& n) const;
};


} /* stg */

INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(zlib_sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(zlib_stream_sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(zlib_pointer_sink)
