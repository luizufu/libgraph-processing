#include <libgraph-processing/detail/static/zlib_source.hxx>

#include <unistd.h>

namespace stg::detail
{

// source
template <typename T>
zlib_source<T>::zlib_source(int fd,
        off_t begin, off_t end,
        size_t buffer_size)
    : _fd(fd)
    , _end(end)
    , _z_stream({})
    , _in_buffer(buffer_size)
    , _out_buffer(buffer_size)
    , _out_i(0)
    , _total_read(0)
    , _size_to_write(0)
{

    lseek(_fd, begin, SEEK_SET);
    zlib_init();
}

template <typename T>
void zlib_source<T>::reset(off_t begin, off_t end)
{
    zlib_end();

    lseek(_fd, begin, SEEK_SET);
    _end = end;
    _out_i = 0;
    _total_read = 0;

    zlib_init();
}

template <typename T>
bool zlib_source<T>::read(T& e)
{
    size_t n = sizeof(e);

    if(_out_i + n <= _size_to_write)
    {
        std::copy_n(&_out_buffer[_out_i],
            n, reinterpret_cast<char*>(&e));
        _out_i += n;

        if(_out_i >= _size_to_write || (_out_buffer.size() - _out_i) < n)
            try_flush_from_stream();

        return true;
    }

    return false;
}

template <typename T>
size_t zlib_source<T>::total_read() const
{
    return _total_read;
}

template <typename T>
void zlib_source<T>::zlib_init()
{
    _z_stream.zalloc = Z_NULL;
    _z_stream.zfree = Z_NULL;
    _z_stream.opaque = Z_NULL;
    _z_stream.avail_in = 0;
    _z_stream.next_in = Z_NULL;

    assert(inflateInit(&_z_stream) == Z_OK);

    try_flush_from_stream();
}

template <typename T>
void zlib_source<T>::zlib_end()
{
    inflateEnd(&_z_stream);
}


template <typename T>
bool zlib_source<T>::try_flush_from_stream()
{
    if(lseek(_fd, 0, SEEK_CUR) < _end)
    {
        size_t to_read = std::min(
            (size_t) (_end - lseek(_fd, 0, SEEK_CUR)),
            _in_buffer.size());

        size_t n = ::read(_fd, &_in_buffer[0], to_read);

        if(n > 0)
        {
            _z_stream.avail_in = n;
            _z_stream.next_in = (Bytef*) _in_buffer.data();

            size_t last_total_in = _z_stream.total_in;
            size_t size_read = 0;

            do {

                _z_stream.avail_out = _out_buffer.size();
                _z_stream.next_out = (Bytef*) _out_buffer.data();

                assert(inflate(&_z_stream, Z_NO_FLUSH) != Z_STREAM_ERROR);
                _size_to_write = _out_buffer.size() - _z_stream.avail_out;
                size_read += _z_stream.total_in - last_total_in;

                last_total_in = _z_stream.total_in;

            } while(_z_stream.avail_out == 0);

            _out_i = 0;


            if(size_read >= n)
                return true;

            for(size_t i = size_read; i < n; ++i)
                _in_buffer[i - size_read] = _in_buffer[i];

            return false;
        }
    }

    return false;
}


// stream source
template <typename T>
zlib_stream_source<T>::zlib_stream_source(std::istream& stream,
        off_t begin, off_t end,
        size_t buffer_size)
    : _stream(stream)
    , _end(end)
    , _z_stream({})
    , _in_buffer(buffer_size)
    , _out_buffer(buffer_size)
    , _out_i(0)
    , _total_read(0)
    , _size_to_write(0)
{

    stream.seekg(begin, std::ios::beg);
    zlib_init();
}

template <typename T>
void zlib_stream_source<T>::reset(off_t begin, off_t end)
{
    zlib_end();

    _stream.seekg(begin, std::ios::beg);
    _end = end;
    _out_i = 0;
    _total_read = 0;

    zlib_init();
}

template <typename T>
bool zlib_stream_source<T>::read(T& e)
{
    size_t n = sizeof(e);

    if(_out_i + n <= _size_to_write)
    {
        std::copy_n(&_out_buffer[_out_i],
            n, reinterpret_cast<char*>(&e));
        _out_i += n;

        if(_out_i >= _size_to_write || (_out_buffer.size() - _out_i) < n)
            try_flush_from_stream();

        return true;
    }

    return false;
}

template <typename T>
size_t zlib_stream_source<T>::total_read() const
{
    return _total_read;
}

template <typename T>
void zlib_stream_source<T>::zlib_init()
{
    _z_stream.zalloc = Z_NULL;
    _z_stream.zfree = Z_NULL;
    _z_stream.opaque = Z_NULL;
    _z_stream.avail_in = 0;
    _z_stream.next_in = Z_NULL;

    assert(inflateInit(&_z_stream) == Z_OK);

    try_flush_from_stream();
}

template <typename T>
void zlib_stream_source<T>::zlib_end()
{
    inflateEnd(&_z_stream);
}


template <typename T>
bool zlib_stream_source<T>::try_flush_from_stream()
{
    if(_stream.tellg() < _end)
    {
        size_t to_read = std::min(
            (size_t) (_end-_stream.tellg()),
            _in_buffer.size());

        _stream.read(&_in_buffer[0], to_read);

        size_t n = _stream.gcount();

        if(n > 0)
        {
            _z_stream.avail_in = n;
            _z_stream.next_in = (Bytef*) _in_buffer.data();

            size_t last_total_in = _z_stream.total_in;
            size_t size_read = 0;

            do {

                _z_stream.avail_out = _out_buffer.size();
                _z_stream.next_out = (Bytef*) _out_buffer.data();

                assert(inflate(&_z_stream, Z_NO_FLUSH) != Z_STREAM_ERROR);
                _size_to_write = _out_buffer.size() - _z_stream.avail_out;
                size_read += _z_stream.total_in - last_total_in;

                last_total_in = _z_stream.total_in;

            } while(_z_stream.avail_out == 0);

            _out_i = 0;


            if(size_read >= n)
                return true;

            for(size_t i = size_read; i < n; ++i)
                _in_buffer[i - size_read] = _in_buffer[i];

            return false;
        }
    }

    return false;
}



// pointer source
template <typename T>
zlib_pointer_source<T>::zlib_pointer_source(const char* begin, const char* end, size_t buffer_size)
    : _pointer(begin)
    , _end(end)
    , _z_stream({})
    , _in_buffer(buffer_size)
    , _out_buffer(buffer_size)
    , _out_i(0)
    , _total_read(0)
    , _size_to_write(0)
{
    zlib_init();
}

template <typename T>
void zlib_pointer_source<T>::reset(const char* begin, const char* end)
{
    zlib_end();

    _pointer = begin;
    _end = end;
    _out_i = 0;
    _total_read = 0;

    zlib_init();
}

template <typename T>
bool zlib_pointer_source<T>::read(T& e)
{
    size_t n = sizeof(e);

    if(_out_i + n <= _size_to_write)
    {
        std::copy_n(&_out_buffer[_out_i],
            n, reinterpret_cast<char*>(&e));
        _out_i += n;

        if(_out_i >= _size_to_write || (_out_buffer.size() - _out_i) < n)
            try_flush_from_pointer();

        return true;
    }

    return false;

    return false;
}

template <typename T>
size_t zlib_pointer_source<T>::total_read() const
{
    return _total_read;
}

template <typename T>
void zlib_pointer_source<T>::zlib_init()
{
    _z_stream.zalloc = Z_NULL;
    _z_stream.zfree = Z_NULL;
    _z_stream.opaque = Z_NULL;
    _z_stream.avail_in = 0;
    _z_stream.next_in = Z_NULL;

    assert(inflateInit(&_z_stream) == Z_OK);

    try_flush_from_pointer();
}

template <typename T>
void zlib_pointer_source<T>::zlib_end()
{
    inflateEnd(&_z_stream);
}


template <typename T>
bool zlib_pointer_source<T>::try_flush_from_pointer()
{

    if(_pointer < _end)
    {
        size_t n = std::min(
            (size_t) (_end - _pointer),
            _in_buffer.size());

        std::copy_n(_pointer, n, _in_buffer.data());
        _pointer += n;

        if(n > 0)
        {
            _z_stream.avail_in = n;
            _z_stream.next_in = (Bytef*) _in_buffer.data();

            size_t last_total_in = _z_stream.total_in;
            size_t size_read = 0;

            do {

                _z_stream.avail_out = _out_buffer.size();
                _z_stream.next_out = (Bytef*) _out_buffer.data();

                assert(inflate(&_z_stream, Z_NO_FLUSH) != Z_STREAM_ERROR);
                _size_to_write = _out_buffer.size() - _z_stream.avail_out;
                size_read += _z_stream.total_in - last_total_in;

                last_total_in = _z_stream.total_in;

            } while(_z_stream.avail_out == 0);

            _out_i = 0;


            if(size_read >= n)
                return true;

            for(size_t i = size_read; i < n; ++i)
                _in_buffer[i - size_read] = _in_buffer[i];

            return false;
        }
    }

    return false;
}

INSTATIATE_STATIC_STRUCT_TEMPLATES(zlib_source)
INSTATIATE_STATIC_STRUCT_TEMPLATES(zlib_stream_source)
INSTATIATE_STATIC_STRUCT_TEMPLATES(zlib_pointer_source)

} /* stg */

