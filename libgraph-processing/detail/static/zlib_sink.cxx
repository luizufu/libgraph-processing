#include <libgraph-processing/detail/static/zlib_sink.hxx>

#include <unistd.h>

namespace stg::detail
{

// sink
template <typename T>
zlib_sink<T>::zlib_sink(int fd,
        int compression_level,
        off_t begin, off_t end,
        size_t buffer_size)
    : _fd(fd)
    , _end(end)
    , _z_stream({})
    , _compression_level(compression_level)
    , _in_buffer(buffer_size)
    , _out_buffer(buffer_size)
    , _in_i(0)
    , _total_written(0)
{
    lseek(_fd, begin, SEEK_SET);
    zlib_init();
}

template <typename T>
void zlib_sink<T>::reset(off_t begin, off_t end)
{
    zlib_end();

    lseek(_fd, begin, SEEK_SET);
    _end = end;
    _in_i = 0;
    _total_written = 0;

    zlib_init();
}

template <typename T>
bool zlib_sink<T>::write(const T& e)
{
    if(lseek(_fd, 0, SEEK_CUR) < _end)
    {
        size_t n = sizeof(e);

        std::copy_n(reinterpret_cast<const char*>(&e), n, &_in_buffer[_in_i]);
        _in_i += n;

        if((_in_buffer.size() - _in_i) < n)
            if(!try_flush_to_stream(Z_NO_FLUSH))
                return false;

        return true;
    }

    return false;
}

template <typename T>
void zlib_sink<T>::flush()
{
    zlib_end();
    _in_i = 0;
    zlib_init();
}

template <typename T>
size_t zlib_sink<T>::total_written() const
{
    return _total_written;
}

template <typename T>
bool zlib_sink<T>::has_remaining() const
{
    return _in_i > 0;
}


template <typename T>
void zlib_sink<T>::get_remaining(const T* edges, size_t& n) const
{
    edges =  reinterpret_cast<const T*>(_in_buffer.data());
    n = _in_i / sizeof(T);
}

template <typename T>
void zlib_sink<T>::zlib_init()
{

    _z_stream.zalloc = Z_NULL;
    _z_stream.zfree = Z_NULL;
    _z_stream.opaque = Z_NULL;

    assert(deflateInit(&_z_stream, _compression_level) == Z_OK);
}

template <typename T>
void zlib_sink<T>::zlib_end()
{
    /* if(has_remaining() && _stream.operator bool() && _stream.tellp() < _end) */

    // if _in_i == 0, finish the deflate anyway
    if(lseek(_fd, 0, SEEK_CUR) < _end)
        try_flush_to_stream(Z_FINISH);

    deflateEnd(&_z_stream);
}

template <typename T>
bool zlib_sink<T>::try_flush_to_stream(int flush)
{
    /* _in_buffer[_in_i] = '\0'; */

    _z_stream.avail_in = (uInt) _in_i;
    _z_stream.next_in = (Bytef*) _in_buffer.data();

    size_t last_total_in = _z_stream.total_in;
    size_t size_read = 0;

    do {

        _z_stream.avail_out = (uInt) _out_buffer.size();
        _z_stream.next_out = (Bytef*) _out_buffer.data();

        assert(deflate(&_z_stream, flush) != Z_STREAM_ERROR);
        size_t size_to_write = _out_buffer.size() - _z_stream.avail_out;

        if(size_to_write > 0)
        {
            size_read += _z_stream.total_in - last_total_in;

            if(size_to_write > (_end - lseek(_fd, 0, SEEK_CUR)))
                break;

            _total_written += ::write(_fd, _out_buffer.data(), size_to_write);
            last_total_in = _z_stream.total_in;
        }

    } while(_z_stream.avail_out == 0);


    // if there is something in _in_buffer not write to stream then shift data
    // eliminating already read data and update _in_i accordingly
    // else just, if all data in _in_buffer was write to stream then just
    // reset _in_i
    // NOTE: data that couldn't get flushed to stream can be checked with
    // has_remaining() and obtained the edges back with get_remaining(..., ...)
    /* std::cout << size_read << std::endl; */
    if(size_read >= _in_i)
        return true;

    for(size_t i = size_read; i < _in_i; ++i)
        _in_buffer[i - size_read] = _in_buffer[i];

    _in_i -= size_read;
    return false;
}


// stream sink
template <typename T>
zlib_stream_sink<T>::zlib_stream_sink(std::ostream& stream,
        int compression_level,
        off_t begin, off_t end,
        size_t buffer_size)
    : _stream(stream)
    , _end(end)
    , _z_stream({})
    , _compression_level(compression_level)
    , _in_buffer(buffer_size)
    , _out_buffer(buffer_size)
    , _in_i(0)
    , _total_written(0)

{
    _stream.seekp(begin, std::ios::beg);
    zlib_init();
}


template <typename T>
void zlib_stream_sink<T>::reset(off_t begin, off_t end)
{
    zlib_end();

    _stream.seekp(begin, std::ios::beg);
    _end = end;
    _in_i = 0;
    _total_written = 0;

    zlib_init();
}

template <typename T>
bool zlib_stream_sink<T>::write(const T& e)
{
    if(_stream.operator bool() && _stream.tellp() < _end)
    {
        size_t n = sizeof(e);

        std::copy_n(reinterpret_cast<const char*>(&e), n, &_in_buffer[_in_i]);
        _in_i += n;

        if((_in_buffer.size() - _in_i) < n)
            if(!try_flush_to_stream(Z_NO_FLUSH))
                return false;

        return true;
    }

    return false;
}

template <typename T>
void zlib_stream_sink<T>::flush()
{
    zlib_end();
    _in_i = 0;
    zlib_init();
}

template <typename T>
size_t zlib_stream_sink<T>::total_written() const
{
    return _total_written;
}

template <typename T>
bool zlib_stream_sink<T>::has_remaining() const
{
    return _in_i > 0;
}


template <typename T>
void zlib_stream_sink<T>::get_remaining(const T* edges, size_t& n) const
{
    edges =  reinterpret_cast<const T*>(_in_buffer.data());
    n = _in_i / sizeof(T);
}

template <typename T>
void zlib_stream_sink<T>::zlib_init()
{

    _z_stream.zalloc = Z_NULL;
    _z_stream.zfree = Z_NULL;
    _z_stream.opaque = Z_NULL;

    assert(deflateInit(&_z_stream, _compression_level) == Z_OK);
}

template <typename T>
void zlib_stream_sink<T>::zlib_end()
{
    /* if(has_remaining() && _stream.operator bool() && _stream.tellp() < _end) */

    // if _in_i == 0, finish the deflate anyway
    if(_stream.operator bool() && _stream.tellp() < _end)
        try_flush_to_stream(Z_FINISH);

    deflateEnd(&_z_stream);
}

template <typename T>
bool zlib_stream_sink<T>::try_flush_to_stream(int flush)
{
    /* _in_buffer[_in_i] = '\0'; */

    _z_stream.avail_in = (uInt) _in_i;
    _z_stream.next_in = (Bytef*) _in_buffer.data();

    size_t last_total_in = _z_stream.total_in;
    size_t size_read = 0;

    do {

        _z_stream.avail_out = (uInt) _out_buffer.size();
        _z_stream.next_out = (Bytef*) _out_buffer.data();

        assert(deflate(&_z_stream, flush) != Z_STREAM_ERROR);
        size_t size_to_write = _out_buffer.size() - _z_stream.avail_out;

        if(size_to_write > 0)
        {
            size_read += _z_stream.total_in - last_total_in;

            if(size_to_write > (_end - _stream.tellp()))
                break;

            _stream.write(_out_buffer.data(), size_to_write);
            _total_written += size_to_write;

            last_total_in = _z_stream.total_in;
        }

    } while(_z_stream.avail_out == 0);


    // if there is something in _in_buffer not write to stream then shift data
    // eliminating already read data and update _in_i accordingly
    // else just, if all data in _in_buffer was write to stream then just
    // reset _in_i
    // NOTE: data that couldn't get flushed to stream can be checked with
    // has_remaining() and obtained the edges back with get_remaining(..., ...)
    /* std::cout << size_read << std::endl; */
    if(size_read >= _in_i)
    {
        _in_i = 0;
        return true;
    }

    for(size_t i = size_read; i < _in_i; ++i)
        _in_buffer[i - size_read] = _in_buffer[i];

    _in_i -= size_read;
    return false;
}




// pointer sink
template <typename T>
zlib_pointer_sink<T>::zlib_pointer_sink(int compression_level, char* begin, char* end, size_t buffer_size)
    : _pointer(begin)
    , _end(end)
    , _z_stream({})
    , _compression_level(compression_level)
    , _in_buffer(buffer_size)
    , _out_buffer(buffer_size)
    , _in_i(0)
    , _total_written(0)

{
    zlib_init();
}


template <typename T>
void zlib_pointer_sink<T>::reset(char* begin, char* end)
{
    zlib_end();

    _pointer = begin;
    _end = end;
    _in_i = 0;
    _total_written = 0;

    zlib_init();
}

template <typename T>
bool zlib_pointer_sink<T>::write(const T& e)
{
    if(_pointer != _end)
    {
        size_t n = sizeof(e);

        std::copy_n(reinterpret_cast<const char*>(&e), n, &_in_buffer[_in_i]);
        _in_i += n;

        if((_in_buffer.size() - _in_i) < n)
            if(!try_flush_to_pointer(Z_NO_FLUSH))
                return false;

        return true;
    }

    return false;
}

template <typename T>
void zlib_pointer_sink<T>::flush()
{
    zlib_end();
    _in_i = 0;
    zlib_init();
}

template <typename T>
size_t zlib_pointer_sink<T>::total_written() const
{
    return _total_written;
}

template <typename T>
bool zlib_pointer_sink<T>::has_remaining() const
{
    return _in_i > 0;
}

template <typename T>
void zlib_pointer_sink<T>::get_remaining(const T* edges, size_t& n) const
{
    edges =  reinterpret_cast<const T*>(_in_buffer.data());
    n = _in_i / sizeof(T);
}

template <typename T>
void zlib_pointer_sink<T>::zlib_init()
{

    _z_stream.zalloc = Z_NULL;
    _z_stream.zfree = Z_NULL;
    _z_stream.opaque = Z_NULL;

    assert(deflateInit(&_z_stream, _compression_level) == Z_OK);
}

template <typename T>
void zlib_pointer_sink<T>::zlib_end()
{
    if(has_remaining() && _pointer < _end)
        try_flush_to_pointer(Z_FINISH);

    deflateEnd(&_z_stream);
}

template <typename T>
bool zlib_pointer_sink<T>::try_flush_to_pointer(int flush)
{
    /* _in_buffer[_in_i] = '\0'; */

    _z_stream.avail_in = (uInt) _in_i;
    _z_stream.next_in = (Bytef*) _in_buffer.data();

    size_t last_total_in = _z_stream.total_in;
    size_t size_read = 0;

    do {

        _z_stream.avail_out = (uInt) _out_buffer.size();
        _z_stream.next_out = (Bytef*) _out_buffer.data();

        assert(deflate(&_z_stream, flush) != Z_STREAM_ERROR);
        size_t size_to_write = _out_buffer.size() - _z_stream.avail_out;

        if(size_to_write > 0)
        {
            size_read += _z_stream.total_in - last_total_in;

            if(size_to_write > (_end - _pointer))
                break;

            std::copy_n(_out_buffer.data(), size_to_write, _pointer);
            _pointer += size_to_write;

            _total_written += size_to_write;

            last_total_in = _z_stream.total_in;
        }

    } while(_z_stream.avail_out == 0);


    // if there is something in _in_buffer not write to stream then shift data
    // eliminating already read data and update _in_i accordingly
    // else just, if all data in _in_buffer was write to stream then just
    // reset _in_i
    // NOTE: data that couldn't get flushed to stream can be checked with
    // has_remaining() and obtained the edges back with get_remaining(..., ...)
    /* std::cout << size_read << std::endl; */
    if(size_read >= _in_i)
    {
        _in_i = 0;
        return true;
    }

    for(size_t i = size_read; i < _in_i; ++i)
        _in_buffer[i - size_read] = _in_buffer[i];

    _in_i -= size_read;
    return false;
}

INSTATIATE_STATIC_STRUCT_TEMPLATES(zlib_sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES(zlib_stream_sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES(zlib_pointer_sink)

} /* stg */


