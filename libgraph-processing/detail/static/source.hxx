#pragma once

#include <libgraph-processing/detail/macro/static.hxx>
#include <istream>
#include <limits>
#include <sys/types.h>

namespace stg::detail
{


template <typename T>
class source
{
    int _fd;
    off_t _end;
    size_t _total_read;

public:

    source(int fd,
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max());

    void reset(
       off_t begin = 0,
       off_t end = std::numeric_limits<off_t>::max());

    bool read(T& e);
    size_t total_read() const;
};

template <typename T>
class stream_source
{
    std::istream& _stream;
    off_t _end;
    size_t _total_read;

public:

    stream_source(std::istream& stream,
        off_t begin = 0,
        off_t end = std::numeric_limits<std::streamoff>::max());

    void reset(
        off_t begin = 0,
        off_t end = std::numeric_limits<std::streamoff>::max());

    bool read(T& e);
    size_t total_read() const;
};


template <typename T>
class pointer_source
{
    const char* _pointer;
    const char* _end;
    size_t _total_read;

public:

    pointer_source(const char* begin, const char* end);

    void reset(const char* begin, const char* end);

    bool read(T& e);
    size_t total_read() const;
};


} /* stg */

INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(source)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(stream_source)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(pointer_source)
