#include <libgraph-processing/detail/static/source.hxx>

#include <unistd.h>

namespace stg::detail
{

// source
template <typename T>
source<T>::source(int fd, off_t begin, off_t end)
    : _fd(fd)
    , _end(end)
    , _total_read(0)
{
    lseek(_fd, begin, SEEK_SET);
}

template <typename T>
void source<T>::reset(off_t begin, off_t end)
{
    _end = end;
    _total_read = 0;
    lseek(_fd, begin, SEEK_SET);
}

template <typename T>
bool source<T>::read(T& e)
{
    if(lseek(_fd, 0, SEEK_CUR) < _end)
    {
        ssize_t ret = ::read(_fd, reinterpret_cast<char*>(&e), sizeof(e));
        if(ret)
        {
            _total_read += ret;
            return true;
        }
    }

    return false;
}

template <typename T>
size_t source<T>::total_read() const
{
    return _total_read;
}

// stream source
template <typename T>
stream_source<T>::stream_source(std::istream& stream,
        off_t begin, off_t end)
    : _stream(stream)
    , _end(end)
    , _total_read(0)
{
    _stream.seekg(begin, std::ios::beg);
}

template <typename T>
void stream_source<T>::reset(off_t begin, off_t end)
{
    _end = end;
    _total_read = 0;
    _stream.seekg(begin, std::ios::beg);
}

template <typename T>
bool stream_source<T>::read(T& e)
{
    if(_stream.tellg() < _end)
    {
        if(_stream.read(reinterpret_cast<char*>(&e), sizeof(e)))
        {
            _total_read += sizeof(e);
            return true;
        }
    }

    return false;
}

template <typename T>
size_t stream_source<T>::total_read() const
{
    return _total_read;
}


// pointer source
template <typename T>
pointer_source<T>::pointer_source(const char* begin, const char* end)
    : _pointer(begin)
    , _end(end)
    , _total_read(0)
{
}

template <typename T>
void pointer_source<T>::reset(const char* begin, const char* end)
{
    _pointer = begin;
    _end = end;
    _total_read = 0;
}

template <typename T>
bool pointer_source<T>::read(T& e)
{
    size_t n = sizeof(e);

    if(n <= _end - _pointer)
    {
        std::copy_n(_pointer, n, reinterpret_cast<char*>(&e));
        _pointer += n;
        _total_read += sizeof(e);

        return true;
    }

    return false;
}

template <typename T>
size_t pointer_source<T>::total_read() const
{
    return _total_read;
}

INSTATIATE_STATIC_STRUCT_TEMPLATES(source)
INSTATIATE_STATIC_STRUCT_TEMPLATES(stream_source)
INSTATIATE_STATIC_STRUCT_TEMPLATES(pointer_source)

} /* stg */


