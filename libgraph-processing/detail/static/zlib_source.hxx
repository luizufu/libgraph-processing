#pragma once

#include <libgraph-processing/detail/macro/static.hxx>
#include <istream>
#include <limits>
#include <vector>
#include <zlib.h>
#include <sys/types.h>

namespace stg::detail
{


template <typename T>
class zlib_source
{
    int _fd;
    off_t _end;

    z_stream _z_stream;

    std::vector<char> _in_buffer;
    std::vector<char> _out_buffer;

    size_t _out_i;
    size_t _total_read;
    size_t _size_to_write;

    void zlib_init();
    void zlib_end();
    bool try_flush_from_stream();


public:

    zlib_source(int fd,
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max(),
        size_t buffer_size = 1 << 16);


    void reset(
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max());

    bool read(T& e);
    size_t total_read() const;
};


template <typename T>
class zlib_stream_source
{
    std::istream& _stream;
    off_t _end;

    z_stream _z_stream;

    std::vector<char> _in_buffer;
    std::vector<char> _out_buffer;

    size_t _out_i;
    size_t _total_read;
    size_t _size_to_write;

    void zlib_init();
    void zlib_end();
    bool try_flush_from_stream();


public:

    zlib_stream_source(std::istream& stream,
        off_t begin = 0,
        off_t end = std::numeric_limits<std::streamoff>::max(),
        size_t buffer_size = 1 << 16);


    void reset(off_t begin = 0,
        off_t end = std::numeric_limits<std::streamoff>::max());

    bool read(T& e);
    size_t total_read() const;
};


template <typename T>
class zlib_pointer_source
{
    const char* _pointer;
    const char* _end;

    z_stream _z_stream;

    std::vector<char> _in_buffer;
    std::vector<char> _out_buffer;

    size_t _out_i;
    size_t _total_read;
    size_t _size_to_write;

    void zlib_init();
    void zlib_end();
    bool try_flush_from_pointer();

public:

    zlib_pointer_source(const char* begin, const char* end,
                             size_t buffer_size = 1 << 16);

    void reset(const char* begin, const char* end);

    bool read(T& e);
    size_t total_read() const;
};


} /* stg */

INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(zlib_source)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(zlib_stream_source)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(zlib_pointer_source)
