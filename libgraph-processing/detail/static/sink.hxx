#pragma once

#include <libgraph-processing/detail/macro/static.hxx>
#include <ostream>
#include <limits>
#include <sys/types.h>

namespace stg::detail
{

template <typename T>
class sink
{
    int _fd;
    off_t _end;
    size_t _total_written;

public:

    sink(int fd,
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max());

    void reset(
        off_t begin = 0,
        off_t end = std::numeric_limits<off_t>::max());

    bool write(const T& e);
    void flush();
    size_t total_written() const;
};


template <typename T>
class stream_sink
{
    std::ostream& _stream;
    off_t _end;
    size_t _total_written;

public:

    stream_sink(std::ostream& stream,
                     off_t begin = 0,
                     off_t end = std::numeric_limits<std::streamoff>::max());

    void reset(off_t begin = 0,
               off_t end = std::numeric_limits<std::streamoff>::max());

    bool write(const T& e);
    void flush();
    size_t total_written() const;
};

template <typename T>
class pointer_sink
{
    char* _pointer;
    char* _end;
    size_t _total_written;

public:

    pointer_sink(char* begin, char* end);

    void reset(char* begin, char* end);

    bool write(const T& e);
    void flush();
    size_t total_written() const;
};


} /* stg */

INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(stream_sink)
INSTATIATE_STATIC_STRUCT_TEMPLATES_EXTERN(pointer_sink)
