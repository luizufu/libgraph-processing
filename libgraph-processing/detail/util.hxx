#pragma once

#include <cstddef>

// move these macros to better places
#define PSIZE(array) PAGE_SIZE / sizeof(array[0])
#define PESIZE(edges) PAGE_SIZE / sizeof(edges[0]) - sizeof(uint32_t)

namespace stg::detail
{

template <typename T>
T* as(std::byte* bytes)
{
    return reinterpret_cast<T*>(bytes);
}
template <typename T>
const T* as(const std::byte* bytes)
{
    return reinterpret_cast<T*>(bytes);
}

template <typename T>
T& ref_as(std::byte* bytes)
{
    return *reinterpret_cast<T*>(bytes);
}
template <typename T>
const T& ref_as(const std::byte* bytes)
{
    return *reinterpret_cast<const T*>(bytes);
}

template <typename T>
const std::byte* to_bytes(const T& data)
{
    return reinterpret_cast<const std::byte*>(&data);
}

template <typename T>
std::byte* to_bytes(T& data)
{
    return reinterpret_cast<std::byte*>(&data);
}

} /* stg */
