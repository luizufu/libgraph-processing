#pragma once

#include <libgraph-processing/config.hxx>
#include <cstdint>

namespace stg::detail
{

    template <typename Header>
    struct alignas(PAGE_SIZE) dynamic_super_block
    {
        Header header;
        uint32_t key_list1;
        uint32_t key_list2;
        uint32_t key_list3;
        uint32_t key_list4;
        uint32_t key_list5;
        uint32_t free_list;
    };

} /* stg::detail */


