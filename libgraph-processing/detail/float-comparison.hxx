#pragma once

#include <cmath>
#include <limits>

namespace stg::detail
{

inline bool approximately_equal(float a, float b,
        float epsilon = std::numeric_limits<float>::epsilon())
{
    return fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
}

inline bool essentially_equal(float a, float b,
        float epsilon = std::numeric_limits<float>::epsilon())
{
    return fabs(a - b) <= ( (fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * epsilon);
}

inline bool definitely_greater_than(float a, float b,
        float epsilon = std::numeric_limits<float>::epsilon())
{
    return (a - b) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
}

inline bool definitely_less_than(float a, float b,
        float epsilon = std::numeric_limits<float>::epsilon())
{
    return (b - a) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
}


} /* stg */
