#pragma once


namespace stg::detail
{

template<typename Graph>
struct extract_edge;

template<
    template<typename, typename> class Graph,
    typename Edge, typename Neighbor>
struct extract_edge<Graph<Edge, Neighbor>>
{
    using type = Edge;
};


template<typename Graph>
struct extract_neighbor;

template<
    template<typename, typename> class Graph,
    typename Edge, typename Neighbor>
struct extract_neighbor<Graph<Edge, Neighbor>>
{
    using type = Neighbor;
};

template<typename BlockProvider>
struct extract_block;

template<template<typename> class BlockProvider, typename Block>
struct extract_block<BlockProvider<Block>>
{
    using type = Block;
};

template <typename Graph>
using extract_edge_t = typename extract_edge<Graph>::type;

template <typename Graph>
using extract_neighbor_t = typename extract_neighbor<Graph>::type;

template <typename BlockProvider>
using extract_block_t = typename extract_block<BlockProvider>::type;


} /* stg */
