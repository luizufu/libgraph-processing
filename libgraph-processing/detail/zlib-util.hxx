#pragma once

#include <cassert>
#include "zlib.h"

namespace stg::detail
{

/* int partcompress(const char* in, size_t size, z_stream& def) */
/* { */
/*     int ret, flush; */

/*     flush = Z_NO_FLUSH; */
/*     do { */
/*         def.avail_in = size - def.total_in; */
/*         def.next_in = (Bytef*) (in + def.total_in); */
/*         if (def.total_in == size) */
/*             flush = Z_FINISH; */
/*         ret = deflate(&def, flush); */
/*         assert(ret != Z_STREAM_ERROR); */
/*     } while (def.avail_out != 0 && flush == Z_NO_FLUSH); */
/*     return ret; */
/* } */

/* int recompress(z_stream& inf, z_stream& def) */
/* { */
/*     int ret, flush; */

/*     flush = Z_NO_FLUSH; */
/*     do { */
/*         /1* decompress *1/ */
/*         inf.avail_out = RAW_LENGTH; */
/*         inf.next_out = raw; */
/*         ret = inflate(&inf, Z_NO_FLUSH); */
/*         assert(ret != Z_STREAM_ERROR && ret != Z_DATA_ERROR && */
/*                ret != Z_NEED_DICT); */
/*         if (ret == Z_MEM_ERROR) */
/*             return ret; */

/*         /1* compress what was decompresed until done or no room *1/ */
/*         def.avail_in = RAW_LENGTH - inf.avail_out; */
/*         def.next_in = raw; */
/*         if (inf.avail_out != 0) */
/*             flush = Z_FINISH; */
/*         ret = deflate(&def, flush); */
/*         assert(ret != Z_STREAM_ERROR); */
/*     } while (ret != Z_STREAM_END && def.avail_out != 0); */
/*     return ret; */
/* } */

int compress2_dic(Bytef* dest, uLongf *destLen, const Bytef* source, uLong sourceLen, int level, Bytef* dic, uLongf* dicLen)
{
    z_stream stream;
    int err;
    const uInt max = (uInt)-1;
    uLong left;

    left = *destLen;
    *destLen = 0;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;
    stream.opaque = (voidpf)0;

    err = deflateInit(&stream, level);
    if (err != Z_OK) return err;

    err = deflateSetDictionary(&stream, dic, *dicLen);
    if (err != Z_OK) return err;

    stream.next_out = dest;
    stream.avail_out = 0;
    stream.next_in = (z_const Bytef *)source;
    stream.avail_in = 0;

    do {
        if (stream.avail_out == 0) {
            stream.avail_out = left > (uLong)max ? max : (uInt)left;
            left -= stream.avail_out;
        }
        if (stream.avail_in == 0) {
            stream.avail_in = sourceLen > (uLong)max ? max : (uInt)sourceLen;
            sourceLen -= stream.avail_in;
        }
        err = deflate(&stream, sourceLen ? Z_NO_FLUSH : Z_FINISH);
    } while (err == Z_OK);

    *destLen = stream.total_out;
    deflateEnd(&stream);
    return err == Z_STREAM_END ? Z_OK : err;
}

int compress_dic(Bytef* dest, uLongf* destLen, const Bytef* source, uLong sourceLen, )
{
    return compress2_dic(dest, destLen, source, sourceLen,
            Z_DEFAULT_COMPRESSION);
}

int uncompress2_dic(Bytef* dest, uLongf* destLen, const Bytef* source, uLong* sourceLen)
{
    z_stream stream;
    int err;
    const uInt max = (uInt)-1;
    uLong len, left;
    Byte buf[1];    /* for detection of incomplete stream when *destLen == 0 */

    len = *sourceLen;
    if (*destLen) {
        left = *destLen;
        *destLen = 0;
    }
    else {
        left = 1;
        dest = buf;
    }

    stream.next_in = (z_const Bytef *)source;
    stream.avail_in = 0;
    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;
    stream.opaque = (voidpf)0;

    err = inflateInit(&stream);
    if (err != Z_OK) return err;

    stream.next_out = dest;
    stream.avail_out = 0;

    do {
        if (stream.avail_out == 0) {
            stream.avail_out = left > (uLong)max ? max : (uInt)left;
            left -= stream.avail_out;
        }
        if (stream.avail_in == 0) {
            stream.avail_in = len > (uLong)max ? max : (uInt)len;
            len -= stream.avail_in;
        }
        err = inflate(&stream, Z_NO_FLUSH);
    } while (err == Z_OK);

    *sourceLen -= len + stream.avail_in;
    if (dest != buf)
        *destLen = stream.total_out;
    else if (stream.total_out && err == Z_BUF_ERROR)
        left = 1;

    inflateEnd(&stream);
    return err == Z_STREAM_END ? Z_OK :
           err == Z_NEED_DICT ? Z_DATA_ERROR  :
           err == Z_BUF_ERROR && left + stream.avail_out ? Z_DATA_ERROR :
           err;
}

int uncompress_dic(Bytef* dest, uLongf* destLen, const Bytef* source, uLong sourceLen)
{
    return uncompress2_dic(dest, destLen, source, &sourceLen);
}

} /* stg */
