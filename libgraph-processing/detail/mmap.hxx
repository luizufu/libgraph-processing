#pragma once

#include <string>
#include <cstddef>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

namespace stg::detail
{

template <typename T>
struct mmapping
{
    int fd = -1;
    T* begin = nullptr;
    T* end = nullptr;
};

template <typename T>
bool is_mmapped(const mmapping<T>& m)
{
    return m.fd != -1;
}


inline size_t get_file_size(const std::string& filename)
{
    struct stat st;
    stat(filename.c_str(), &st);
    return st.st_size;
}

template <typename T>
mmapping<T> mmap_file(const std::string& filename)
{
    int fd = open(filename.c_str(), O_RDWR, 0);
    if(fd == -1)
    {

        std::cerr << "Cannot open " << filename
                  << " in order to sort" << std::endl;
        exit(1);
    }

    size_t file_size = get_file_size(filename);
    T* mapping = reinterpret_cast<T*>(mmap(NULL, file_size,
                                           PROT_READ | PROT_WRITE,
                                           MAP_SHARED | MAP_POPULATE,
                                           fd, 0));
    if(mapping == MAP_FAILED)
    {
        std::cerr << "Cannot map " << filename << " to memory" << std::endl;
        exit(1);
    }

    return { fd, mapping, mapping + (file_size / sizeof(T)) };
}

template <typename T>
void unmmap_file(mmapping<T> mapping)
{
    size_t file_size = (mapping.end - mapping.begin) * sizeof(T);
    if(munmap(reinterpret_cast<char*>(mapping.begin), file_size) != 0)
    {
        close(mapping.fd);
        std::cerr << "Cannot unmap file from memory" << std::endl;
        exit(1);
    }

    close(mapping.fd);
}


} /* stg */
