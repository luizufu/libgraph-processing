#include <libgraph-processing/detail/dynamic/zlib-disk-block-unordered-multimap.hxx>
#include <libgraph-processing/detail/util.hxx>
#include <iostream>

namespace stg::detail
{


template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::create(BlockAllocator& alloc)
{
    block b = {
        .kmap_root = kmap_t::create(alloc),
        .kmap_leaf = b.kmap_root,
        .total = 0
    };

    uint32_t root = alloc.new_block();
    alloc.get().write(root, to_bytes(b));

    return root;
}

template <typename Key, typename Value, typename BlockAllocator>
void zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::destroy(BlockAllocator& alloc, uint32_t root)
{
    block b;
    alloc.get().read(root, to_bytes(b));
    kmap_t::destroy(alloc, b.kmap_root);
    alloc.delete_block(root, to_bytes(b));
}

template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::insert(BlockAllocator& alloc, uint32_t root, const Key& key, const Value& value)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(auto location = kmap_t::find_location(alloc, b.kmap_root, key))
    {
        kmap_t::update_value_direct(
            alloc, *location, [&](auto& kn){
                vlist_t::push(alloc, kn.block, value);
                ++kn.total;
                return true;
            });
    }
    else
    {
        zlib_unordered_key_node kn = {
            .total = 1,
            .block = vlist_t::create(alloc)
        };

        vlist_t::push(alloc, kn.block, value);

        uint32_t old_root = b.kmap_root;
        kmap_t::insert(alloc, b.kmap_root, key, kn);

        if(b.kmap_root != old_root)
            alloc.get().write(root, to_bytes(b));
    }

    ++b.total;
    alloc.get().write(root, to_bytes(b));

    return true;
}

template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::insert(BlockAllocator& alloc, uint32_t root, const Key& key, std::vector<Value>& values)
{
    if(!root)
        return 0;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(auto location = kmap_t::find_location(alloc, b.kmap_root, key))
    {
        kmap_t::update_value_direct(
            alloc, *location, [&](auto& kn){
                for(const auto& value : values)
                    vlist_t::push(alloc, kn.block, value);

                kn.total += values.size();
                return true;
            });
    }
    else
    {
        zlib_unordered_key_node kn = {
            .total = (uint32_t) values.size(),
            .block = vlist_t::create(alloc)
        };

        for(auto value : values)
            vlist_t::push(alloc, kn.block, value);

        uint32_t old_root = b.kmap_root;
        kmap_t::insert(alloc, b.kmap_root, key, kn);

        if(b.kmap_root != old_root)
            alloc.get().write(root, to_bytes(b));
    }

    b.total += values.size();
    alloc.get().write(root, to_bytes(b));

    return values.size();
}

template <typename Key, typename Value, typename BlockAllocator>
std::vector<Value> zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    std::vector<Value> removed;

    if(!root)
        return removed;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(auto kn = kmap_t::remove(alloc, b.kmap_root, key))
    {
        removed = vlist_t::remove_all(alloc, kn->block);

        b.total -= removed.size();

        vlist_t::destroy(alloc, kn->block);

        uint32_t old_root = b.kmap_root;
        kmap_t::remove(alloc, b.kmap_root, key);

        if(b.kmap_root != old_root || !removed.empty())
            alloc.get().write(root, to_bytes(b));
    }

    return removed;
}

template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t root, const Key& key, const Value& value)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    uint32_t old_root = b.kmap_root;
    bool removed = false;

    if(auto location = kmap_t::find_location(alloc, b.kmap_root, key))
    {
        zlib_unordered_key_node kn = { .total = 1 };
        kmap_t::update_value_direct(
            alloc, *location, [&](auto& node){
                if(vlist_t::remove(alloc, node.block, value))
                {
                    removed = true;
                    --node.total;
                    --b.total;
                }

                kn = node;
                return true;
            });

        if(kn.total == 0)
        {
            vlist_t::destroy(alloc, kn.block);
            kmap_t::remove(alloc, b.kmap_root, key);
        }
    }

    if(b.kmap_root != old_root || removed)
        alloc.get().write(root, to_bytes(b));

    return true;
}

template <typename Key, typename Value, typename BlockAllocator>
std::vector<Value> zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t root, const Key& key, std::vector<Value>& values)
{
    std::vector<Value> removed;

    if(!root)
        return removed;

    block b;
    alloc.get().read(root, to_bytes(b));
    uint32_t old_root = b.kmap_root;

    if(auto location = kmap_t::find_location(alloc, b.kmap_root, key))
    {
        zlib_unordered_key_node kn = { .total = 1 };
        kmap_t::update_value_direct(
            alloc, *location, [&](auto& node){
                for(auto v : vlist_t::remove(alloc, node.block, values))
                {
                    removed.push_back(v);
                    --node.total;
                    --b.total;
                }

                kn = node;
                return true;
            });

        if(kn.total == 0)
        {
            vlist_t::destroy(alloc, kn.block);
            kmap_t::remove(alloc, b.kmap_root, key);
        }
    }

    if(b.kmap_root != old_root || !removed.empty())
        alloc.get().write(root, to_bytes(b));

    return removed;
}

template <typename Key, typename Value, typename BlockAllocator>
cppcoro::generator<Value> zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::find(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    if(root)
    {
        block b;
        alloc.get().read(root, to_bytes(b));

        if(auto kn = kmap_t::find(alloc, b.kmap_root, key))
            for(auto v : vlist_t::all(alloc, kn->block))
                co_yield v;
    }
}

template <typename Key, typename Value, typename BlockAllocator>
cppcoro::generator<std::pair<Key, Value>> zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::all(BlockAllocator& alloc, uint32_t root)
{
    if(root)
    {
        block b;
        alloc.get().read(root, to_bytes(b));

        for(auto [key, kn] : kmap_t::all(alloc, b.kmap_leaf))
            for(auto v : vlist_t::all(alloc, kn.block))
                co_yield { key, v };
    }
}

template <typename Key, typename Value, typename BlockAllocator>
cppcoro::generator<Key> zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::all_keys(BlockAllocator& alloc, uint32_t root)
{
    if(root)
    {
        block b;
        alloc.get().read(root, to_bytes(b));

        for(auto entry : kmap_t::all(alloc, b.kmap_leaf))
            co_yield entry.first;
    }
}

template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::count(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    if(root)
    {
        block b;
        alloc.get().read(root, to_bytes(b));

        if(auto kn = kmap_t::find(alloc, b.kmap_root, key))
            return kn->total;
    }

    return 0;
}

template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::exists(BlockAllocator& alloc, uint32_t root, const Key& key, const Value& value)
{
    for(const auto& v : find(alloc, root, key))
        if(v == value)
            return true;

    return false;
}

template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::size(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return 0;

    block b;
    alloc.get().read(root, to_bytes(b));
    return b.total;
}


template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::empty(BlockAllocator& alloc, uint32_t root)
{
    return size(alloc, root) == 0;
}


template <typename Key, typename Value, typename BlockAllocator>
void zlib_disk_block_unordered_multimap<Key, Value, BlockAllocator>::debug(BlockAllocator& alloc, uint32_t root)
{
    if(root)
    {
        block b;
        alloc.get().read(root, to_bytes(b));

        std::cout << "KEY MAP\n\n";
        kmap_t::debug(alloc, b.kmap_root, b.kmap_leaf);

        for(auto [key, kn] : kmap_t::all(alloc, b.kmap_leaf))
        {
            std::cout << "\nKEY NODE #" << key << " at " << kn << '\n';
            vlist_t::debug(alloc, kn.block);
        }
    }
}

INSTATIATE_LV0_STRUCT_TEMPLATES(zlib_disk_block_unordered_multimap)

} /* stg */

