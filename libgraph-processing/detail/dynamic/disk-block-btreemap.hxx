#pragma once

#include <libgraph-processing/config.hxx>
#include <libgraph-processing/detail/macro/dynamic1.hxx>
#include <cppcoro/generator.hpp>
#include <vector>
#include <optional>

namespace stg::detail
{

template <typename Key, typename Value, typename BlockAllocator>
class disk_block_btreemap
{
    enum block_type : uint32_t
    {
        internal,
        external
    };

    static constexpr uint32_t N_IKEYS =
        (PAGE_SIZE - 2*sizeof(uint32_t)) /
        (sizeof(Key) + sizeof(uint32_t)) - 1;

    static constexpr uint32_t N_EKEYS =
        (PAGE_SIZE - 3*sizeof(uint32_t)) /
        (sizeof(Key) + sizeof(Value)) - 1;

    struct iblock
    {
        Key keys[N_IKEYS + 1];
        uint32_t pointers[N_IKEYS + 1];

    };

    struct eblock
    {
        Value values[N_EKEYS + 1];
        Key keys[N_EKEYS + 1];
        uint32_t pointer;

    };

    struct alignas(PAGE_SIZE) block
    {
        union
        {
            iblock i;
            eblock e;
        };

        uint32_t type;
        uint32_t size;
    };

    struct update_result
    {
        bool updated_parent;
        bool succeeded;
    };

    static size_t find_eq(const Key& key, const block& b);
    static size_t find_geq(const Key& key, const block& b);

    static update_result insert_rec(BlockAllocator& alloc,
            uint32_t index, block& parent,
            const Key& key, const Value& value);

    static update_result remove_rec(BlockAllocator& alloc,
            uint32_t index, block& parent,
            const Key& key, Value& value,
            uint32_t& root);

public:

    struct location
    {
        uint32_t block;
        uint32_t pos;
    };

    static uint32_t create(BlockAllocator& alloc);
    static void destroy(BlockAllocator& alloc, uint32_t root);

    static bool insert(BlockAllocator& alloc, uint32_t& root,
                       const Key& key, const Value& value);

    template <typename Updater>
    static bool update_value(BlockAllocator& alloc, uint32_t& root,
                             Updater updater);

    template <typename Updater>
    static bool update_value(BlockAllocator& alloc, uint32_t& root,
                             const Key& key, Updater updater);

    template <typename Updater>
    static bool update_value_direct(BlockAllocator& alloc, const location& loc,
                                    Updater updater);

    static std::optional<Value> remove(BlockAllocator& alloc, uint32_t& root,
                                       const Key& key);

    static std::vector<std::pair<Key, Value>>
        remove(BlockAllocator& alloc, uint32_t& root,
               const std::vector<Key>& keys);

    static std::optional<Value> find(BlockAllocator& alloc, uint32_t root,
                                     const Key& key);

    static std::optional<location>
        find_location(BlockAllocator& alloc, uint32_t root, const Key& key);

    static cppcoro::generator<std::pair<Key, Value>>
        all(BlockAllocator& alloc, uint32_t first_leaf);

    static bool exists(BlockAllocator& alloc, uint32_t root,
                       const Key& key);

    static size_t size(BlockAllocator& alloc, uint32_t first_leaf);
    static bool empty(BlockAllocator& alloc, uint32_t first_leaf);

    static void debug(BlockAllocator& alloc,
                      uint32_t root, uint32_t first_leaf);
};



} /* stg */

#include <libgraph-processing/detail/dynamic/disk-block-btreemap.ixx>

INSTATIATE_LV1_STRUCT_TEMPLATES_EXTERN(disk_block_btreemap)
