#include <libgraph-processing/detail/util.hxx>

namespace stg::detail
{

template <typename Key, typename Value, typename BlockAllocator>
template <typename Updater>
bool disk_block_btreemap<Key, Value, BlockAllocator>::update_value(BlockAllocator& alloc,uint32_t& root, Updater updater)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    if(b.type == block_type::external)
    {
        if(b.size == 0)
            return false;

        bool updated = false;
        for(size_t i = 0; i < b.size; ++i)
            updated |= updater(b.e.keys[i], b.e.values[i]);

        if(!updated)
            return false;

        alloc.get().write(root, to_bytes(b));
        return true;
    }

    bool updated = false;
    for(size_t i = 0; i < b.size; ++i)
        updated |= update_value(alloc, b.i.pointers[i], updater);

    return updated;
}

template <typename Key, typename Value, typename BlockAllocator>
template <typename Updater>
bool disk_block_btreemap<Key, Value, BlockAllocator>::update_value(BlockAllocator& alloc,uint32_t& root, const Key& key, Updater updater)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    if(b.type == block_type::external)
    {
        if(b.size == 0)
            return false;

        size_t i = find_eq(key, b);
        if(i == b.size)
            return false;

        if(!updater(b.e.values[i]))
            return false;

        alloc.get().write(root, to_bytes(b));
        return true;
    }

    size_t i = find_geq(key, b);
    return update_value(alloc, b.i.pointers[i], key, updater);
}

template <typename Key, typename Value, typename BlockAllocator>
template <typename Updater>
bool disk_block_btreemap<Key, Value, BlockAllocator>::update_value_direct(BlockAllocator& alloc, const location& loc, Updater updater)
{
    if(!loc.block)
        return false;

    block b;
    alloc.get().read(loc.block, to_bytes(b));

    if(b.type != block_type::external)
        return false;

    if(loc.pos >= b.size)
        return false;

    if(!updater(b.e.values[loc.pos]))
        return false;

    alloc.get().write(loc.block, to_bytes(b));
    return true;
}

} /* stg */
