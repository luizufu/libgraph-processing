#include <libgraph-processing/detail/dynamic/zlib-disk-block-list.hxx>
#include <libgraph-processing/detail/util.hxx>
#include <iostream>
#include <algorithm>
#include <array>
#include <zlib.h>

namespace stg::detail
{


static const size_t RAW_LENGTH = 1 << 16;
static std::array<std::byte, RAW_LENGTH> buffer;

template <typename UBlock>
static size_t sizeof_ublock(const UBlock& ub)
{
    return
        sizeof(ub.next) +
        ub.elements.size()*sizeof(typename decltype(ub.elements)::value_type);
}

template <typename Vector>
static size_t sizeof_vector(const Vector& v)
{
    return v.size() * sizeof(typename Vector::value_type);
}


template <typename T, typename BlockAllocator>
void zlib_disk_block_list<T, BlockAllocator>::compress(ublock& ub, block& b)
{
    std::copy_n(
        reinterpret_cast<const std::byte*>(&ub.next),
        sizeof(ub.next), buffer.data());

    if(!ub.elements.empty())
    {
        std::copy_n(
            reinterpret_cast<const std::byte*>(ub.elements.data()),
            sizeof_vector(ub.elements), buffer.data() + sizeof(ub.next));
    }

    uLongf out_len = PAGE_SIZE;
    ::compress(
        (Bytef*) b.data, &out_len,
        (Bytef*) buffer.data(), sizeof_ublock(ub));

    b.size = out_len + sizeof(b.size);
}

template <typename T, typename BlockAllocator>
void zlib_disk_block_list<T, BlockAllocator>::uncompress(block& b, ublock& ub)
{
    uLongf out_len = RAW_LENGTH;
    ::uncompress(
        (Bytef*) buffer.data(), &out_len,
        (Bytef*) b.data, b.size);

    std::copy_n(
        buffer.data(), sizeof(ub.next), reinterpret_cast<std::byte*>(&ub.next));

    if(out_len > sizeof(ub.next))
    {
        using vector_t = decltype(ub.elements);
        using value_t = typename vector_t::value_type;

        ub.elements = vector_t(
                (value_t*) (buffer.data() + sizeof(ub.next)),
                (value_t*) (buffer.data() + out_len));
    }
}


template <typename T, typename BlockAllocator>
uint32_t zlib_disk_block_list<T, BlockAllocator>::create(BlockAllocator& alloc)
{
    ublock ub = { .next = 0 };

    block b;
    compress(ub, b);

    uint32_t id = alloc.new_block();
    alloc.get().write(id, to_bytes(b));

    return id;
}

template <typename T, typename BlockAllocator>
void zlib_disk_block_list<T, BlockAllocator>::destroy(BlockAllocator& alloc, uint32_t root)
{
    remove_all(alloc, root);
    alloc.delete_block(root);
}

template <typename T, typename BlockAllocator>
std::pair<uint32_t, size_t> zlib_disk_block_list<T, BlockAllocator>::push(BlockAllocator& alloc, uint32_t root, const T& element)
{
    if(!root)
        return {};

    block b1;
    alloc.get().read(root, to_bytes(b1));

    if(b1.size < PAGE_SIZE - (sizeof(T) * 2))
    {
        ublock ub1;
        uncompress(b1, ub1);

        ub1.elements.push_back(element);
        compress(ub1, b1);

        alloc.get().write(root, to_bytes(b1));

        return { root, ub1.elements.size() - 1 };
    }

    ublock ub1;
    uncompress(b1, ub1);

    ublock ub2 = { .next = ub1.next };
    ub2.elements.push_back(element);

    ub1.next = alloc.new_block();
    compress(ub1, b1);
    alloc.get().write(root, to_bytes(b1));

    block b2;
    compress(ub2, b2);
    alloc.get().write(ub1.next, to_bytes(b2));

    return { ub1.next, 0 };
}

template <typename T, typename BlockAllocator>
void zlib_disk_block_list<T, BlockAllocator>::pop(BlockAllocator& alloc, uint32_t& root)
{
    if(!root)
        return;

    block b;
    alloc.get().read(root, to_bytes(b));

    ublock ub;
    uncompress(b, ub);

    if(!ub.elements.empty())
    {
        ub.elements.pop_back();

        if(!ub.elements.empty())
        {
            compress(ub, b);
            alloc.get().write(root, to_bytes(b));
        }
        else if(ub.next)
        {
            uint32_t next = ub.next;
            alloc.delete_block(root, to_bytes(b));
            root = next;
        }
    }
}

template <typename T, typename BlockAllocator>
bool zlib_disk_block_list<T, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, const T& element)
{
    if(!root)
        return false;

    uint32_t prev = root;
    ublock ub_prev = { .next = root };
    block b_prev;
    ublock ub_cur;
    block b_cur;

    while(ub_prev.next)
    {
        alloc.get().read(ub_prev.next, to_bytes(b_cur));
        uncompress(b_cur, ub_cur);

        auto it = std::find(
                ub_cur.elements.begin(), ub_cur.elements.end(), element);

        if(it != ub_cur.elements.end())
        {
            *it = ub_cur.elements.back();
            ub_cur.elements.pop_back();

            if(ub_cur.elements.size() > 0)
            {
                compress(ub_cur, b_cur);
                alloc.get().write(ub_prev.next, to_bytes(b_cur));
            }
            else if(ub_prev.next != root)
            {
                uint32_t next = ub_cur.next;
                alloc.delete_block(ub_prev.next, to_bytes(b_cur));

                ub_prev.next = ub_cur.next;
                compress(ub_prev, b_prev);
                alloc.get().write(prev, to_bytes(b_prev));
            }
            else if(ub_cur.next) // is root
            {
                uint32_t next = ub_cur.next;
                alloc.delete_block(ub_prev.next, to_bytes(b_cur));
                root = next;
            }
            else // is root and has no next
            {
                compress(ub_cur, b_cur);
                alloc.get().write(ub_prev.next, to_bytes(b_cur));
            }

            return true;
        }

        prev = ub_prev.next;
        ub_prev = ub_cur;
    }

    return false;
}

template <typename T, typename BlockAllocator>
std::vector<T> zlib_disk_block_list<T, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, std::vector<T>& elements)
{
    if(!root)
        return {};

    uint32_t prev = root;
    ublock ub_prev = { .next = root };
    block b_prev;
    ublock ub_cur;
    block b_cur;

    std::vector<T> removed;
    while(ub_prev.next)
    {
        alloc.get().read(ub_prev.next, to_bytes(b_cur));
        uncompress(b_cur, ub_cur);

        bool removed_something = false;
        for(size_t i = 0; i < elements.size(); ++i)
        {
            auto it = std::find(
                ub_cur.elements.begin(), ub_cur.elements.end(), elements[i]);

            if(it != ub_cur.elements.end())
            {
                removed.push_back(*it);
                *it = ub_cur.elements.back();
                ub_cur.elements.pop_back();
                elements[i] = elements.back();
                elements.pop_back();
                removed_something = true;
            }
        }

        if(removed_something)
        {
            if(!ub_cur.elements.empty())
            {
                compress(ub_cur, b_cur);
                alloc.get().write(ub_prev.next, to_bytes(b_cur));
            }
            else if(ub_prev.next != root)
            {
                uint32_t next = ub_cur.next;
                alloc.delete_block(ub_prev.next, to_bytes(b_cur));

                ub_prev.next = ub_cur.next;
                compress(ub_prev, b_prev);
                alloc.get().write(prev, to_bytes(b_prev));
            }
            else if(ub_cur.next) // is root
            {
                uint32_t next = ub_cur.next;
                alloc.delete_block(ub_prev.next, to_bytes(b_cur));
                root = next;
            }
            else // is root and has no next
            {
                compress(ub_cur, b_cur);
                alloc.get().write(ub_prev.next, to_bytes(b_cur));
            }

            if(elements.empty())
                return removed;
        }

        prev = ub_prev.next;
        ub_prev = ub_cur;
    }

    return removed;
}

template <typename T, typename BlockAllocator>
std::vector<T> zlib_disk_block_list<T, BlockAllocator>::remove_all(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return {};

    std::vector<T> removed;

    block b;
    ublock ub;
    alloc.get().read(root, to_bytes(b));
    uncompress(b, ub);

    removed.insert(removed.end(), ub.elements.begin(), ub.elements.end());
    ub.elements.clear();
    ub.next = 0;

    compress(ub, b);
    alloc.get().write(root, to_bytes(b));

    while(ub.next)
    {
        uint32_t old_next = ub.next;
        alloc.get().read(ub.next, to_bytes(b));
        uncompress(b, ub);
        removed.insert(removed.end(), ub.elements.begin(), ub.elements.end());
        alloc.delete_block(old_next, to_bytes(b));
    }

    return removed;
}

template <typename T, typename BlockAllocator>
T zlib_disk_block_list<T, BlockAllocator>::direct_access(BlockAllocator& alloc, uint32_t block_id, size_t pos)
{
    block b;
    alloc.get().read(block_id, to_bytes(b));

    ublock ub;
    uncompress(b, ub);

    return ub.elements[pos];
}

template <typename T, typename BlockAllocator>
cppcoro::generator<T> zlib_disk_block_list<T, BlockAllocator>::all(BlockAllocator& alloc, uint32_t root)
{
    block b;
    ublock ub = { .next = root };

    while(ub.next)
    {
        alloc.get().read(ub.next, to_bytes(b));
        uncompress(b, ub);

        for(auto& element : ub.elements)
            co_yield element;
    }
}


template <typename T, typename BlockAllocator>
bool zlib_disk_block_list<T, BlockAllocator>::exists(BlockAllocator& alloc, uint32_t root, const T& element)
{
    for(auto& e : all(alloc, root))
        if(e == element)
            return true;

    return false;
}

template <typename T, typename BlockAllocator>
size_t zlib_disk_block_list<T, BlockAllocator>::size(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return 0;

    size_t count = 0;
    block b;
    ublock ub = { .next = root };

    while(ub.next)
    {
        alloc.get().read(ub.next, to_bytes(b));
        uncompress(b, ub);

        count += ub.elements.size();
    }

    return count;
}


template <typename T, typename BlockAllocator>
bool zlib_disk_block_list<T, BlockAllocator>::empty(BlockAllocator& alloc, uint32_t root)
{
    return size(alloc, root) == 0;
}

template <typename T, typename BlockAllocator>
void zlib_disk_block_list<T, BlockAllocator>::debug(BlockAllocator& alloc, uint32_t root)
{
    block b;
    ublock ub = { .next = root };

    while(ub.next)
    {
        alloc.get().read(ub.next, to_bytes(b));
        uncompress(b, ub);

        for(size_t i = 0; i < ub.elements.size(); ++i)
            std::cout << "element #" << i << ": " << ub.elements[i] << '\n';

        std::cout << "size: " << ub.elements.size() << '\n';
        std::cout << "next: " << ub.next << '\n';
    }
}

INSTATIATE_LV2_STRUCT_TEMPLATES(zlib_disk_block_list)

} /* stg */

