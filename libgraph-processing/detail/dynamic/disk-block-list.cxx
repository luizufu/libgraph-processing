#include <libgraph-processing/detail/dynamic/disk-block-list.hxx>
#include <libgraph-processing/detail/util.hxx>
#include <algorithm>
#include <iostream>

namespace stg::detail
{


template <typename T, typename BlockAllocator>
uint32_t disk_block_list<T, BlockAllocator>::create(BlockAllocator& alloc)
{
    block b = {
        .size = 0,
        .next = 0
    };

    uint32_t id = alloc.new_block();
    alloc.get().write(id, to_bytes(b));

    return id;
}

template <typename T, typename BlockAllocator>
void disk_block_list<T, BlockAllocator>::destroy(BlockAllocator& alloc, uint32_t root)
{
    remove_all(alloc, root);
    alloc.delete_block(root);
}

template <typename T, typename BlockAllocator>
std::pair<uint32_t, size_t> disk_block_list<T, BlockAllocator>::push(BlockAllocator& alloc, uint32_t root, const T& element)
{
    if(!root)
        return {};

    block b1;
    alloc.get().read(root, to_bytes(b1));

    if(b1.size < N_ELEMENTS)
    {
        b1.elements[b1.size++] = element;
        alloc.get().write(root, to_bytes(b1));

        return { root, b1.size - 1 };
    }

    block b2 = {
        .size = 1,
        .next = b1.next
    };
    b2.elements[0] = element;

    b1.next = alloc.new_block();
    alloc.get().write(root, to_bytes(b1));
    alloc.get().write(b1.next, to_bytes(b2));

    return { b1.next, 0 };
}

template <typename T, typename BlockAllocator>
void disk_block_list<T, BlockAllocator>::pop(BlockAllocator& alloc, uint32_t& root)
{
    if(!root)
        return;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(b.size > 0)
    {
        --b.size;

        if(b.size > 0)
        {
            alloc.get().write(root, to_bytes(b));
        }
        else if(b.next)
        {
            uint32_t next = b.next;
            alloc.delete_block(root, to_bytes(b));
            root = next;
        }
    }
}

template <typename T, typename BlockAllocator>
bool disk_block_list<T, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, const T& element)
{
    if(!root)
        return false;

    uint32_t prev = root;
    block b_prev = { .next = root };
    block b_cur;

    while(b_prev.next)
    {
        alloc.get().read(b_prev.next, to_bytes(b_cur));

        auto it= std::find(b_cur.elements, b_cur.elements+b_cur.size, element);
        if(it != b_cur.elements + b_cur.size)
        {
            *it = b_cur.elements[--b_cur.size];

            if(b_cur.size > 0)
            {
                alloc.get().write(b_prev.next, to_bytes(b_cur));
            }
            else if(b_prev.next != root)
            {
                uint32_t next = b_cur.next;
                alloc.delete_block(b_prev.next, to_bytes(b_cur));
                b_prev.next = b_cur.next;
                alloc.get().write(prev, to_bytes(b_prev));
            }
            else if(b_cur.next) // is root
            {
                uint32_t next = b_cur.next;
                alloc.delete_block(b_prev.next, to_bytes(b_cur));
                root = next;
            }
            else // is root and has no next
            {
                alloc.get().write(b_prev.next, to_bytes(b_cur));
            }

            return true;
        }

        prev = b_prev.next;
        b_prev = b_cur;
    }

    return false;
}

template <typename T, typename BlockAllocator>
std::vector<T> disk_block_list<T, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, std::vector<T>& elements)
{
    if(!root)
        return {};

    uint32_t prev = root;
    block b_prev = { .next = root };
    block b_cur;

    std::vector<T> removed;
    while(b_prev.next)
    {
        alloc.get().read(b_prev.next, to_bytes(b_cur));

        bool removed_something = false;
        for(size_t i = 0; i < elements.size(); ++i)
        {
            auto it = std::find(
                b_cur.elements, b_cur.elements + b_cur.size, elements[i]);

            if(it != b_cur.elements + b_cur.size)
            {
                removed.push_back(*it);
                *it = b_cur.elements[--b_cur.size];
                elements[i] = elements.back();
                elements.pop_back();
                removed_something = true;
            }
        }

        if(removed_something)
        {
            if(b_cur.size > 0)
            {
                alloc.get().write(b_prev.next, to_bytes(b_cur));
            }
            else if(b_prev.next != root)
            {
                uint32_t next = b_cur.next;
                alloc.delete_block(b_prev.next, to_bytes(b_cur));
                b_prev.next = b_cur.next;
                alloc.get().write(prev, to_bytes(b_prev));
            }
            else if(b_cur.next) // is root
            {
                uint32_t next = b_cur.next;
                alloc.delete_block(b_prev.next, to_bytes(b_cur));
                root = next;
            }
            else // is root and has no next
            {
                alloc.get().write(b_prev.next, to_bytes(b_cur));
            }

            if(elements.empty())
                return removed;
        }

        prev = b_prev.next;
        b_prev = b_cur;
    }

    return removed;
}

template <typename T, typename BlockAllocator>
std::vector<T> disk_block_list<T, BlockAllocator>::remove_all(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return {};

    std::vector<T> removed;

    block b;
    alloc.get().read(root, to_bytes(b));

    removed.insert(removed.end(), b.elements, b.elements + b.size);
    b.size = 0;
    b.next = 0;

    alloc.get().write(root, to_bytes(b));

    while(b.next)
    {
        uint32_t old_next = b.next;
        alloc.get().read(b.next, to_bytes(b));
        removed.insert(removed.end(), b.elements, b.elements + b.size);
        alloc.delete_block(old_next, to_bytes(b));
    }

    return removed;
}

template <typename T, typename BlockAllocator>
T disk_block_list<T, BlockAllocator>::direct_access(BlockAllocator& alloc, uint32_t block_id, size_t pos)
{
    block b;
    alloc.get().read(block_id, to_bytes(b));
    return b.elements[pos];
}

template <typename T, typename BlockAllocator>
cppcoro::generator<T> disk_block_list<T, BlockAllocator>::all(BlockAllocator& alloc, uint32_t root)
{
    block b = { .next = root };

    while(b.next)
    {
        alloc.get().read(b.next, to_bytes(b));

        for(size_t i = 0; i < b.size; ++i)
            co_yield b.elements[i];
    }
}


template <typename T, typename BlockAllocator>
bool disk_block_list<T, BlockAllocator>::exists(BlockAllocator& alloc, uint32_t root, const T& element)
{
    for(auto& e : all(alloc, root))
        if(e == element)
            return true;

    return false;
}

template <typename T, typename BlockAllocator>
size_t disk_block_list<T, BlockAllocator>::size(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return 0;

    size_t count = 0;
    block b = { .next = root };

    while(b.next)
    {
        alloc.get().read(b.next, to_bytes(b));
        count += b.size;
    }

    return count;
}


template <typename T, typename BlockAllocator>
bool disk_block_list<T, BlockAllocator>::empty(BlockAllocator& alloc, uint32_t root)
{
    return size(alloc, root) == 0;
}

template <typename T, typename BlockAllocator>
void disk_block_list<T, BlockAllocator>::debug(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return;

    block b = { .next = root };

    while(b.next)
    {
        alloc.get().read(b.next, to_bytes(b));

        for(size_t i = 0; i < b.size; ++i)
            std::cout << "element #" << i << ": " << b.elements[i] << '\n';

        std::cout << "size: " << b.size << '\n';
        std::cout << "next: " << b.next << '\n';
    }
}

INSTATIATE_LV2_STRUCT_TEMPLATES(disk_block_list)

} /* stg */
