#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreeset.hxx>
#include <libgraph-processing/detail/util.hxx>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <queue>
#include <array>
#include <zlib.h>

namespace stg::detail
{

static const size_t RAW_LENGTH = 1 << 16;
static const size_t EXCESS = 512;
static std::array<std::byte, RAW_LENGTH> buffer;
static const std::byte separator{0b11111111};

enum block_type : uint32_t
{
    internal,
    external
};

struct update_result
{
    bool updated_parent;
    bool succeeded;
};

template <typename Vector>
static size_t sizeof_vector(const Vector& v)
{
    return v.size() * sizeof(typename Vector::value_type);
}

template <typename UBlock>
static size_t sizeof_ublock(const UBlock& ub)
{
    return sizeof(ub.type)
        + sizeof_vector(ub.keys)
        + sizeof_vector(ub.pointers);
}


template <typename Key, size_t N>
static constexpr size_t msize(Key (&)[N]);

template <typename UBlock>
static constexpr size_t n_ikeys(const UBlock& b);

template <typename UBlock>
static constexpr size_t n_ekeys(const UBlock& b);

template <typename Key, typename UBlock>
static size_t find_geq_i(const Key& key, const UBlock& b);

template <typename Key, typename UBlock>
static size_t find_eq_e(const Key& key, const UBlock& b);

template <typename Key, typename UBlock>
static void insert_ord_i(const Key& key, uint32_t value, UBlock& b);

template <typename Key, typename UBlock>
static void insert_ord_e(const Key& key, UBlock& b);

template <typename UBlock>
static uint32_t remove_ord_i(uint32_t i, UBlock& b);

template <typename UBlock>
static void remove_ord_e(uint32_t i, UBlock& b);

template <typename UBlock>
static void split_half_i(UBlock& b1, UBlock& b2);

template <typename UBlock>
static void split_half_e(UBlock& b1, UBlock& b2);

template <typename UBlock, typename Key>
static void share_left_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock, typename Key>
static void share_left_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock, typename Key>
static void share_right_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock, typename Key>
static void share_right_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock>
static void merge_i(UBlock& b1, UBlock& b2);

template <typename UBlock>
static void merge_e(UBlock& b1, UBlock& b2);

template <typename Block,
          typename BlockAllocator, typename UBlock, typename Key>
static update_result insert_rec(BlockAllocator& alloc, uint32_t index, UBlock& parent, const Key& key);

template <typename Block,
          typename BlockAllocator, typename UBlock, typename Key>
static update_result remove_rec(BlockAllocator& alloc, uint32_t index, UBlock& parent, const Key& key, uint32_t& new_root);


template <typename UBlock, typename Block>
void compress(UBlock& ub, Block& b)
{
    std::copy_n(
            reinterpret_cast<const std::byte*>(&ub.type),
            sizeof(ub.type), buffer.data());

    if(!ub.keys.empty())
        std::copy_n(
                reinterpret_cast<const std::byte*>(ub.keys.data()),
                sizeof_vector(ub.keys),
                buffer.data() + sizeof(ub.type));

    // end of keys
    buffer[sizeof(ub.type) + sizeof_vector(ub.keys)] = separator;
    buffer[sizeof(ub.type) + sizeof_vector(ub.keys) + 1] = separator;
    buffer[sizeof(ub.type) + sizeof_vector(ub.keys) + 2] = separator;
    buffer[sizeof(ub.type) + sizeof_vector(ub.keys) + 3] = separator;

    if(!ub.pointers.empty())
        std::copy_n(
                reinterpret_cast<const std::byte*>(ub.pointers.data()),
                sizeof_vector(ub.pointers),
                buffer.data() + sizeof(ub.type) + sizeof_vector(ub.keys) + 4);

    uLongf out_len = PAGE_SIZE;
    ::compress(
            (Bytef*) b.data, &out_len,
            (Bytef*) buffer.data(), sizeof_ublock(ub) + 4);

    b.size = out_len + sizeof(b.size);
}

template <typename Block, typename UBlock>
void uncompress(Block& b, UBlock& ub)
{
    uLongf out_len = RAW_LENGTH;
    ::uncompress(
            (Bytef*) buffer.data(), &out_len,
            (Bytef*) b.data, b.size - sizeof(b.size));

    std::copy_n(
            buffer.data(), sizeof(ub.type),
            reinterpret_cast<std::byte*>(&ub.type));


    size_t mark_pos = sizeof(ub.type);

    while(true)
    {
        while(buffer[mark_pos] != separator)
            ++mark_pos;

        if(buffer[mark_pos + 1] == separator
        && buffer[mark_pos + 2] == separator
        && buffer[mark_pos + 3] == separator)
        {
            break;
        }

        ++mark_pos;
    }

    if(out_len > sizeof(ub.type) && mark_pos > sizeof(ub.type))
    {
        using vector_t = decltype(ub.keys);
        using value_t = typename vector_t::value_type;

        ub.keys = vector_t(
                (value_t*) (buffer.data() + sizeof(ub.type)),
                (value_t*) (buffer.data() + mark_pos));
    }

    if(out_len > sizeof(ub.type) + sizeof_vector(ub.keys) + 4)
    {
        using vector_t = decltype(ub.pointers);
        using value_t = typename vector_t::value_type;

        ub.pointers = vector_t(
                (value_t*) (buffer.data() + mark_pos + 4),
                (value_t*) (buffer.data() + out_len));
    }
}

template <typename Key, typename BlockAllocator>
uint32_t zlib_disk_block_btreeset<Key, BlockAllocator>::create(BlockAllocator& alloc)
{
    ublock ub = { .type = block_type::external };
    ub.pointers.push_back(0);

    block b;
    compress(ub, b);

    uint32_t id = alloc.new_block();
    alloc.get().write(id, to_bytes(b));

    return id;
}

template <typename Key, typename BlockAllocator>
void zlib_disk_block_btreeset<Key, BlockAllocator>::destroy(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return;

    block b;
    alloc.get().read(root, to_bytes(b));

    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::internal)
        for(auto& pointer : ub.pointers)
            destroy(alloc, pointer);

    alloc.delete_block(root, to_bytes(b));
}


template <typename Key, typename BlockAllocator>
bool zlib_disk_block_btreeset<Key, BlockAllocator>::insert(BlockAllocator& alloc, uint32_t& root, const Key& key)
{
    if(!root)
        return false;

    block b1;
    alloc.get().read(root, to_bytes(b1));

    ublock ub1;
    uncompress(b1, ub1);

    if(ub1.type == block_type::external)
    {
        /* if(find_eq_e(key, b) < b.size) */
        /*     return false; */

        if(b1.size < PAGE_SIZE - 2*sizeof(Key))
        {
            insert_ord_e(key, ub1);
            compress(ub1, b1);
            alloc.get().write(root, to_bytes(b1));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b;
            ublock new_ub = { .type = block_type::external };

            split_half_e(ub1, new_ub);

            insert_ord_e(key, key < new_ub.keys[0] ? ub1 : new_ub);
            new_ub.pointers.push_back(ub1.pointers.back());
            ub1.pointers.back() = new_b_id;

            compress(ub1, b1);
            compress(new_ub, new_b);

            alloc.get().write(root, to_bytes(b1));
            alloc.get().write(new_b_id, to_bytes(new_b));

            uint32_t new_root_id = alloc.new_block();
            block new_root;
            ublock new_uroot = { .type = block_type::internal };

            new_uroot.keys.push_back(new_ub.keys[0]);
            new_uroot.pointers.push_back(root);
            new_uroot.pointers.push_back(new_b_id);
            root = new_root_id;

            compress(new_uroot, new_root);

            alloc.get().write(new_root_id, to_bytes(new_root));
        }

        return true;
    }

    update_result res = { .succeeded = false };

    if((res = insert_rec<block>(alloc, find_geq_i(key, ub1), ub1, key))
            .updated_parent)
    {

        if(b1.size < PAGE_SIZE - 2*sizeof(Key))
        {
            compress(ub1, b1);
            alloc.get().write(root, to_bytes(b1));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b;
            ublock new_ub = { .type = block_type::internal };

            split_half_i(ub1, new_ub);

            // need this
            Key parent_key = ub1.keys.back();
            ub1.keys.pop_back();

            compress(ub1, b1);
            compress(new_ub, new_b);

            alloc.get().write(root, to_bytes(b1));
            alloc.get().write(new_b_id, to_bytes(new_b));

            uint32_t new_root_id = alloc.new_block();
            block new_root;
            ublock new_uroot = { .type = block_type::internal };

            new_uroot.keys.push_back(parent_key);
            new_uroot.pointers.push_back(root);
            new_uroot.pointers.push_back(new_b_id);
            root = new_root_id;

            compress(new_uroot, new_root);

            alloc.get().write(new_root_id, to_bytes(new_root));
        }
    }

    return res.succeeded;
}


template <typename Key, typename BlockAllocator>
bool zlib_disk_block_btreeset<Key, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, const Key& key)
{
    if(!root)
        return false;

    block b1;
    alloc.get().read(root, to_bytes(b1));
    ublock ub1;
    uncompress(b1, ub1);

    if(ub1.type == block_type::external)
    {
        if(ub1.keys.empty())
            return false;

        size_t i = find_eq_e(key, ub1);
        if(i == ub1.keys.size())
            return false;

        remove_ord_e(i, ub1);
        compress(ub1, b1);

        alloc.get().write(root, to_bytes(b1));

        return true;
    }

    uint32_t new_root = root;
    update_result res = { .succeeded = false };

    if((res = remove_rec<block>(
            alloc, find_geq_i(key, ub1), ub1, key, new_root)).updated_parent)
    {
        if(!ub1.keys.empty())
        {
            compress(ub1, b1);
            alloc.get().write(root, to_bytes(b1));
        }
        else
        {
            alloc.delete_block(root);
        }

        if(new_root != root)
            root = new_root;
    }

    return res.succeeded;
}

template <typename Key, typename BlockAllocator>
std::vector<Key> zlib_disk_block_btreeset<Key, BlockAllocator>::remove(BlockAllocator& alloc,uint32_t& root, const std::vector<Key>& keys)
{
    std::vector<Key> res;

    for(auto& key : keys)
        if(remove(alloc, root, key))
            res.push_back(key);

    return res;
}

template <typename Key, typename BlockAllocator>
std::vector<Key> zlib_disk_block_btreeset<Key, BlockAllocator>::remove_all(BlockAllocator& alloc, uint32_t& root, uint32_t first_leaf)
{
    std::vector<Key> keys;

    for(auto key : all(alloc, first_leaf))
        keys.push_back(key);

    return remove(alloc, root, keys);
}

template <typename Key, typename BlockAllocator>
cppcoro::generator<Key> zlib_disk_block_btreeset<Key, BlockAllocator>::all(BlockAllocator& alloc, uint32_t first_leaf)
{
    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        ublock ub;
        uncompress(b, ub);

        for(auto& key : ub.keys)
            co_yield key;

        next_block = ub.pointers.back();
    }
}

template <typename Key, typename BlockAllocator>
bool zlib_disk_block_btreeset<Key, BlockAllocator>::exists(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
        return find_eq_e(key, ub) < ub.keys.size();

    return exists(alloc, ub.pointers[find_geq_i(key, ub)], key);
}

template <typename Key, typename BlockAllocator>
size_t zlib_disk_block_btreeset<Key, BlockAllocator>::size(BlockAllocator& alloc, uint32_t first_leaf)
{
    size_t size = 0;
    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        ublock ub;
        uncompress(b, ub);
        next_block = ub.pointers.back();

        size += ub.keys.size();
    }

    return size;
}

template <typename Key, typename BlockAllocator>
bool zlib_disk_block_btreeset<Key, BlockAllocator>::empty(BlockAllocator& alloc, uint32_t first_leaf)
{
    return size(alloc, first_leaf) == 0;
}

template <typename Key, typename BlockAllocator>
void zlib_disk_block_btreeset<Key, BlockAllocator>::debug(BlockAllocator& alloc, uint32_t root, uint32_t first_leaf)
{
    std::cout << "~~INTERNAL~~\n";

    std::queue<uint32_t> ids;
    ids.push(root);

    while(!ids.empty())
    {
        uint32_t id = ids.front(); ids.pop();
        block b;
        alloc.get().read(id, to_bytes(b));
        ublock ub;
        uncompress(b, ub);

        if(ub.type == block_type::internal)
        {
            std::cout << "internal block at " << id << '\n';

            for(size_t i = 0; i < ub.keys.size(); ++i)
                std::cout
                    << "(" << ub.pointers[i] << "*) | "
                    << ub.keys[i] << " | ";

            std::cout << "(" << ub.pointers.back() << "*)\n";

            if(ub.pointers.back())
                ids.push(ub.pointers.back());
        }
    }

    std::cout << "~~EXTERNAL~~\n";

    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        ublock ub;
        uncompress(b, ub);

        std::cout << "external block at " << next_block << '\n';

        for(size_t i = 0; i < ub.keys.size(); ++i)
            std::cout << ub.keys[i] << " | ";

        std::cout << "(" << ub.pointers.back() << "*)\n";

        next_block = ub.pointers.back();
    }
}

template <typename Block, typename BlockAllocator, typename UBlock, typename Key>
update_result insert_rec(BlockAllocator& alloc, uint32_t index, UBlock& parent, const Key& key)
{
    if(!parent.pointers[index])
        return { .updated_parent = false, .succeeded = false };

    Block b1;
    alloc.get().read(parent.pointers[index], to_bytes(b1));
    UBlock ub1;
    uncompress(b1, ub1);

    if(ub1.type == block_type::external)
    {
        /* if(find_eq(key, b) < b.size) */
        /*     return res; */

        if(b1.size < PAGE_SIZE - 2*sizeof(Key))
        {
            insert_ord_e(key, ub1);
            compress(ub1, b1);
            alloc.get().write(parent.pointers[index], to_bytes(b1));

            return { .updated_parent = false, .succeeded = true };
        }

        uint32_t new_b_id = alloc.new_block();
        Block new_b;
        UBlock new_ub { .type = block_type::external };

        split_half_e(ub1, new_ub);

        insert_ord_e(key, key < new_ub.keys[0] ? ub1 : new_ub);
        new_ub.pointers.push_back(ub1.pointers.back());
        ub1.pointers.back() = new_b_id;

        compress(ub1, b1);
        compress(new_ub, new_b);

        alloc.get().write(parent.pointers[index], to_bytes(b1));
        alloc.get().write(new_b_id, to_bytes(new_b));

        insert_ord_i(new_ub.keys[0], new_b_id, parent);

        return { .updated_parent = true, .succeeded = true };
    }

    update_result res = { .updated_parent = false, .succeeded = false };

    if((res = insert_rec<Block>(alloc, find_geq_i(key, ub1), ub1 , key))
            .updated_parent)
    {

        if(b1.size < PAGE_SIZE - 2*sizeof(Key))
        {
            compress(ub1, b1);
            alloc.get().write(parent.pointers[index], to_bytes(b1));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            Block new_b;
            UBlock new_ub = { .type = block_type::internal };

            split_half_i(ub1, new_ub);

            // need this
            Key parent_key = ub1.keys.back();
            ub1.keys.pop_back();

            compress(ub1, b1);
            compress(new_ub, new_b);

            alloc.get().write(parent.pointers[index], to_bytes(b1));
            alloc.get().write(new_b_id, to_bytes(new_b));

            insert_ord_i(parent_key, new_b_id, parent);

            res.updated_parent = true;
        }
    }

    return res;
}

template <typename Block, typename BlockAllocator, typename UBlock, typename Key>
update_result remove_rec(BlockAllocator& alloc, uint32_t index, UBlock& parent, const Key& key, uint32_t& root)
{
    size_t b_id = parent.pointers[index];
    if(!b_id)
        return { .updated_parent = false, .succeeded = false };

    Block b1;
    alloc.get().read(b_id, to_bytes(b1));
    UBlock ub1;
    uncompress(b1, ub1);

    if(ub1.type == block_type::external)
    {
        if(ub1.keys.empty())
            return { .updated_parent = false, .succeeded = false };

        size_t i = find_eq_e(key, ub1);
        if(i == ub1.keys.size())
            return { .updated_parent = false, .succeeded = false };

        remove_ord_e(i, ub1);
        compress(ub1, b1);

        if(b1.size >= (PAGE_SIZE - EXCESS) / 2)
        {
            alloc.get().write(b_id, to_bytes(b1));
            return { .updated_parent = false, .succeeded = true };
        }

        // try borrow from left
        if(index > 0)
        {
            uint32_t s_left_id = parent.pointers[index - 1];
            Block s_left;
            alloc.get().read(s_left_id, to_bytes(s_left));

            if(s_left.size > (PAGE_SIZE + EXCESS) / 2)
            {
                UBlock s_uleft;
                uncompress(s_left, s_uleft);

                share_left_e(s_uleft, s_left.size, ub1, b1.size,
                        parent.keys[index - 1]);

                compress(ub1, b1);
                compress(s_uleft, s_left);

                alloc.get().write(b_id, to_bytes(b1));
                alloc.get().write(s_left_id, to_bytes(s_left));

                return { .updated_parent = true, .succeeded = true };
            }
        }

        // try borrow from right
        if(index < parent.pointers.size() - 1)
        {
            uint32_t s_right_id = parent.pointers[index + 1];
            Block s_right;
            alloc.get().read(s_right_id, to_bytes(s_right));

            if(s_right.size > (PAGE_SIZE + EXCESS) / 2)
            {
                UBlock s_uright;
                uncompress(s_right, s_uright);

                share_right_e(ub1, b1.size, s_uright, s_right.size,
                        parent.keys[index]);

                compress(ub1, b1);
                compress(s_uright, s_right);

                alloc.get().write(b_id, to_bytes(b1));
                alloc.get().write(s_right_id, to_bytes(s_right));

                return { .updated_parent = true, .succeeded = true };
            }
        }

        // try merge with left
        if(index > 0)
        {
            uint32_t s_left_id = parent.pointers[index - 1];
            Block s_left;
            alloc.get().read(s_left_id, to_bytes(s_left));

            UBlock s_uleft;
            uncompress(s_left, s_uleft);

            merge_e(s_uleft, ub1);
            remove_ord_i(index - 1, parent);

            if(parent.keys.empty())
                root = s_left_id;

            compress(s_uleft, s_left);

            alloc.get().write(s_left_id, to_bytes(s_left));
            alloc.delete_block(b_id, to_bytes(b1));

            return { .updated_parent = true, .succeeded = true };
        }

        // try merge with right
        if(index < parent.pointers.size() - 1)
        {
            uint32_t s_right_id = parent.pointers[index + 1];
            Block s_right;
            alloc.get().read(s_right_id, to_bytes(s_right));

            UBlock s_uright;
            uncompress(s_right, s_uright);

            merge_e(ub1, s_uright);
            remove_ord_i(index, parent);

            if(parent.keys.empty())
                root = b_id;

            compress(ub1, b1);

            alloc.get().write(b_id, to_bytes(b1));
            alloc.delete_block(s_right_id, to_bytes(s_right));

            return { .updated_parent = true, .succeeded = true };
        }

        return { .updated_parent = false, .succeeded = false };
    }

    update_result res = { .updated_parent = false, .succeeded = false };

    if((res = remove_rec<Block>(
            alloc, find_geq_i(key, ub1), ub1, key, root)).updated_parent)
    {
        compress(ub1, b1);

        if(b1.size >= (PAGE_SIZE - EXCESS) / 2)
        {
            alloc.get().write(b_id, to_bytes(b1));
        }
        else
        {
            // try borrow from left
            if(index > 0)
            {
                uint32_t s_left_id = parent.pointers[index - 1];
                Block s_left;
                alloc.get().read(s_left_id, to_bytes(s_left));

                if(s_left.size > (PAGE_SIZE + EXCESS) / 2)
                {
                    UBlock s_uleft;
                    uncompress(s_left, s_uleft);

                    share_left_i(s_uleft, s_left.size, ub1, b1.size,
                            parent.keys[index - 1]);

                    compress(ub1, b1);
                    compress(s_uleft, s_left);

                    alloc.get().write(b_id, to_bytes(b1));
                    alloc.get().write(s_left_id, to_bytes(s_left));

                    res.updated_parent = true;
                    return res;
                }
            }

            // try borrow from right
            if(index < parent.pointers.size() - 1)
            {
                uint32_t s_right_id = parent.pointers[index + 1];
                Block s_right;
                alloc.get().read(s_right_id, to_bytes(s_right));

                if(s_right.size > (PAGE_SIZE + EXCESS) / 2)
                {
                    UBlock s_uright;
                    uncompress(s_right, s_uright);

                    share_right_i(ub1, b1.size, s_uright, s_right.size,
                            parent.keys[index]);

                    compress(ub1, b1);
                    compress(s_uright, s_right);

                    alloc.get().write(b_id, to_bytes(b1));
                    alloc.get().write(s_right_id, to_bytes(s_right));

                    res.updated_parent = true;
                    return res;
                }
            }

            // try merge with left
            if(index > 0)
            {
                uint32_t s_left_id = parent.pointers[index - 1];
                Block s_left;
                alloc.get().read(s_left_id, to_bytes(s_left));

                UBlock s_uleft;
                uncompress(s_left, s_uleft);

                s_uleft.keys.push_back(parent.keys[index - 1]);

                merge_i(s_uleft, ub1);
                remove_ord_i(index - 1, parent);

                if(parent.keys.empty())
                    root = s_left_id;

                compress(s_uleft, s_left);

                alloc.get().write(s_left_id, to_bytes(s_left));
                alloc.delete_block(b_id, to_bytes(b1));

                res.updated_parent = true;
                return res;
            }

            // try merge with right
            if(index < parent.pointers.size() - 1)
            {
                uint32_t s_right_id = parent.pointers[index + 1];
                Block s_right;
                alloc.get().read(s_right_id, to_bytes(s_right));

                UBlock s_uright;
                uncompress(s_right, s_uright);

                ub1.keys.push_back(parent.keys[index]);

                merge_i(ub1, s_uright);
                remove_ord_i(index, parent);

                if(parent.keys.empty())
                    root = b_id;

                compress(ub1, b1);

                alloc.get().write(b_id, to_bytes(b1));
                alloc.delete_block(s_right_id, to_bytes(s_right));

                res.updated_parent = true;
                return res;
            }
        }
    }

    return res;
}

template <typename Key, typename UBlock>
size_t find_geq_i(const Key& key, const UBlock& b)
{
    auto it = std::lower_bound(b.keys.begin(), b.keys.end(), key);
    size_t i =  std::distance(b.keys.begin(), it);

    if(b.type == block_type::internal && it != b.keys.end() && *it == key)
        ++i;

    return i;
}

template <typename Key, typename UBlock>
size_t find_eq_e(const Key& key, const UBlock& b)
{
    auto it = std::lower_bound(b.keys.begin(), b.keys.end(), key);

    if(it == b.keys.end() || *it != key)
        return b.keys.size();

    return std::distance(b.keys.begin(), it);
}

template <typename Key, typename UBlock>
void insert_ord_i(const Key& key, uint32_t value, UBlock& b)
{
    b.keys.resize(b.keys.size() + 1);
    b.pointers.resize(b.pointers.size() + 1);

    size_t i = b.keys.size() - 1;
    while(i > 0 && key < b.keys[i - 1])
    {
        b.keys[i] = b.keys[i - 1];
        b.pointers[i + 1] = b.pointers[i];
        --i;
    }

    b.keys[i] = key;
    b.pointers[i + 1] = value;
}

template <typename Key, typename UBlock>
void insert_ord_e(const Key& key, UBlock& b)
{
    b.keys.resize(b.keys.size() + 1);

    size_t i = b.keys.size() - 1;
    while(i > 0 && key < b.keys[i - 1])
    {
        b.keys[i] = b.keys[i - 1];
        --i;
    }

    b.keys[i] = key;
}

template <typename UBlock>
uint32_t remove_ord_i(uint32_t i, UBlock& b)
{
    uint32_t pointer = b.pointers[i];

    while(i < b.keys.size() - 1)
    {
        b.keys[i] = b.keys[i + 1];
        b.pointers[i + 1] = b.pointers[i + 2];
        ++i;
    }

    b.keys.pop_back();
    b.pointers.pop_back();

    return pointer;
}

template <typename UBlock>
void remove_ord_e(uint32_t i, UBlock& b)
{
    while(i < b.keys.size() - 1)
    {
        b.keys[i] = b.keys[i + 1];
        ++i;
    }

    b.keys.pop_back();
}

template <typename UBlock>
void split_half_i(UBlock& b1, UBlock& b2)
{
    size_t mid = b1.keys.size() / 2;

    b2.keys.reserve(b2.keys.size() + b1.keys.size() - (mid + 1));
    b2.pointers.reserve(b2.pointers.size() + b1.pointers.size() - mid);

    b2.pointers.push_back(b1.pointers[mid + 1]);

    for(size_t i = mid + 1; i < b1.keys.size(); ++i)
    {
        b2.keys.push_back(b1.keys[i]);
        b2.pointers.push_back(b1.pointers[i + 1]);
    }

    b1.keys.resize(mid + 1);
    b1.pointers.resize(mid + 1);
}

template <typename UBlock>
void split_half_e(UBlock& b1, UBlock& b2)
{
    size_t mid = b1.keys.size() / 2;

    b2.keys.reserve(b2.keys.size() + b1.keys.size() - mid);

    for(size_t i = mid; i < b1.keys.size(); ++i)
        b2.keys.push_back(b1.keys[i]);

    b1.keys.resize(mid);
}


template <typename UBlock, typename Key>
void share_left_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b1_size - half) / (b1_size / b1.keys.size());

    b1.keys.push_back(parent_key);

    b2.keys.insert(b2.keys.begin(),
            b1.keys.end() - to_pass, b1.keys.end());
    b1.keys.resize(b1.keys.size() - to_pass);

    b2.pointers.insert(b2.pointers.begin(),
            b1.pointers.end() - to_pass, b1.pointers.end());
    b1.pointers.resize(b1.pointers.size() - to_pass);

    parent_key = b1.keys.back();
    b1.keys.pop_back();
}

template <typename UBlock, typename Key>
void share_left_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b1_size - half) / (b1_size / b1.keys.size());

    b2.keys.insert(b2.keys.begin(),
            b1.keys.end() - to_pass, b1.keys.end());
    b1.keys.resize(b1.keys.size() - to_pass);

    parent_key = b2.keys[0];
}

template <typename UBlock, typename Key>
void share_right_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b2_size - half) / (b2_size / b2.keys.size());

    b1.keys.push_back(parent_key);

    b1.keys.insert(b1.keys.end(),
            b2.keys.begin(), b2.keys.begin() + to_pass);
    b2.keys.erase(b2.keys.begin(), b2.keys.begin() + to_pass);

    b1.pointers.insert(b1.pointers.end(),
            b2.pointers.begin(), b2.pointers.begin() + to_pass);
    b2.pointers.erase(b2.pointers.begin(), b2.pointers.begin() + to_pass);

    parent_key = b1.keys.back();
    b1.keys.pop_back();
}

template <typename UBlock, typename Key>
void share_right_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b2_size - half) / (b2_size / b2.keys.size());

    b1.keys.insert(b1.keys.end(),
            b2.keys.begin(), b2.keys.begin() + to_pass);
    b2.keys.erase(b2.keys.begin(), b2.keys.begin() + to_pass);

    parent_key = b2.keys[0];
}


template <typename UBlock>
void merge_i(UBlock& b1, UBlock& b2)
{
    b1.keys.insert(b1.keys.end(), b2.keys.begin(), b2.keys.end());
    b1.pointers.insert(b1.pointers.end(),
            b2.pointers.begin(), b2.pointers.end());
}

template <typename UBlock>
void merge_e(UBlock& b1, UBlock& b2)
{
    b1.keys.insert(b1.keys.end(), b2.keys.begin(), b2.keys.end());
    b1.pointers[0] = b2.pointers[0];
}

template <typename Key, size_t N>
static constexpr size_t msize(Key (&)[N])
{
    return N;
}

template <typename UBlock>
static constexpr size_t n_ikeys(const UBlock& b)
{
    return msize(b.i.keys) - 1;
}

template <typename UBlock>
static constexpr size_t n_ekeys(const UBlock& b)
{
    return msize(b.e.keys) - 1;
}

INSTATIATE_LV2_STRUCT_TEMPLATES(zlib_disk_block_btreeset)

} /* stg */

