#include <libgraph-processing/detail/util.hxx>

namespace stg::detail
{

template <typename T, typename BlockAllocator>
template <typename Predicate>
std::vector<T> disk_block_list<T, BlockAllocator>::remove_if(BlockAllocator& alloc, uint32_t& root, Predicate pred)
{
    if(!root)
        return {};

    uint32_t prev = root;
    block b_prev = { .next = root };
    block b_cur;

    std::vector<T> removed;
    while(b_prev.next)
    {
        alloc.get().read(b_prev.next, to_bytes(b_cur));

        bool removed_something = false;

        for(size_t i = 0; i < b_cur.size; ++i)
        {
            if(pred(b_cur.elements[i]))
            {
                removed.push_back(b_cur.elements[i]);
                b_cur.elements[i] = b_cur.elements[--b_cur.size];
                removed_something = true;
            }
        }

        if(removed_something)
        {
            if(b_cur.size > 0)
            {
                alloc.get().write(b_prev.next, to_bytes(b_cur));
            }
            else if(b_prev.next != root)
            {
                uint32_t next = b_cur.next;
                alloc.delete_block(b_prev.next, to_bytes(b_cur));
                b_prev.next = b_cur.next;
                alloc.get().write(prev, to_bytes(b_prev));
            }
            else if(b_cur.next) // is root
            {
                uint32_t next = b_cur.next;
                alloc.delete_block(b_prev.next, to_bytes(b_cur));
                root = next;
            }
            else // is root and has no next
            {
                alloc.get().write(b_prev.next, to_bytes(b_cur));
            }
        }

        prev = b_prev.next;
        b_prev = b_cur;
    }

    return removed;
}

template <typename T, typename BlockAllocator>
template <typename UpdateFunction>
void disk_block_list<T, BlockAllocator>::try_update(BlockAllocator& alloc, uint32_t root, const T& element, UpdateFunction update)
{
    size_t id = root;
    while(id)
    {
        block b;
        alloc.get().read(id, to_bytes(b));

        for(size_t i = 0; i < b.size; ++i)
        {
            if(b.elements[i] == element)
            {
                update(b.elements[i]);
                alloc.get().write(id, to_bytes(b));

                return;
            }
        }

        id = b.next;
    }
}

template <typename T, typename BlockAllocator>
template <typename UpdateFunction>
void disk_block_list<T, BlockAllocator>::direct_update(BlockAllocator& alloc, uint32_t block_id, UpdateFunction update)
{
    block b;
    alloc.get().read(block_id, to_bytes(b));

    for(size_t i = 0; i < b.size; ++i)
    {
        if(update(b.elements[i]))
        {
            alloc.get().write(block_id, to_bytes(b));
            return;
        }
    }
}


template <typename T, typename BlockAllocator>
template <typename UpdateFunction>
void disk_block_list<T, BlockAllocator>::direct_update(BlockAllocator& alloc, uint32_t block_id, size_t pos, UpdateFunction update)
{
    block b;
    alloc.get().read(block_id, to_bytes(b));
    update(b.elements[pos]);
    alloc.get().write(block_id, to_bytes(b));
}


} /* stg */
