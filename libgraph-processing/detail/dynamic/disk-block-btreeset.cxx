#include <libgraph-processing/detail/dynamic/disk-block-btreeset.hxx>
#include <libgraph-processing/detail/util.hxx>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <queue>

namespace stg::detail
{

enum block_type : uint32_t
{
    internal,
    external
};

struct update_result
{
    bool updated_parent;
    bool succeeded;
};


template <typename Key, size_t N>
static constexpr size_t msize(Key (&)[N]);

template <typename Block>
static constexpr size_t n_ikeys(const Block& b);

template <typename Block>
static constexpr size_t n_ekeys(const Block& b);

template <typename Key, typename Block>
static size_t find_geq_i(const Key& key, const Block& b);

template <typename Key, typename Block>
static size_t find_eq_e(const Key& key, const Block& b);

template <typename Key, typename Block>
static void insert_ord_i(const Key& key, uint32_t value, Block& b);

template <typename Key, typename Block>
static void insert_ord_e(const Key& key, Block& b);

template <typename Block>
static uint32_t remove_ord_i(uint32_t i, Block& b);

template <typename Block>
static void remove_ord_e(uint32_t i, Block& b);

template <typename Block>
static void split_half_i(Block& b1, Block& b2);

template <typename Block>
static void split_half_e(Block& b1, Block& b2);

template <typename Block, typename Key>
static void share_left_i(Block& b1, Block& b2, Key& parent_key);

template <typename Block, typename Key>
static void share_left_e(Block& b1, Block& b2, Key& parent_key);

template <typename Block, typename Key>
static void share_right_i(Block& b1, Block& b2, Key& parent_key);

template <typename Block, typename Key>
static void share_right_e(Block& b1, Block& b2, Key& parent_key);

template <typename Block>
static void merge_i(Block& b1, Block& b2);

template <typename Block>
static void merge_e(Block& b1, Block& b2);

template <typename BlockAllocator, typename Block, typename Key>
static update_result insert_rec(BlockAllocator& alloc, uint32_t index, Block& parent, const Key& key);

template <typename BlockAllocator, typename Block, typename Key>
static update_result remove_rec(BlockAllocator& alloc, uint32_t index, Block& parent, const Key& key, uint32_t& new_root);

template <typename Key, typename BlockAllocator>
uint32_t disk_block_btreeset<Key, BlockAllocator>::create(BlockAllocator& alloc)
{
    block root = {
        .type = block_type::external,
        .e.size = 0,
        .e.pointer = 0
    };

    uint32_t root_id = alloc.new_block();
    alloc.get().write(root_id, to_bytes(root));

    return root_id;
}

template <typename Key, typename BlockAllocator>
void disk_block_btreeset<Key, BlockAllocator>::destroy(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(b.type == block_type::internal)
        for(size_t i = 0; i < N_IKEYS + 1; ++i)
            destroy(alloc, b.i.pointers[i]);

    alloc.delete_block(root, to_bytes(b));
}


template <typename Key, typename BlockAllocator>
bool disk_block_btreeset<Key, BlockAllocator>::insert(BlockAllocator& alloc, uint32_t& root, const Key& key)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(b.type == block_type::external)
    {
        /* if(find_eq_e(key, b) < b.size) */
        /*     return false; */

        if(b.e.size < N_EKEYS)
        {
            insert_ord_e(key, b);
            alloc.get().write(root, to_bytes(b));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b = {
                .type = block_type::external,
                .e.size = 0,
                .e.pointer = 0
            };

            split_half_e(b, new_b);

            insert_ord_e(key, key < new_b.e.keys[0] ? b : new_b);
            new_b.e.pointer = b.e.pointer;
            b.e.pointer = new_b_id;

            alloc.get().write(root, to_bytes(b));
            alloc.get().write(new_b_id, to_bytes(new_b));

            uint32_t new_root_id = alloc.new_block();
            block new_root = {
                .type = block_type::internal,
                .i.size = 1
            };

            new_root.i.keys[0] = new_b.e.keys[0];
            new_root.i.pointers[0] = root;
            new_root.i.pointers[1] = new_b_id;
            root = new_root_id;

            alloc.get().write(new_root_id, to_bytes(new_root));
        }

        return true;
    }

    update_result res = { .succeeded = false };

    if((res = insert_rec(alloc, find_geq_i(key, b), b, key)).updated_parent)
    {
        if(b.i.size < N_IKEYS)
        {
            alloc.get().write(root, to_bytes(b));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b = {
                .type = block_type::internal,
                .i.size = 0
            };

            split_half_i(b, new_b);

            alloc.get().write(root, to_bytes(b));
            alloc.get().write(new_b_id, to_bytes(new_b));

            uint32_t new_root_id = alloc.new_block();
            block new_root = {
                .type = block_type::internal,
                .i.size = 1
            };

            new_root.i.keys[0] = b.i.keys[b.i.size];
            new_root.i.pointers[0] = root;
            new_root.i.pointers[1] = new_b_id;
            root = new_root_id;

            alloc.get().write(new_root_id, to_bytes(new_root));
        }
    }

    return res.succeeded;
}

template <typename Key, typename BlockAllocator>
bool disk_block_btreeset<Key, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, const Key& key)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    if(b.type == block_type::external)
    {
        if(b.e.size == 0)
            return false;

        size_t i = find_eq_e(key, b);
        if(i == b.e.size)
            return false;

        remove_ord_e(i, b);
        alloc.get().write(root, to_bytes(b));

        return true;
    }

    uint32_t new_root = root;
    update_result res = { .succeeded = false };


    if((res = remove_rec(alloc, find_geq_i(key, b), b, key, new_root))
             .updated_parent)
    {
        if(b.i.size > 0)
        {
            alloc.get().write(root, to_bytes(b));
        }
        else
        {
            alloc.delete_block(root);
        }

        if(new_root != root)
            root = new_root;
    }

    return res.succeeded;
}

template <typename Key, typename BlockAllocator>
std::vector<Key> disk_block_btreeset<Key, BlockAllocator>::remove(BlockAllocator& alloc,uint32_t& root, const std::vector<Key>& keys)
{
    std::vector<Key> res;

    for(auto& key : keys)
        if(remove(alloc, root, key))
            res.push_back(key);

    return res;
}

template <typename Key, typename BlockAllocator>
std::vector<Key> disk_block_btreeset<Key, BlockAllocator>::remove_all(BlockAllocator& alloc, uint32_t& root, uint32_t first_leaf)
{
    std::vector<Key> keys;

    for(auto key : all(alloc, first_leaf))
        keys.push_back(key);

    return remove(alloc, root, keys);
}

template <typename Key, typename BlockAllocator>
cppcoro::generator<Key> disk_block_btreeset<Key, BlockAllocator>::all(BlockAllocator& alloc, uint32_t first_leaf)
{
    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));

        size_t i = 0;
        while(i < b.e.size)
            co_yield b.e.keys[i++];

        next_block = b.e.pointer;
    }
}

template <typename Key, typename BlockAllocator>
bool disk_block_btreeset<Key, BlockAllocator>::exists(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));

    if(b.type == block_type::external)
        return find_eq_e(key, b) < b.e.size;

    return exists(alloc, b.i.pointers[find_geq_i(key, b)], key);
}

template <typename Key, typename BlockAllocator>
size_t disk_block_btreeset<Key, BlockAllocator>::size(BlockAllocator& alloc, uint32_t first_leaf)
{
    size_t size = 0;
    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        next_block = b.e.pointer;

        size += b.e.size;
    }

    return size;
}

template <typename Key, typename BlockAllocator>
bool disk_block_btreeset<Key, BlockAllocator>::empty(BlockAllocator& alloc, uint32_t first_leaf)
{
    return size(alloc, first_leaf) == 0;
}

template <typename Key, typename BlockAllocator>
void disk_block_btreeset<Key, BlockAllocator>::debug(BlockAllocator& alloc, uint32_t root, uint32_t first_leaf)
{
    std::cout << "~~INTERNAL~~\n";

    std::queue<uint32_t> ids;
    ids.push(root);

    while(!ids.empty())
    {
        uint32_t id = ids.front(); ids.pop();
        block b;
        alloc.get().read(id, to_bytes(b));

        if(b.type == block_type::internal)
        {
            std::cout << "internal block at " << id << '\n';

            for(size_t i = 0; i < b.i.size; ++i)
                std::cout
                    << "(" << b.i.pointers[i] << "*) | "
                    << b.i.keys[i] << " | ";

            std::cout << "(" << b.i.pointers[b.i.size] << "*)\n";

            if(b.i.pointers[b.i.size])
                ids.push(b.i.pointers[b.i.size]);
        }
    }

    std::cout << "~~EXTERNAL~~\n";

    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));

        std::cout << "external block at " << next_block << '\n';

        for(size_t i = 0; i < b.e.size; ++i)
            std::cout << b.e.keys[i] << " | ";

        std::cout << "(" << b.e.pointer << "*)\n";

        next_block = b.e.pointer;
    }
}

template <typename BlockAllocator, typename Block, typename Key>
update_result insert_rec(BlockAllocator& alloc, uint32_t index, Block& parent, const Key& key)
{
    if(!parent.i.pointers[index])
        return { .updated_parent = false, .succeeded = false };

    Block b;
    alloc.get().read(parent.i.pointers[index], to_bytes(b));
    if(b.type == block_type::external)
    {
        /* if(find_eq(key, b) < b.size) */
        /*     return res; */

        if(b.e.size < n_ekeys(b))
        {
            insert_ord_e(key, b);
            alloc.get().write(parent.i.pointers[index], to_bytes(b));

            return { .updated_parent = false, .succeeded = true };
        }

        uint32_t new_b_id = alloc.new_block();
        Block new_b = {
            .type = block_type::external,
            .e.size = 0,
            .e.pointer = 0
        };

        split_half_e(b, new_b);

        insert_ord_e(key, key < new_b.e.keys[0] ? b : new_b);
        new_b.e.pointer = b.e.pointer;
        b.e.pointer = new_b_id;

        alloc.get().write(parent.i.pointers[index], to_bytes(b));
        alloc.get().write(new_b_id, to_bytes(new_b));

        insert_ord_i(new_b.e.keys[0], new_b_id, parent);

        return { .updated_parent = true, .succeeded = true };
    }

    update_result res = { .updated_parent = false, .succeeded  = false };

    if((res = insert_rec(alloc, find_geq_i(key, b), b, key)).updated_parent)
    {
        if(b.i.size < n_ikeys(b))
        {
            alloc.get().write(parent.i.pointers[index], to_bytes(b));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            Block new_b = {
                .type = block_type::internal,
                .i.size = 0
            };

            split_half_i(b, new_b);

            alloc.get().write(parent.i.pointers[index], to_bytes(b));
            alloc.get().write(new_b_id, to_bytes(new_b));

            insert_ord_i(b.i.keys[b.i.size], new_b_id, parent);

            res.updated_parent = true;
        }
    }

    return res;
}

template <typename BlockAllocator, typename Block, typename Key>
update_result remove_rec(BlockAllocator& alloc, uint32_t index, Block& parent, const Key& key, uint32_t& root)
{
    size_t b_id = parent.i.pointers[index];
    if(!b_id)
        return { .updated_parent = false, .succeeded = false };

    Block b;
    alloc.get().read(b_id, to_bytes(b));

    if(b.type == block_type::external)
    {
        if(b.e.size == 0)
            return { .updated_parent = false, .succeeded = false };

        size_t i = find_eq_e(key, b);
        if(i == b.e.size)
            return { .updated_parent = false, .succeeded = false };

        remove_ord_e(i, b);

        if(b.e.size >= n_ekeys(b) / 2)
        {
            alloc.get().write(b_id, to_bytes(b));
            return { .updated_parent = false, .succeeded = true };
        }

        // try borrow from left
        if(index > 0)
        {
            uint32_t s_left_id = parent.i.pointers[index - 1];
            Block s_left;
            alloc.get().read(s_left_id, to_bytes(s_left));

            if(s_left.e.size > n_ekeys(s_left) / 2)
            {
                share_left_e(s_left, b, parent.i.keys[index - 1]);
                alloc.get().write(b_id, to_bytes(b));
                alloc.get().write(s_left_id, to_bytes(s_left));

                return { .updated_parent = true, .succeeded = true };
            }
        }

        // try borrow from right
        if(index < parent.i.size)
        {
            uint32_t s_right_id = parent.i.pointers[index + 1];
            Block s_right;
            alloc.get().read(s_right_id, to_bytes(s_right));

            if(s_right.e.size > n_ekeys(s_right) / 2)
            {
                share_right_e(b, s_right, parent.i.keys[index]);
                alloc.get().write(b_id, to_bytes(b));
                alloc.get().write(s_right_id, to_bytes(s_right));

                return { .updated_parent = true, .succeeded = true };
            }
        }

        // try merge with left
        if(index > 0)
        {
            uint32_t s_left_id = parent.i.pointers[index - 1];
            Block s_left;
            alloc.get().read(s_left_id, to_bytes(s_left));


            merge_e(s_left, b);
            remove_ord_i(index - 1, parent);

            if(parent.i.size == 0)
                root = s_left_id;

            alloc.get().write(s_left_id, to_bytes(s_left));
            alloc.delete_block(b_id, to_bytes(b));

            return { .updated_parent = true, .succeeded = true };
        }

        // try merge with right
        if(index < parent.i.size)
        {
            uint32_t s_right_id = parent.i.pointers[index + 1];
            Block s_right;
            alloc.get().read(s_right_id, to_bytes(s_right));

            merge_e(b, s_right);
            remove_ord_i(index, parent);

            if(parent.i.size == 0)
                root = b_id;

            alloc.get().write(b_id, to_bytes(b));
            alloc.delete_block(s_right_id, to_bytes(s_right));

            return { .updated_parent = true, .succeeded = true };
        }

        return { .updated_parent = false, .succeeded = false };
    }

    update_result res = { .updated_parent = false, .succeeded = false };

    if((res = remove_rec(alloc, find_geq_i(key, b), b, key, root))
            .updated_parent)
    {

        if(b.i.size >= n_ikeys(b) / 2)
        {
            alloc.get().write(b_id, to_bytes(b));
        }
        else
        {
            // try borrow from left
            if(index > 0)
            {
                uint32_t s_left_id = parent.i.pointers[index - 1];
                Block s_left;
                alloc.get().read(s_left_id, to_bytes(s_left));

                if(s_left.i.size > n_ikeys(s_left) / 2)
                {
                    share_left_i(s_left, b, parent.i.keys[index - 1]);
                    alloc.get().write(b_id, to_bytes(b));
                    alloc.get().write(s_left_id, to_bytes(s_left));

                    res.updated_parent = true;
                    return res;
                }
            }

            // try borrow from right
            if(index < parent.i.size)
            {
                uint32_t s_right_id = parent.i.pointers[index + 1];
                Block s_right;
                alloc.get().read(s_right_id, to_bytes(s_right));

                if(s_right.i.size > n_ikeys(s_right) / 2)
                {
                    share_right_i(b, s_right, parent.i.keys[index]);
                    alloc.get().write(b_id, to_bytes(b));
                    alloc.get().write(s_right_id, to_bytes(s_right));

                    res.updated_parent = true;
                    return res;
                }
            }

            // try merge with left
            if(index > 0)
            {
                uint32_t s_left_id = parent.i.pointers[index - 1];
                Block s_left;
                alloc.get().read(s_left_id, to_bytes(s_left));

                s_left.i.keys[s_left.i.size++] = parent.i.keys[index - 1];

                merge_i(s_left, b);
                remove_ord_i(index - 1, parent);

                if(parent.i.size == 0)
                    root = s_left_id;

                alloc.get().write(s_left_id, to_bytes(s_left));
                alloc.delete_block(b_id, to_bytes(b));

                res.updated_parent = true;
                return res;
            }

            // try merge with right
            if(index < parent.i.size)
            {
                uint32_t s_right_id = parent.i.pointers[index + 1];
                Block s_right;
                alloc.get().read(s_right_id, to_bytes(s_right));

                b.i.keys[b.i.size++] = parent.i.keys[index];

                merge_i(b, s_right);
                remove_ord_i(index, parent);

                if(parent.i.size == 0)
                    root = b_id;

                alloc.get().write(b_id, to_bytes(b));
                alloc.delete_block(s_right_id, to_bytes(s_right));

                res.updated_parent = true;
                return res;
            }
        }
    }

    return res;
}

template <typename Key, typename Block>
size_t find_geq_i(const Key& key, const Block& b)
{
    auto it = std::lower_bound(b.i.keys, b.i.keys + b.i.size, key);
    size_t i = it - b.i.keys;

    if(it != b.i.keys + b.i.size && *it == key)
        ++i;

    return i;
}

template <typename Key, typename Block>
size_t find_eq_e(const Key& key, const Block& b)
{
    auto it = std::lower_bound(b.e.keys, b.e.keys + b.e.size, key);

    if(*it != key)
        return b.e.size;

    return it - b.e.keys;
}

template <typename Key, typename Block>
void insert_ord_i(const Key& key, uint32_t value, Block& b)
{
    size_t i = b.i.size;
    while(i > 0 && key < b.i.keys[i - 1])
    {
        b.i.keys[i] = b.i.keys[i - 1];
        b.i.pointers[i + 1] = b.i.pointers[i];
        --i;
    }

    b.i.keys[i] = key;
    b.i.pointers[i + 1] = value;

    ++b.i.size;
}

template <typename Key, typename Block>
void insert_ord_e(const Key& key, Block& b)
{
    size_t i = b.e.size;
    while(i > 0 && key < b.e.keys[i - 1])
    {
        b.e.keys[i] = b.e.keys[i - 1];
        --i;
    }

    b.e.keys[i] = key;

    ++b.e.size;
}

template <typename Block>
uint32_t remove_ord_i(uint32_t i, Block& b)
{
    uint32_t value = b.i.pointers[i];
    while(i < b.i.size - 1)
    {
        b.i.keys[i] = b.i.keys[i + 1];
        b.i.pointers[i + 1] = b.i.pointers[i + 2];
        ++i;
    }

    --b.i.size;

    return value;
}

template <typename Block>
void remove_ord_e(uint32_t i, Block& b)
{
    while(i < b.e.size - 1)
    {
        b.e.keys[i] = b.e.keys[i + 1];
        ++i;
    }

    --b.e.size;
}

template <typename Block>
void split_half_i(Block& b1, Block& b2)
{
    size_t mid = b1.i.size / 2;

    b2.i.pointers[0] = b1.i.pointers[mid + 1];

    for(size_t i = mid + 1; i < b1.i.size; ++i)
    {
        b2.i.keys[b2.i.size] = b1.i.keys[i];
        b2.i.pointers[b2.i.size + 1] = b1.i.pointers[i + 1];
        ++b2.i.size;
    }

    b1.i.size = mid;
}

template <typename Block>
void split_half_e(Block& b1, Block& b2)
{
    size_t mid = b1.e.size / 2;

    for(size_t i = mid; i < b1.e.size; ++i)
    {
        b2.e.keys[b2.e.size] = b1.e.keys[i];
        ++b2.e.size;
    }

    b1.e.size = mid;
}


template <typename Block, typename Key>
void share_left_i(Block& b1, Block& b2, Key& parent_key)
{
    size_t half = std::ceil((b1.i.size + b2.i.size) / 2.f);
    size_t to_pass =  b1.i.size - half;

    b1.i.keys[b1.i.size++] = parent_key;

    for(size_t j = b2.i.size + to_pass - 1; j > to_pass - 1; --j)
    {
        b2.i.keys[j] = b2.i.keys[j - to_pass];
        b2.i.pointers[j + 1] = b2.i.pointers[j - to_pass + 1];
    }

    b2.i.pointers[to_pass] = b2.i.pointers[0];
    b2.i.size += to_pass;

    for(size_t j = 0, k = half + 1; k < b1.i.size; ++j, ++k)
    {
        b2.i.keys[j] = b1.i.keys[k];
        b2.i.pointers[j] = b1.i.pointers[k];
    }
    b1.i.size -= to_pass + 1;

    parent_key = b1.i.keys[b1.i.size];
}

template <typename Block, typename Key>
void share_left_e(Block& b1, Block& b2, Key& parent_key)
{
    size_t half = std::ceil((b1.e.size + b2.e.size) / 2.f);
    size_t to_pass =  b1.e.size - half;

    for(size_t j = b2.e.size + to_pass - 1; j > to_pass - 1; --j)
        b2.e.keys[j] = b2.e.keys[j - to_pass];
    b2.e.size += to_pass;

    for(size_t j = 0, k = half; k < b1.e.size; ++j, ++k)
        b2.e.keys[j] = b1.e.keys[k];
    b1.e.size -= to_pass;

    parent_key = b2.e.keys[0];
}

template <typename Block, typename Key>
void share_right_i(Block& b1, Block& b2, Key& parent_key)
{
    size_t half = std::ceil(( b2.i.size + b1.i.size) / 2.f);
    size_t to_pass =  b2.i.size - half;

    b1.i.keys[b1.i.size++] = parent_key;

    for(size_t j = b1.i.size, k = 0; k < to_pass; ++j, ++k)
    {
        b1.i.keys[j] = b2.i.keys[k];
        b1.i.pointers[j] = b2.i.pointers[k];
    }
    b1.i.size += to_pass - 1;

    for(size_t k = 0; k < b2.i.size - to_pass; ++k)
    {
        b2.i.keys[k] = b2.i.keys[k + to_pass];
        b2.i.pointers[k] = b2.i.pointers[k + to_pass];
    }
    b2.i.pointers[b2.i.size - to_pass] = b2.i.pointers[b2.i.size];
    b2.i.size -= to_pass;

    parent_key = b1.i.keys[b1.i.size];
}

template <typename Block, typename Key>
void share_right_e(Block& b1, Block& b2, Key& parent_key)
{
    size_t half = std::ceil(( b2.e.size + b1.e.size) / 2.f);
    size_t to_pass =  b2.e.size - half;

    for(size_t j = b1.e.size, k = 0; k < to_pass; ++j, ++k)
        b1.e.keys[j] = b2.e.keys[k];
    b1.e.size += to_pass;

    for(size_t k = 0; k < b2.e.size - to_pass; ++k)
        b2.e.keys[k] = b2.e.keys[k + to_pass];
    b2.e.size -= to_pass;

    parent_key = b2.e.keys[0];
}


template <typename Block>
void merge_i(Block& b1, Block& b2)
{
    for(size_t j = 0; j < b2.i.size; ++j)
    {
        b1.i.keys[b1.i.size] = b2.i.keys[j];
        b1.i.pointers[b1.i.size] = b2.i.pointers[j];
        ++b1.i.size;
    }
    b1.i.pointers[b1.i.size] = b2.i.pointers[b2.i.size];
}

template <typename Block>
void merge_e(Block& b1, Block& b2)
{
    for(size_t j = 0; j < b2.e.size; ++j)
        b1.e.keys[b1.e.size++] = b2.e.keys[j];

    b1.e.pointer = b2.e.pointer;
}

template <typename Key, size_t N>
static constexpr size_t msize(Key (&)[N])
{
    return N;
}

template <typename Block>
static constexpr size_t n_ikeys(const Block& b)
{
    return msize(b.i.keys) - 1;
}

template <typename Block>
static constexpr size_t n_ekeys(const Block& b)
{
    return msize(b.e.keys) - 1;
}

INSTATIATE_LV2_STRUCT_TEMPLATES(disk_block_btreeset)

} /* stg */

