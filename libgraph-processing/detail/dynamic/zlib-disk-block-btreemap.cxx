#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreemap.hxx>
#include <libgraph-processing/detail/util.hxx>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <queue>
#include <array>
#include <zlib.h>

namespace stg::detail
{

static const size_t RAW_LENGTH = 1 << 16;
static const size_t EXCESS = 128;
static std::array<std::byte, RAW_LENGTH> buffer;
static const std::byte separator {0b11111111};

template <typename UBlock>
static size_t sizeof_ublock(const UBlock& ub);

template <typename Vector>
static size_t sizeof_vector(const Vector& v);

template <typename UBlock, typename Block>
static void compress(UBlock& ub, Block& b);

template <typename Block, typename UBlock>
static void uncompress(Block& b, UBlock& ub);

template <typename Key, typename UBlock>
static void insert_ord_i(const Key& key, uint32_t value, UBlock& b);

template <typename Key, typename Value, typename UBlock>
static void insert_ord_e(const Key& key, const Value& value, UBlock& b);

template <typename UBlock>
static uint32_t remove_ord_i(uint32_t i, UBlock& b);

template <typename UBlock>
static auto remove_ord_e(uint32_t i, UBlock& b);

template <typename UBlock>
static void split_half_i(UBlock& b1, UBlock& b2);

template <typename UBlock>
static void split_half_e(UBlock& b1, UBlock& b2);

template <typename UBlock, typename Key>
static void share_left_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock, typename Key>
static void share_left_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock, typename Key>
static void share_right_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock, typename Key>
static void share_right_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key);

template <typename UBlock>
static void merge_i(UBlock& b1, UBlock& b2);

template <typename UBlock>
static void merge_e(UBlock& b1, UBlock& b2);

template <typename Key, typename Value, typename BlockAllocator>
uint32_t zlib_disk_block_btreemap<Key, Value, BlockAllocator>::create(BlockAllocator& alloc)
{
    ublock ub = { .type = block_type::external };
    ub.pointers.push_back(0);

    block b;
    compress(ub, b);

    uint32_t id = alloc.new_block();
    alloc.get().write(id, to_bytes(b));

    return id;
}

template <typename Key, typename Value, typename BlockAllocator>
void zlib_disk_block_btreemap<Key, Value, BlockAllocator>::destroy(BlockAllocator& alloc, uint32_t root)
{
    if(!root)
        return;

    block b;
    alloc.get().read(root, to_bytes(b));

    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::internal)
        for(auto& pointer : ub.pointers)
            destroy(alloc, pointer);

    alloc.delete_block(root, to_bytes(b));
}


template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_btreemap<Key, Value, BlockAllocator>::insert(BlockAllocator& alloc, uint32_t& root, const Key& key, const Value& value)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));

    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
       if(find_eq(key, ub) < ub.keys.size())
           return false;

        if(b.size < PAGE_SIZE - 2*(sizeof(Key) + sizeof(Value)))
        {
            insert_ord_e(key, value, ub);
            compress(ub, b);
            alloc.get().write(root, to_bytes(b));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b;
            ublock new_ub = { .type = block_type::external };

            split_half_e(ub, new_ub);

            insert_ord_e(key, value, key < new_ub.keys[0] ? ub : new_ub);
            new_ub.pointers.push_back(ub.pointers.back());
            ub.pointers.back() = new_b_id;

            compress(ub, b);
            compress(new_ub, new_b);

            alloc.get().write(root, to_bytes(b));
            alloc.get().write(new_b_id, to_bytes(new_b));

            uint32_t new_root_id = alloc.new_block();
            block new_root;
            ublock new_uroot = { .type = block_type::internal };

            new_uroot.keys.push_back(new_ub.keys[0]);
            new_uroot.pointers.push_back(root);
            new_uroot.pointers.push_back(new_b_id);
            root = new_root_id;

            compress(new_uroot, new_root);

            alloc.get().write(new_root_id, to_bytes(new_root));
        }

        return true;
    }

    update_result res = { .succeeded = false };

    if((res = insert_rec(alloc, find_geq(key, ub), ub, key, value))
            .updated_parent)
    {

        if(b.size < PAGE_SIZE - 2*(sizeof(Key) + sizeof(Value)))
        {
            compress(ub, b);
            alloc.get().write(root, to_bytes(b));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b;
            ublock new_ub = { .type = block_type::internal };

            split_half_i(ub, new_ub);

            // need this
            Key parent_key = ub.keys.back();
            ub.keys.pop_back();

            compress(ub, b);
            compress(new_ub, new_b);

            alloc.get().write(root, to_bytes(b));
            alloc.get().write(new_b_id, to_bytes(new_b));

            uint32_t new_root_id = alloc.new_block();
            block new_root;
            ublock new_uroot = { .type = block_type::internal };

            new_uroot.keys.push_back(parent_key);
            new_uroot.pointers.push_back(root);
            new_uroot.pointers.push_back(new_b_id);
            root = new_root_id;

            compress(new_uroot, new_root);

            alloc.get().write(new_root_id, to_bytes(new_root));
        }
    }

    return res.succeeded;
}


template <typename Key, typename Value, typename BlockAllocator>
std::optional<Value> zlib_disk_block_btreemap<Key, Value, BlockAllocator>::remove(BlockAllocator& alloc, uint32_t& root, const Key& key)
{
    if(!root)
        return {};

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        if(ub.keys.empty())
            return {};

        size_t i = find_eq(key, ub);
        if(i == ub.keys.size())
            return {};

        Value value = remove_ord_e(i, ub);
        compress(ub, b);

        alloc.get().write(root, to_bytes(b));

        return value;
    }

    Value value = {};
    uint32_t new_root = root;
    update_result res = { .succeeded = false };

    if((res = remove_rec(alloc, find_geq(key,ub), ub, key, value, new_root))
            .updated_parent)
    {

        if(!ub.keys.empty())
        {
            compress(ub, b);
            alloc.get().write(root, to_bytes(b));
        }
        else
        {
            alloc.delete_block(root);
        }

        if(new_root != root)
            root = new_root;
    }

    if(!res.succeeded)
        return {};

    return value;
}

template <typename Key, typename Value, typename BlockAllocator>
std::vector<std::pair<Key, Value>> zlib_disk_block_btreemap<Key, Value, BlockAllocator>::remove(BlockAllocator& alloc,uint32_t& root, const std::vector<Key>& keys)
{
    std::vector<std::pair<Key, Value>> res;

    for(auto& key : keys)
        if(auto value = remove(alloc, root, key))
            res.push_back({ key, *value });

    return res;
}

template <typename Key, typename Value, typename BlockAllocator>
std::optional<Value> zlib_disk_block_btreemap<Key, Value, BlockAllocator>::find(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    if(!root)
        return {};

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        size_t i = find_eq(key, ub);
        if(i == ub.keys.size())
            return {};

        return ub.values[i];
    }

    return find(alloc, ub.pointers[find_geq(key, ub)], key);
}

template <typename Key, typename Value, typename BlockAllocator>
auto zlib_disk_block_btreemap<Key, Value, BlockAllocator>::find_location(BlockAllocator& alloc, uint32_t root, const Key& key)
    -> std::optional<location>
{
    if(!root)
        return {};

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        location loc {
            .block = root,
            .pos = (uint32_t) find_eq(key, ub)
        };

        if(loc.pos == ub.keys.size())
            return {};

        return loc;
    }

    return find_location(alloc, ub.pointers[find_geq(key, ub)], key);
}

template <typename Key, typename Value, typename BlockAllocator>
cppcoro::generator<std::pair<Key, Value>> zlib_disk_block_btreemap<Key, Value, BlockAllocator>::all(BlockAllocator& alloc, uint32_t first_leaf)
{
    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        ublock ub;
        uncompress(b, ub);

        for(size_t i = 0; i < ub.keys.size(); ++i)
            co_yield { ub.keys[i], ub.values[i] };

        next_block = ub.pointers.back();
    }
}

template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_btreemap<Key, Value, BlockAllocator>::exists(BlockAllocator& alloc, uint32_t root, const Key& key)
{
    if(!root)
        return false;

    block b;
    alloc.get().read(root, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
        return find_eq(key, ub) < ub.keys.size();

    return exists(alloc, ub.pointers[find_geq(key, ub)], key);
}

template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_btreemap<Key, Value, BlockAllocator>::size(BlockAllocator& alloc, uint32_t first_leaf)
{
    size_t size = 0;
    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        ublock ub;
        uncompress(b, ub);
        next_block = ub.pointers.back();

        size += ub.keys.size();
    }

    return size;
}

template <typename Key, typename Value, typename BlockAllocator>
bool zlib_disk_block_btreemap<Key, Value, BlockAllocator>::empty(BlockAllocator& alloc, uint32_t first_leaf)
{
    return size(alloc, first_leaf) == 0;
}

template <typename Key, typename Value, typename BlockAllocator>
void zlib_disk_block_btreemap<Key, Value, BlockAllocator>::debug(BlockAllocator& alloc, uint32_t root, uint32_t first_leaf)
{
    std::cout << "~~INTERNAL~~\n";

    std::queue<uint32_t> ids;
    ids.push(root);

    while(!ids.empty())
    {
        uint32_t id = ids.front(); ids.pop();
        block b;
        alloc.get().read(id, to_bytes(b));
        ublock ub;
        uncompress(b, ub);

        if(ub.type == block_type::internal)
        {
            std::cout << "internal block at " << id << '\n';

            for(size_t i = 0; i < ub.keys.size(); ++i)
                std::cout
                    << "(" << ub.pointers[i] << "*) | "
                    << ub.keys[i] << " | ";

            std::cout << "(" << ub.pointers.back() << "*)\n";

            if(ub.pointers.back())
                ids.push(ub.pointers.back());
        }
    }

    std::cout << "~~EXTERNAL~~\n";

    uint32_t next_block = first_leaf;

    while(next_block)
    {
        block b;
        alloc.get().read(next_block, to_bytes(b));
        ublock ub;
        uncompress(b, ub);

        std::cout << "external block at " << next_block << '\n';

        for(size_t i = 0; i < ub.keys.size(); ++i)
            std::cout
                << "[" << ub.keys[i] << ", "
                << ub.values[i] << " ] | ";

        std::cout << "(" << ub.pointers.back() << "*)\n";

        next_block = ub.pointers.back();
    }
}

template <typename Key, typename Value, typename BlockAllocator>
auto zlib_disk_block_btreemap<Key, Value, BlockAllocator>::insert_rec(BlockAllocator& alloc, uint32_t index, ublock& parent, const Key& key, const Value& value)
    -> update_result
{
    if(!parent.pointers[index])
        return { .updated_parent = false, .succeeded = false };

    block b;
    alloc.get().read(parent.pointers[index], to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        if(find_eq(key, ub) < ub.keys.size())
            return { .updated_parent = false, .succeeded = false };

        if(b.size < PAGE_SIZE - 2*(sizeof(Key) + sizeof(Value)))
        {
            insert_ord_e(key, value, ub);
            compress(ub, b);
            alloc.get().write(parent.pointers[index], to_bytes(b));

            return { .updated_parent = false, .succeeded = true };
        }

        uint32_t new_b_id = alloc.new_block();
        block new_b;
        ublock new_ub = { .type = block_type::external };

        split_half_e(ub, new_ub);

        insert_ord_e(key, value, key < new_ub.keys[0] ? ub : new_ub);
        new_ub.pointers.push_back(ub.pointers.back());
        ub.pointers.back() = new_b_id;

        compress(ub, b);
        compress(new_ub, new_b);

        alloc.get().write(parent.pointers[index], to_bytes(b));
        alloc.get().write(new_b_id, to_bytes(new_b));

        insert_ord_i(new_ub.keys[0], new_b_id, parent);

        return { .updated_parent = true, .succeeded = true };
    }

    update_result res = { .updated_parent = false, .succeeded = false };

    if((res = insert_rec(alloc,find_geq(key, ub), ub, key, value))
            .updated_parent)
    {

        if(b.size < PAGE_SIZE - 2*(sizeof(Key) + sizeof(Value)))
        {
            compress(ub, b);
            alloc.get().write(parent.pointers[index], to_bytes(b));
        }
        else
        {
            uint32_t new_b_id = alloc.new_block();
            block new_b;
            ublock new_ub = { .type = block_type::internal };

            split_half_i(ub, new_ub);

            // need this
            Key parent_key = ub.keys.back();
            ub.keys.pop_back();

            compress(ub, b);
            compress(new_ub, new_b);

            alloc.get().write(parent.pointers[index], to_bytes(b));
            alloc.get().write(new_b_id, to_bytes(new_b));

            insert_ord_i(parent_key, new_b_id, parent);

            res.updated_parent = true;
        }
    }

    return res;
}

template <typename Key, typename Value, typename BlockAllocator>
auto zlib_disk_block_btreemap<Key, Value, BlockAllocator>::remove_rec(BlockAllocator& alloc, uint32_t index, ublock& parent, const Key& key, Value& value, uint32_t& root)
    -> update_result
{
    size_t b_id = parent.pointers[index];
    if(!b_id)
        return { .updated_parent = false, .succeeded = false };

    block b;
    alloc.get().read(b_id, to_bytes(b));
    ublock ub;
    uncompress(b, ub);

    if(ub.type == block_type::external)
    {
        if(ub.keys.empty())
            return { .updated_parent = false, .succeeded = false };

        size_t i = find_eq(key, ub);
        if(i == ub.keys.size())
            return { .updated_parent = false, .succeeded = false };

        value = remove_ord_e(i, ub);
        compress(ub, b);

        if(b.size >= (PAGE_SIZE - EXCESS) / 2)
        {
            alloc.get().write(b_id, to_bytes(b));
            return { .updated_parent = false, .succeeded = true };
        }

        // try borrow from left
        if(index > 0)
        {
            uint32_t s_left_id = parent.pointers[index - 1];
            block s_left;
            alloc.get().read(s_left_id, to_bytes(s_left));

            if(s_left.size > (PAGE_SIZE + EXCESS) / 2)
            {
                ublock s_uleft;
                uncompress(s_left, s_uleft);

                share_left_e(s_uleft, s_left.size, ub, b.size,
                        parent.keys[index - 1]);

                compress(ub, b);
                compress(s_uleft, s_left);

                alloc.get().write(b_id, to_bytes(b));
                alloc.get().write(s_left_id, to_bytes(s_left));

                return { .updated_parent = true, .succeeded = true };
            }
        }

        // try borrow from right
        if(index < parent.pointers.size() - 1)
        {
            uint32_t s_right_id = parent.pointers[index + 1];
            block s_right;
            alloc.get().read(s_right_id, to_bytes(s_right));

            if(s_right.size > (PAGE_SIZE + EXCESS) / 2)
            {
                ublock s_uright;
                uncompress(s_right, s_uright);

                share_right_e(ub, b.size, s_uright, s_right.size,
                        parent.keys[index]);

                compress(ub, b);
                compress(s_uright, s_right);

                alloc.get().write(b_id, to_bytes(b));
                alloc.get().write(s_right_id, to_bytes(s_right));

                return { .updated_parent = true, .succeeded = true };
            }
        }

        // try merge with left
        if(index > 0)
        {
            uint32_t s_left_id = parent.pointers[index - 1];
            block s_left;
            alloc.get().read(s_left_id, to_bytes(s_left));

            ublock s_uleft;
            uncompress(s_left, s_uleft);

            merge_e(s_uleft, ub);
            remove_ord_i(index - 1, parent);

            if(parent.keys.empty())
                root = s_left_id;

            compress(s_uleft, s_left);

            alloc.get().write(s_left_id, to_bytes(s_left));
            alloc.delete_block(b_id, to_bytes(b));

            return { .updated_parent = true, .succeeded = true };
        }

        // try merge with right
        if(index < parent.pointers.size() - 1)
        {
            uint32_t s_right_id = parent.pointers[index + 1];
            block s_right;
            alloc.get().read(s_right_id, to_bytes(s_right));

            ublock s_uright;
            uncompress(s_right, s_uright);

            merge_e(ub, s_uright);
            remove_ord_i(index, parent);

            if(parent.keys.empty())
                root = b_id;

            compress(ub, b);

            alloc.get().write(b_id, to_bytes(b));
            alloc.delete_block(s_right_id, to_bytes(s_right));

            return { .updated_parent = true, .succeeded = true };
        }

        return { .updated_parent = false, .succeeded = false };
    }

    update_result res = { .updated_parent = false, .succeeded = false };

    if((res = remove_rec(alloc, find_geq(key, ub), ub, key, value, root))
            .updated_parent)
    {
        compress(ub, b);

        if(b.size >= (PAGE_SIZE - EXCESS) / 2)
        {
            alloc.get().write(b_id, to_bytes(b));
        }
        else
        {
            // try borrow from left
            if(index > 0)
            {
                uint32_t s_left_id = parent.pointers[index - 1];
                block s_left;
                alloc.get().read(s_left_id, to_bytes(s_left));

                if(s_left.size > (PAGE_SIZE + EXCESS) / 2)
                {
                    ublock s_uleft;
                    uncompress(s_left, s_uleft);

                    share_left_i(s_uleft, s_left.size, ub, b.size,
                            parent.keys[index - 1]);

                    compress(ub, b);
                    compress(s_uleft, s_left);

                    alloc.get().write(b_id, to_bytes(b));
                    alloc.get().write(s_left_id, to_bytes(s_left));

                    res.updated_parent = true;
                    return res;
                }
            }

            // try borrow from right
            if(index < parent.pointers.size() - 1)
            {
                uint32_t s_right_id = parent.pointers[index + 1];
                block s_right;
                alloc.get().read(s_right_id, to_bytes(s_right));

                if(s_right.size > (PAGE_SIZE + EXCESS) / 2)
                {
                    ublock s_uright;
                    uncompress(s_right, s_uright);

                    share_right_i(ub, b.size, s_uright, s_right.size,
                            parent.keys[index]);

                    compress(ub, b);
                    compress(s_uright, s_right);

                    alloc.get().write(b_id, to_bytes(b));
                    alloc.get().write(s_right_id, to_bytes(s_right));

                    res.updated_parent = true;
                    return res;
                }
            }

            // try merge with left
            if(index > 0)
            {
                uint32_t s_left_id = parent.pointers[index - 1];
                block s_left;
                alloc.get().read(s_left_id, to_bytes(s_left));

                ublock s_uleft;
                uncompress(s_left, s_uleft);

                s_uleft.keys.push_back(parent.keys[index - 1]);

                merge_i(s_uleft, ub);
                remove_ord_i(index - 1, parent);

                if(parent.keys.empty())
                    root = s_left_id;

                compress(s_uleft, s_left);

                alloc.get().write(s_left_id, to_bytes(s_left));
                alloc.delete_block(b_id, to_bytes(b));

                res.updated_parent = true;
                return res;
            }

            // try merge with right
            if(index < parent.pointers.size() - 1)
            {
                uint32_t s_right_id = parent.pointers[index + 1];
                block s_right;
                alloc.get().read(s_right_id, to_bytes(s_right));

                ublock s_uright;
                uncompress(s_right, s_uright);

                ub.keys.push_back(parent.keys[index]);

                merge_i(ub, s_uright);
                remove_ord_i(index, parent);

                if(parent.keys.empty())
                    root = b_id;

                compress(ub, b);

                alloc.get().write(b_id, to_bytes(b));
                alloc.delete_block(s_right_id, to_bytes(s_right));

                res.updated_parent = true;
                return res;
            }
        }
    }

    return res;
}

template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_btreemap<Key, Value, BlockAllocator>::find_geq(const Key& key, const ublock& ub)
{

    auto it = std::lower_bound(ub.keys.begin(), ub.keys.end(), key);
    size_t i =  std::distance(ub.keys.begin(), it);

    if(ub.type == block_type::internal && it != ub.keys.end() && *it == key)
        ++i;

    return i;
}

template <typename Key, typename Value, typename BlockAllocator>
size_t zlib_disk_block_btreemap<Key, Value, BlockAllocator>::find_eq(const Key& key, const ublock& ub)
{
    auto it = std::lower_bound(ub.keys.begin(), ub.keys.end(), key);

    if(it == ub.keys.end() || *it != key)
        return ub.keys.size();

    return std::distance(ub.keys.begin(), it);
}

template <typename Key, typename Value, typename BlockAllocator>
void zlib_disk_block_btreemap<Key, Value, BlockAllocator>::compress(ublock& ub, block& b)
{
    std::copy_n(
            reinterpret_cast<const std::byte*>(&ub.type),
            sizeof(ub.type), buffer.data());

    if(ub.type == block_type::internal)
    {
        if(!ub.keys.empty())
            std::copy_n(
                    reinterpret_cast<const std::byte*>(ub.keys.data()),
                    sizeof_vector(ub.keys),
                    buffer.data() + sizeof(ub.type));

        // end of keys
        buffer[sizeof(ub.type) + sizeof_vector(ub.keys)] = separator;
        buffer[sizeof(ub.type) + sizeof_vector(ub.keys) + 1] = separator;
        buffer[sizeof(ub.type) + sizeof_vector(ub.keys) + 2] = separator;
        buffer[sizeof(ub.type) + sizeof_vector(ub.keys) + 3] = separator;

        if(!ub.pointers.empty())
            std::copy_n(
                    reinterpret_cast<const std::byte*>(ub.pointers.data()),
                    sizeof_vector(ub.pointers),
                    buffer.data() + sizeof(ub.type)+sizeof_vector(ub.keys) + 4);

        uLongf out_len = PAGE_SIZE;
        ::compress(
                (Bytef*) b.data, &out_len,
                (Bytef*) buffer.data(), sizeof(ub.type)
                    + sizeof_vector(ub.keys) + sizeof_vector(ub.pointers) + 4);

        b.size = out_len + sizeof(b.size);
    }
    else
    {
        uint32_t pointer = ub.pointers.back();
        std::copy_n(
                reinterpret_cast<const std::byte*>(&pointer),
                sizeof(pointer), buffer.data() + sizeof(ub.type));

        if(!ub.keys.empty())
            std::copy_n(
                    reinterpret_cast<const std::byte*>(ub.keys.data()),
                    sizeof_vector(ub.keys),
                    buffer.data() + sizeof(ub.type) + sizeof(pointer));

        size_t end_of_keys =
            sizeof(ub.type) +
            sizeof(pointer) +
            sizeof_vector(ub.keys);

        // end of keys
        buffer[end_of_keys] = separator;
        buffer[end_of_keys + 1] = separator;
        buffer[end_of_keys + 2] = separator;
        buffer[end_of_keys + 3] = separator;

        if(!ub.values.empty())
            std::copy_n(
                    reinterpret_cast<const std::byte*>(ub.values.data()),
                    sizeof_vector(ub.values),
                    buffer.data() + end_of_keys + 4);

        uLongf out_len = PAGE_SIZE;
        ::compress(
                (Bytef*) b.data, &out_len,
                (Bytef*) buffer.data(), sizeof(ub.type) + sizeof(pointer)
                    + sizeof_vector(ub.keys) + sizeof_vector(ub.values) + 4);

        b.size = out_len + sizeof(b.size);
    }
}

template <typename Key, typename Value, typename BlockAllocator>
void zlib_disk_block_btreemap<Key, Value, BlockAllocator>::uncompress(block& b, ublock& ub)
{
    uLongf out_len = RAW_LENGTH;
    ::uncompress(
            (Bytef*) buffer.data(), &out_len,
            (Bytef*) b.data, b.size - sizeof(b.size));

    std::copy_n(
            buffer.data(), sizeof(ub.type),
            reinterpret_cast<std::byte*>(&ub.type));

    if(ub.type == block_type::internal)
    {
        size_t mark_pos = sizeof(ub.type);

        while(true)
        {
            while(buffer[mark_pos] != separator)
                ++mark_pos;

            if(buffer[mark_pos + 1] == separator
            && buffer[mark_pos + 2] == separator
            && buffer[mark_pos + 3] == separator)
            {
                break;
            }

            ++mark_pos;
        }

        if(out_len > sizeof(ub.type) && mark_pos > sizeof(ub.type))
        {
            using vector_t = decltype(ub.keys);
            using value_t = typename vector_t::value_type;

            ub.keys = vector_t(
                    (value_t*) (buffer.data() + sizeof(ub.type)),
                    (value_t*) (buffer.data() + mark_pos));
        }

        if(out_len > sizeof(ub.type) + sizeof_vector(ub.keys) + 4)
        {
            using vector_t = decltype(ub.pointers);
            using value_t = typename vector_t::value_type;

            ub.pointers = vector_t(
                    (value_t*) (buffer.data() + mark_pos + 4),
                    (value_t*) (buffer.data() + out_len));
        }
    }
    else
    {
        uint32_t pointer;
        std::copy_n(
                buffer.data() + sizeof(ub.type), sizeof(pointer),
                reinterpret_cast<std::byte*>(&pointer));

        ub.pointers.push_back(pointer);

        size_t begin_keys = sizeof(ub.type) + sizeof(pointer);
        size_t mark_pos = begin_keys;

        while(true)
        {
            while(buffer[mark_pos] != separator)
                ++mark_pos;

            if(buffer[mark_pos + 1] == separator
            && buffer[mark_pos + 2] == separator
            && buffer[mark_pos + 3] == separator)
            {
                break;
            }

            ++mark_pos;
        }

        if(out_len > begin_keys && mark_pos > begin_keys)
        {
            using vector_t = decltype(ub.keys);
            using value_t = typename vector_t::value_type;

            ub.keys = vector_t(
                    (value_t*) (buffer.data() + begin_keys),
                    (value_t*) (buffer.data() + mark_pos));
        }

        if(out_len > begin_keys + sizeof_vector(ub.keys) + 4)
        {
            using vector_t = decltype(ub.values);
            using value_t = typename vector_t::value_type;

            ub.values = vector_t(
                    (value_t*) (buffer.data() + mark_pos + 4),
                    (value_t*) (buffer.data() + out_len));
        }
    }
}

template <typename UBlock>
size_t sizeof_ublock(const UBlock& ub)
{
    return sizeof(ub.type)
        + sizeof_vector(ub.keys)
        + sizeof_vector(ub.values)
        + sizeof_vector(ub.pointers);
}

template <typename Vector>
size_t sizeof_vector(const Vector& v)
{
    return v.size() * sizeof(typename Vector::value_type);
}

template <typename Key, typename UBlock>
void insert_ord_i(const Key& key, uint32_t value, UBlock& b)
{
    b.keys.resize(b.keys.size() + 1);
    b.pointers.resize(b.pointers.size() + 1);

    size_t i = b.keys.size() - 1;
    while(i > 0 && key < b.keys[i - 1])
    {
        b.keys[i] = b.keys[i - 1];
        b.pointers[i + 1] = b.pointers[i];
        --i;
    }

    b.keys[i] = key;
    b.pointers[i + 1] = value;
}

template <typename Key, typename Value, typename UBlock>
void insert_ord_e(const Key& key, const Value& value, UBlock& b)
{
    b.keys.resize(b.keys.size() + 1);
    b.values.resize(b.values.size() + 1);

    size_t i = b.keys.size() - 1;
    while(i > 0 && key < b.keys[i - 1])
    {
        b.keys[i] = b.keys[i - 1];
        b.values[i] = b.values[i - 1];
        --i;
    }

    b.keys[i] = key;
    b.values[i] = value;
}

template <typename UBlock>
uint32_t remove_ord_i(uint32_t i, UBlock& b)
{
    uint32_t pointer = b.pointers[i];

    while(i < b.keys.size() - 1)
    {
        b.keys[i] = b.keys[i + 1];
        b.pointers[i + 1] = b.pointers[i + 2];
        ++i;
    }

    b.keys.pop_back();
    b.pointers.pop_back();

    return pointer;
}

template <typename UBlock>
auto remove_ord_e(uint32_t i, UBlock& b)
{
    auto value = b.values[i];

    while(i < b.keys.size() - 1)
    {
        b.keys[i] = b.keys[i + 1];
        b.values[i] = b.values[i + 1];
        ++i;
    }

    b.keys.pop_back();
    b.values.pop_back();

    return value;
}

template <typename UBlock>
void split_half_i(UBlock& b1, UBlock& b2)
{
    size_t mid = b1.keys.size() / 2;

    b2.keys.reserve(b2.keys.size() + b1.keys.size() - (mid + 1));
    b2.pointers.reserve(b2.pointers.size() + b1.pointers.size() - mid);

    b2.pointers.push_back(b1.pointers[mid + 1]);

    for(size_t i = mid + 1; i < b1.keys.size(); ++i)
    {
        b2.keys.push_back(b1.keys[i]);
        b2.pointers.push_back(b1.pointers[i + 1]);
    }

    b1.keys.resize(mid + 1);
    b1.pointers.resize(mid + 1);
}

template <typename UBlock>
void split_half_e(UBlock& b1, UBlock& b2)
{
    size_t mid = b1.keys.size() / 2;

    b2.keys.reserve(b2.keys.size() + b1.keys.size() - mid);
    b2.values.reserve(b2.values.size() + b1.values.size() - mid);

    for(size_t i = mid; i < b1.keys.size(); ++i)
    {
        b2.keys.push_back(b1.keys[i]);
        b2.values.push_back(b1.values[i]);
    }

    b1.keys.resize(mid);
    b1.values.resize(mid);
}


template <typename UBlock, typename Key>
void share_left_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b1_size - half) / (b1_size / b1.keys.size());

    b1.keys.push_back(parent_key);

    b2.keys.insert(b2.keys.begin(),
            b1.keys.end() - to_pass, b1.keys.end());
    b1.keys.resize(b1.keys.size() - to_pass);

    b2.pointers.insert(b2.pointers.begin(),
            b1.pointers.end() - to_pass, b1.pointers.end());
    b1.pointers.resize(b1.pointers.size() - to_pass);

    parent_key = b1.keys.back();

    b1.keys.pop_back();
}

template <typename UBlock, typename Key>
void share_left_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b1_size - half) / (b1_size / b1.keys.size());

    b2.keys.insert(b2.keys.begin(),
            b1.keys.end() - to_pass, b1.keys.end());
    b1.keys.resize(b1.keys.size() - to_pass);

    b2.values.insert(b2.values.begin(),
            b1.values.end() - to_pass, b1.values.end());
    b1.values.resize(b1.values.size() - to_pass);

    parent_key = b2.keys[0];
}

template <typename UBlock, typename Key>
void share_right_i(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b2_size - half) / (b2_size / b2.keys.size());

    b1.keys.push_back(parent_key);

    b1.keys.insert(b1.keys.end(),
            b2.keys.begin(), b2.keys.begin() + to_pass);
    b2.keys.erase(b2.keys.begin(), b2.keys.begin() + to_pass);

    b1.pointers.insert(b1.pointers.end(),
            b2.pointers.begin(), b2.pointers.begin() + to_pass);
    b2.pointers.erase(b2.pointers.begin(), b2.pointers.begin() + to_pass);

    parent_key = b1.keys.back();

    b1.keys.pop_back();
}

template <typename UBlock, typename Key>
void share_right_e(UBlock& b1, size_t b1_size, UBlock& b2, size_t b2_size, Key& parent_key)
{
    size_t half = std::ceil((b1_size + b2_size) / 2.f);
    size_t to_pass =  (b2_size - half) / (b2_size / b2.keys.size());

    b1.keys.insert(b1.keys.end(),
            b2.keys.begin(), b2.keys.begin() + to_pass);
    b2.keys.erase(b2.keys.begin(), b2.keys.begin() + to_pass);

    b1.values.insert(b1.values.end(),
            b2.values.begin(), b2.values.begin() + to_pass);
    b2.values.erase(b2.values.begin(), b2.values.begin() + to_pass);

    parent_key = b2.keys[0];
}


template <typename UBlock>
void merge_i(UBlock& b1, UBlock& b2)
{
    b1.keys.insert(b1.keys.end(), b2.keys.begin(), b2.keys.end());
    b1.pointers.insert(b1.pointers.end(),
            b2.pointers.begin(), b2.pointers.end());
}

template <typename UBlock>
void merge_e(UBlock& b1, UBlock& b2)
{
    b1.keys.insert(b1.keys.end(), b2.keys.begin(), b2.keys.end());
    b1.values.insert(b1.values.end(),
            b2.values.begin(), b2.values.end());
    b1.pointers[0] = b2.pointers[0];
}

INSTATIATE_LV1_COMPRESSED_STRUCT_TEMPLATES(zlib_disk_block_btreemap)

} /* stg */


