#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>
#include <libgraph-processing/detail/util.hxx>

#include <stdexcept>
#include <cstddef>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>

namespace stg::detail
{

disk_block_allocator::disk_block_allocator(const char* filename, uint32_t& free_list)
    : _blocks(filename)
    , _free_list(free_list)
{
}

uint32_t disk_block_allocator::new_block()
{
    uint32_t index;

    if(!_free_list)
    {
        index = _blocks.size();

        _blocks.resize(_blocks.size() + 1);
    }
    else
    {
        index = _free_list;

        deleted_block b;
        _blocks.read(_free_list, to_bytes(b));
        _free_list = b.next;
    }

    return index;
}

void disk_block_allocator::delete_block(uint32_t i)
{
    deleted_block block;
    _blocks.read(i, to_bytes(block));
    block.next = _free_list;
    _free_list = i;

    _blocks.write(i, to_bytes(block));
}

void disk_block_allocator::delete_block(uint32_t i, const std::byte* block)
{
    auto b = ref_as<deleted_block>(block);
    b.next = _free_list;
    _free_list = i;

    _blocks.write(i, to_bytes(b));
}

auto disk_block_allocator::get()
    -> disk_block_vector&
{
    return _blocks;
}


disk_block_allocator::disk_block_vector::disk_block_vector(const char* filename)
{
    _fd = open(filename, O_CREAT | O_RDWR, 0666);
    _size = lseek(_fd, 0, SEEK_END) / PAGE_SIZE;
}

disk_block_allocator::disk_block_vector::~disk_block_vector()
{
    close(_fd);
}

void disk_block_allocator::disk_block_vector::read(size_t i, std::byte* block) const
{
    if(i >= _size)
        throw std::out_of_range("No block at this index");

    if(i == _current_block)
        std::copy_n(_cache, PAGE_SIZE, block);

    lseek(_fd, i * PAGE_SIZE, SEEK_SET);
    ::read(_fd, block, PAGE_SIZE);

    std::copy_n(block, PAGE_SIZE, _cache);
    _current_block = i;
}

void disk_block_allocator::disk_block_vector::write(size_t i, const std::byte* block)
{
    lseek(_fd, i * PAGE_SIZE, SEEK_SET);
    ::write(_fd, block, PAGE_SIZE);

    std::copy_n(block, PAGE_SIZE, _cache);
    _current_block = i;
}

void disk_block_allocator::disk_block_vector::push_back(const std::byte* block)
{
    lseek(_fd, _size * PAGE_SIZE, SEEK_SET);
    ::write(_fd, block, PAGE_SIZE);

    std::copy_n(block, PAGE_SIZE, _cache);
    _current_block = _size;

    ++_size;
}

void disk_block_allocator::disk_block_vector::pop_back()
{
    resize(_size - 1);
}

void disk_block_allocator::disk_block_vector::resize(size_t n)
{
    ftruncate(_fd, ((off_t) n) * PAGE_SIZE);

    _size = n;
}

void disk_block_allocator::disk_block_vector::clear()
{
    resize(0);
}

size_t disk_block_allocator::disk_block_vector::size() const
{
    return _size;
}

bool disk_block_allocator::disk_block_vector::empty() const
{
    return _size == 0;
}

/* auto disk_block_allocator::disk_block_vector::begin() const */
/*     -> const_iterator */
/* { */
/*     return const_iterator(*this, 0); */
/* } */

/* auto disk_block_allocator::disk_block_vector::end() const */
/*     -> const_iterator */
/* { */
/*     return const_iterator(*this, _size); */
/* } */

} /* stg */

