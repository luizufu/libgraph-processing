#include <libgraph-processing/detail/util.hxx>

namespace stg::detail
{

template <typename T, typename BlockAllocator>
template <typename Predicate>
std::vector<T> zlib_disk_block_list<T, BlockAllocator>::remove_if(BlockAllocator& alloc, uint32_t& root, Predicate pred)
{
    if(!root)
        return {};

    uint32_t prev = root;
    ublock ub_prev = { .next = root };
    block b_prev;
    ublock ub_cur;
    block b_cur;

    std::vector<T> removed;
    while(ub_prev.next)
    {
        alloc.get().read(ub_prev.next, to_bytes(b_cur));
        uncompress(b_cur, ub_cur);

        bool removed_something = false;
        for(size_t i = 0; i < ub_cur.elements.size(); ++i)
        {
            if(pred(ub_cur.elements[i]))
            {
                removed.push_back(ub_cur.elements[i]);
                ub_cur.elements[i] = ub_cur.elements.back();
                ub_cur.elements.pop_back();
                removed_something = true;
            }
        }

        if(removed_something)
        {
            if(!ub_cur.elements.empty())
            {
                compress(ub_cur, b_cur);
                alloc.get().write(ub_prev.next, to_bytes(b_cur));
            }
            else if(ub_prev.next != root)
            {
                uint32_t next = ub_cur.next;
                alloc.delete_block(ub_prev.next, to_bytes(b_cur));

                ub_prev.next = ub_cur.next;
                compress(ub_prev, b_prev);
                alloc.get().write(prev, to_bytes(b_prev));
            }
            else if(ub_cur.next) // is root
            {
                uint32_t next = ub_cur.next;
                alloc.delete_block(ub_prev.next, to_bytes(b_cur));
                root = next;
            }
            else // is root and has no next
            {
                compress(ub_cur, b_cur);
                alloc.get().write(ub_prev.next, to_bytes(b_cur));
            }
        }

        prev = ub_prev.next;
        ub_prev = ub_cur;
    }

    return removed;
}

template <typename T, typename BlockAllocator>
template <typename UpdateFunction>
void zlib_disk_block_list<T, BlockAllocator>::try_update(BlockAllocator& alloc, uint32_t root, const T& element, UpdateFunction update)
{
    size_t id = root;
    while(id)
    {
        block b;
        alloc.get().read(id, to_bytes(b));

        ublock ub;
        uncompress(b, ub);

        for(size_t i = 0; i < ub.elements.size(); ++i)
        {
            if(ub.elements[i] == element)
            {
                update(ub.elements[i]);

                compress(ub, b);
                alloc.get().write(id, to_bytes(b));

                return;
            }
        }

        id = ub.next;
    }
}


template <typename T, typename BlockAllocator>
template <typename UpdateFunction>
void zlib_disk_block_list<T, BlockAllocator>::direct_update(BlockAllocator& alloc, uint32_t block_id, size_t pos, UpdateFunction update)
{
    block b;
    alloc.get().read(block_id, to_bytes(b));

    ublock ub;
    uncompress(b, ub);

    update(ub.elements[pos]);

    compress(ub, b);
    alloc.get().write(block_id, to_bytes(b));
}


} /* stg */
