#include <libgraph-processing/detail/dynamic/key-node.hxx>

namespace stg::detail
{

std::ostream& operator<<(std::ostream& out, const key_node& kn)
{
    out << kn.root << ", " << kn.leaf << ", " << kn.total;
    return out;
}

std::ostream& operator<<(std::ostream& out, const unordered_key_node& kn)
{
    out << kn.block << ", " << kn.total;
    return out;
}

std::ostream& operator<<(std::ostream& out, const zlib_key_node& kn)
{
    out << kn.root << ", " << kn.leaf << ", " << kn.total;
    return out;
}

std::ostream& operator<<(std::ostream& out, const zlib_unordered_key_node& kn)
{
    out << kn.block << ", " << kn.total;
    return out;
}


} /* stg */
