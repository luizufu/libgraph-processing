#include <libgraph-processing/disk-edge-list.hxx>
#include <libgraph-processing/detail/static/traverse.hxx>

#include <filesystem>
#include <cassert>
#include <fcntl.h>
#include <unistd.h>

namespace stg
{


template <typename Edge, typename Neighbor>
disk_edge_list<Edge, Neighbor>::disk_edge_list(const std::string& in)
{
    assert(std::filesystem::exists(in));

    _fd = open(in.c_str(), O_RDONLY);
    uint32_t magic;
    read(_fd, reinterpret_cast<char*>(&magic), sizeof(magic));
    assert(magic == EDGE_LIST_MAGIC);

    read(_fd, reinterpret_cast<char*>(&_header), sizeof(_header));
    _props = graph_type_to_graph_props(_header.type);
    assert(_props.format == "edge-list");
}

template <typename Edge, typename Neighbor>
disk_edge_list<Edge, Neighbor>::~disk_edge_list()
{
    close(_fd);
}

template <typename Edge, typename Neighbor>
bool disk_edge_list<Edge, Neighbor>::has_edge(uint32_t u, uint32_t v) const
{
    for(Neighbor n : out_neighbours(u))
    {
        if(_props.sorted && n.v > v)
            break;

        if(n.v == v)
            return true;
    }

    return false;
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_edge_list<Edge, Neighbor>::out_neighbours(uint32_t u) const
{
    return detail::traverse<Edge>(
        _fd, sizeof(EDGE_LIST_MAGIC) + sizeof(_header), _props,
        [u](Edge e) { return e.u == u; },
        [ ](Edge e) { return edge_to_in_neighbor(e); }
    );
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_edge_list<Edge, Neighbor>::in_neighbours(uint32_t v) const
{
    return detail::traverse<Edge>(
        _fd, sizeof(EDGE_LIST_MAGIC) + sizeof(_header), _props,
        [v](Edge e) { return e.v == v; },
        [ ](Edge e) { return edge_to_in_neighbor(e); }
    );
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Edge> disk_edge_list<Edge, Neighbor>::edges() const
{
    return detail::traverse<Edge>(_fd, sizeof(EDGE_LIST_MAGIC) + sizeof(_header), _props);
}


template <typename Edge, typename Neighbor>
size_t disk_edge_list<Edge, Neighbor>::out_degree(uint32_t u) const
{
    size_t count = 0;
    for(auto v : out_neighbours(u))
        ++count;

    return count;
}

template <typename Edge, typename Neighbor>
size_t disk_edge_list<Edge, Neighbor>::in_degree(uint32_t v) const
{
    size_t count = 0;
    for(auto u : in_neighbours(v))
        ++count;

    return count;
}

template <typename Edge, typename Neighbor>
size_t disk_edge_list<Edge, Neighbor>::n_vertices() const
{
    return _header.n_vertices;
}

template <typename Edge, typename Neighbor>
size_t disk_edge_list<Edge, Neighbor>::n_edges() const
{
    return _header.n_edges;
}

template <typename Edge, typename Neighbor>
const graph_props& disk_edge_list<Edge, Neighbor>::props() const
{
    return _props;
}

INSTATIATE_GRAPH_TEMPLATES(disk_edge_list)

} /* stg */

