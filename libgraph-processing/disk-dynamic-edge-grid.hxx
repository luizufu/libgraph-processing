#pragma once

#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/super-block.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-multimap.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-unordered-multimap.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-multimap.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-unordered-multimap.hxx>
#include <libgraph-processing/detail/macro/graph.hxx>

#include <cppcoro/generator.hpp>
#include <cstdint>


namespace stg
{

template <typename Edge, typename Neighbor = neighbor_from_t<Edge>>
class disk_dynamic_edge_grid
{
    mutable detail::disk_block_allocator _allocator;
    uint32_t _root;

    using umultimap_t = detail::disk_block_unordered_multimap<
        uint32_t, Edge, decltype(_allocator)>;
    using multimap_t = detail::disk_block_multimap<
        uint32_t, Edge, decltype(_allocator)>;
    using zumultimap_t = detail::zlib_disk_block_unordered_multimap<
        uint32_t, Edge, decltype(_allocator)>;
    using zmultimap_t = detail::zlib_disk_block_multimap<
        uint32_t, Edge, decltype(_allocator)>;

    detail::dynamic_super_block<edge_grid_header> _super_block;
    graph_props _props;


public:

    disk_dynamic_edge_grid(
        const std::string& in,
        graph_type type = graph_type::dynamic_direct_edge_grid,
        uint32_t grid_size = 32,
        uint32_t max_vertex_id = 1 << 16);

    ~disk_dynamic_edge_grid();


    void debug() const;

    void insert_edge(Edge e);
    void remove_edge(Edge e);
    void remove_out_neighbours(uint32_t u);
    void remove_in_neighbours(uint32_t v);

    bool has_vertex(uint32_t u) const;
    bool has_edge(Edge e) const;
    bool has_edge(uint32_t u, uint32_t v) const;

    cppcoro::generator<Neighbor> out_neighbours(uint32_t u) const;
    cppcoro::generator<Neighbor> in_neighbours(uint32_t v) const;
    cppcoro::generator<Edge> edges() const;

    size_t out_degree(uint32_t u) const;
    size_t in_degree(uint32_t u) const;

    size_t n_vertices() const;
    size_t n_edges() const;

    const graph_props& props() const;
};


} /* stg */

INSTATIATE_GRAPH_TEMPLATES_EXTERN(disk_dynamic_edge_grid)
