#include <libgraph-processing/disk-dynamic-adj-list.hxx>
#include <libgraph-processing/utility.hxx>
#include <iostream>
#include <cassert>

#define CALL(f, ...) \
    _props.sorted && _props.compressed ?  \
        zmultimap_t::f(__VA_ARGS__) : \
    _props.sorted && !_props.compressed ? \
        multimap_t::f(__VA_ARGS__) : \
    !_props.sorted && _props.compressed ? \
        zumultimap_t::f(__VA_ARGS__) : \
        umultimap_t::f(__VA_ARGS__)

namespace stg
{

template <typename Edge, typename Neighbor>
disk_dynamic_adj_list<Edge, Neighbor>::disk_dynamic_adj_list(const std::string& in, graph_type type, direction_t direction)
    : _allocator(in.c_str(), _super_block.free_list)
{

    if(_allocator.get().empty())
    {
        _super_block.header.type = type;
        _super_block.header.n_vertices = 0;
        _super_block.header.n_edges = 0;
        _super_block.header.direction = direction;

        _props = graph_type_to_graph_props(
                _super_block.header.type);
        assert(_props.format == "adj-list" && _props.dynamic);

        _super_block.key_list1 = CALL(create, _allocator);
        _super_block.key_list2 = CALL(create, _allocator);

        _super_block.free_list = 0;
        _allocator.get().push_back(to_bytes(_super_block));
    }
    else
    {
        _allocator.get().read(0, to_bytes(_super_block));

        assert(_super_block.header.magic == ADJ_LIST_MAGIC);

        _props = graph_type_to_graph_props(
                _super_block.header.type);
        assert(_props.format == "adj-list" && _props.dynamic);
    }

    _out_root = _super_block.key_list1;
    _in_root = _super_block.key_list2;
}

template <typename Edge, typename Neighbor>
disk_dynamic_adj_list<Edge, Neighbor>::~disk_dynamic_adj_list()
{
    _allocator.get().write(0, to_bytes(_super_block));
}

template <typename Edge, typename Neighbor>
void disk_dynamic_adj_list<Edge, Neighbor>::debug() const
{
    std::cout << "SUPER BLOCK" << '\n';
    std::cout << "magic: " << _super_block.header.magic << '\n';
    std::cout << "type: " << graph_type_to_string(_super_block.header.type) << '\n';
    std::cout << "n_vertices: " << _super_block.header.n_vertices << '\n';
    std::cout << "n_edges: " << _super_block.header.n_edges << '\n';
    std::cout << "direction: " << _super_block.header.direction << '\n';
    std::cout << "key_list1: " << _super_block.key_list1 << '\n';
    std::cout << "key_list2: " << _super_block.key_list2 << '\n';
    std::cout << "free_list: " << _super_block.free_list << "\n\n";

    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        std::cout << "~~~~~~~~OUT INDEX~~~~~~~~\n\n";
        CALL(debug, _allocator, _out_root);
    }

    if(_super_block.header.direction == INCOMING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        std::cout << "~~~~~~~~IN INDEX~~~~~~~~\n\n";
        CALL(debug, _allocator, _in_root);
    }
}

template <typename Edge, typename Neighbor>
void disk_dynamic_adj_list<Edge, Neighbor>::insert_edge(Edge e)
{

    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        uint32_t key = e.u;
        Neighbor value = edge_to_out_neighbor(e);

        if(CALL(insert, _allocator, _out_root, key, value))
        {
            uint32_t max = std::max(e.u, e.v);
            max = std::max(max + 1, _super_block.header.n_vertices);
            _super_block.header.n_vertices = max;
            ++_super_block.header.n_edges;
        }
    }

    if(_super_block.header.direction == INCOMING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        uint32_t key = e.v;
        Neighbor value = edge_to_in_neighbor(e);

        if(CALL(insert, _allocator, _in_root, key, value))
        {
            if(_super_block.header.direction == INCOMING)
            {
                uint32_t max = std::max(e.u, e.v);
                max = std::max(max + 1,
                        _super_block.header.n_vertices);

                _super_block.header.n_vertices = max;
                ++_super_block.header.n_edges;
            }
        }
    }
}


template <typename Edge, typename Neighbor>
void disk_dynamic_adj_list<Edge, Neighbor>::remove_edge(Edge e)
{
    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        uint32_t key = e.u;
        Neighbor value = edge_to_out_neighbor(e);

        if(CALL(remove, _allocator, _out_root, key, value))
            --_super_block.header.n_edges;
    }

    if(_super_block.header.direction == INCOMING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        uint32_t key = e.v;
        Neighbor value = edge_to_in_neighbor(e);

        if(CALL(remove, _allocator, _in_root, key, value))
            if(_super_block.header.direction == INCOMING)
                --_super_block.header.n_edges;
    }
}

template <typename Edge, typename Neighbor>
void disk_dynamic_adj_list<Edge, Neighbor>::remove_out_neighbours(uint32_t u)
{
    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        auto removed = CALL(remove, _allocator, _out_root, u);

        if(!removed.empty())
        {
            _super_block.header.n_edges -= removed.size();

            if(_super_block.header.direction == OUTGOING_INCOMING)
                for(auto neigh : removed)
                    if(CALL(remove, _allocator, _in_root, neigh.v, {u}))
                        --_super_block.header.n_edges;
        }
    }
    else if(_super_block.header.direction == INCOMING)
    {
        auto predicate = [u](auto neigh) { return neigh.v == u ;};

        auto removed = CALL(remove_if, _allocator, _in_root, predicate);
        _super_block.header.n_edges -= removed.size();
    }
}

template <typename Edge, typename Neighbor>
void disk_dynamic_adj_list<Edge, Neighbor>::remove_in_neighbours(uint32_t v)
{
    if(_super_block.header.direction == INCOMING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        auto removed = CALL(remove, _allocator, _in_root, v);
        _super_block.header.n_edges -= removed.size();

        if(_super_block.header.direction == OUTGOING_INCOMING)
            for(auto neigh : removed)
                CALL(remove, _allocator, _out_root, neigh.v, {v});
    }
    else if(_super_block.header.direction == OUTGOING)
    {
        auto predicate = [v](auto neigh) { return neigh.v == v ;};

        auto removed = CALL(remove_if, _allocator, _out_root, predicate);
        _super_block.header.n_edges -= removed.size();
    }
}


template <typename Edge, typename Neighbor>
bool disk_dynamic_adj_list<Edge, Neighbor>::has_edge(uint32_t u, uint32_t v) const
{
    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        return CALL(exists, _allocator, _out_root, u, {v});
    }
    else if(_super_block.header.direction == INCOMING)
    {
        return CALL(exists, _allocator, _in_root, v, {u});
    }

    return false;
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_dynamic_adj_list<Edge, Neighbor>::out_neighbours(uint32_t u) const
{
    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        for(auto neigh : CALL(find, _allocator, _out_root, u))
            co_yield neigh;
    }
    else if(_super_block.header.direction == INCOMING)
    {
        for(auto [v, neigh] : CALL(all, _allocator, _in_root))
            if(neigh.v == u)
                co_yield edge_to_out_neighbor(make_edge(neigh, v));
    }
}

// TODO which is faster, row-wise or column-wise? do benchmark
template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_dynamic_adj_list<Edge, Neighbor>::in_neighbours(uint32_t v) const
{
    if(_super_block.header.direction == INCOMING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        for(auto neigh : CALL(find, _allocator, _in_root, v))
            co_yield neigh;
    }
    else if(_super_block.header.direction == OUTGOING)
    {
        for(auto [u, neigh] : CALL(all, _allocator, _out_root))
            if(neigh.v == v)
                co_yield edge_to_in_neighbor(make_edge(u, neigh));
    }
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Edge> disk_dynamic_adj_list<Edge, Neighbor>::edges() const
{

    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        for(auto [u, neigh] : CALL(all, _allocator, _out_root))
            co_yield make_edge(u, neigh);
    }
    else if(_super_block.header.direction == INCOMING)
    {
        for(auto [v, neigh] : CALL(all, _allocator, _in_root))
            co_yield make_edge(neigh, v);
    }
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_adj_list<Edge, Neighbor>::out_degree(uint32_t u) const
{
    if(_super_block.header.direction == OUTGOING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        return CALL(count, _allocator, _out_root, u);
    }
    else if(_super_block.header.direction == INCOMING)
    {
        size_t count = 0;
        for(size_t i = 0; i < _super_block.header.n_vertices; ++i)
            for(auto neigh : CALL(find, _allocator, _in_root, i))
                if(neigh.v == u)
                    ++count;

        return count;
    }

    return 0;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_adj_list<Edge, Neighbor>::in_degree(uint32_t v) const
{
    if(_super_block.header.direction == INCOMING ||
        _super_block.header.direction == OUTGOING_INCOMING)
    {
        return CALL(count, _allocator, _in_root, v);
    }
    else if(_super_block.header.direction == OUTGOING)
    {
        size_t count = 0;
        for(size_t i = 0; i < _super_block.header.n_vertices; ++i)
            for(auto neigh : CALL(find, _allocator, _out_root, i))
                if(neigh.v == v)
                    ++count;

        return count;
    }

    return 0;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_adj_list<Edge, Neighbor>::n_vertices() const
{
    return _super_block.header.n_vertices;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_adj_list<Edge, Neighbor>::n_edges() const
{
    return _super_block.header.n_edges;
}

template <typename Edge, typename Neighbor>
const graph_props& disk_dynamic_adj_list<Edge, Neighbor>::props() const
{
    return _props;
}

INSTATIATE_GRAPH_TEMPLATES(disk_dynamic_adj_list)

} /* stg */

#undef CALL
