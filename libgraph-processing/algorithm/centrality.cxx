#include <libgraph-processing/algorithm/centrality.hxx>

#include <libgraph-processing/algorithm/vertex-centric-engine.hxx>
#include <libgraph-processing/algorithm/edge-centric-engine.hxx>
#include <libgraph-processing/algorithm/shortest_path.hxx>
#include <libgraph-processing/detail/float-comparison.hxx>
#include <cppcoro/sync_wait.hpp>
#include <queue>
#include <stack>
#include <vector>
#include <limits>
#include <numeric>
#include <iostream>

namespace stg
{

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    outdegree(const Graph& graph, execution exec)
{
    if(exec == execution::seq)
    {
        for(uint32_t u = 0; u < graph.n_vertices(); ++u)
            co_yield { u, graph.out_degree(u) };
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(engine.template run_compute<float>(
            // compute
            [](auto&& control) {

                control.set_value(control.out_degree());
                control.vote_to_halt();
            },
            //default_value
            0.f));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_scatter_gather<float, float>(
            // scather
            [](auto&& control) {

            },
            // gather
            [](auto&& control) {

                control.value() = control.out_degree();
                control.vote_to_halt();
            },
            //default_value
            0.f));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_scatter_gather<float, float>(
            // scather
            [](auto&& control) {

            },
            // gather
            [](auto&& control) {

                control.value() = control.out_degree();
                control.vote_to_halt();
            },
            //default_value
            0.f));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    indegree(const Graph& graph, execution exec)
{
    if(exec == execution::seq)
    {
        for(uint32_t u = 0; u < graph.n_vertices(); ++u)
            co_yield { u, graph.in_degree(u) };
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_compute<float, float>(
            // compute
            [](auto&& control) {

                if(control.superstep() == 0)
                {
                    for(auto n : control.out_neighbours())
                        control.send_to(n.v, 1.f);
                }
                else
                {
                    auto& msgs = control.messages();
                    float indegree= std::accumulate(
                            msgs.begin(), msgs.end(), 0.f);
                    control.set_value(indegree);
                }

                control.vote_to_halt();
            },
            //default_value
            0.f));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_scatter_gather<float, float>(
            // scatter
            [](auto&& control) {

                for(auto n : control.out_neighbours())
                    control.send_to(n.v, 1.f);
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                float indegree= std::accumulate(msgs.begin(), msgs.end(), 0.f);
                control.set_value(indegree);

                control.vote_to_halt();
            },
            //default_value
            0.f));

            for(size_t i = 0; i < ranks.size(); ++i)
                co_yield { i, ranks[i] };
        }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_scatter_gather<float, float>(
            // scatter
            [](auto&& control) {

                control.send(control.weight());
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                float indegree= std::accumulate(msgs.begin(), msgs.end(), 0.f);
                control.set_value(indegree);

                control.vote_to_halt();
            },
            //default_value
            0.f));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}


template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    closeness(const Graph& graph, execution exec)
{
    size_t n_vertices = graph.n_vertices();
    for(size_t u = 0; u < n_vertices; ++u)
    {
        float rank = 0;
        for(auto path : sssp(graph, u, exec))
            rank += path[0].second;

        co_yield { u, rank / (n_vertices - 1) };
    }
}

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    betweenness(const Graph& graph, execution exec)
{
    if(exec == execution::seq)
    {
        size_t n_vertices = graph.n_vertices();
        std::vector<float> ranks(n_vertices, 0);

        auto comparator = [](const auto& lhs, const auto& rhs) {
            return lhs.second > rhs.second;
        };

        for(size_t s = 0; s < n_vertices; ++s)
        {
            std::vector<float> dist(n_vertices,
                    std::numeric_limits<float>::max());
            std::vector<bool> visited(dist.size(), false);
            std::vector<float> sigma(n_vertices, 0.f);
            std::vector<std::vector<uint32_t>> pred(n_vertices);
            std::stack<uint32_t> stack;

            std::priority_queue<
                std::pair<uint32_t, float>,
                std::vector<std::pair<uint32_t, float>>,
                decltype(comparator)> queue(comparator);

            dist[s] = 0.f;
            sigma[s] = 1.f;
            visited[s] = true;
            queue.push({ s, 0.f });

            while(!queue.empty())
            {
                uint32_t u = queue.top().first; queue.pop();
                stack.push(u);

                for(auto n : graph.out_neighbours(u))
                {
                    if(!visited[n.v] && dist[u] + n.weight() < dist[n.v])
                    {
                        dist[n.v] = dist[u] + n.weight();
                        sigma[n.v] = 0.f;
                        pred[n.v].clear();
                        queue.push({ n.v, dist[n.v] });
                    }

                    if(detail::approximately_equal(
                            dist[n.v], dist[u] + n.weight()))
                    {
                        sigma[n.v] += n.weight() * sigma[u];
                        pred[n.v].push_back(u);
                    }
                }

                visited[u] = true;
            }

            std::vector<float> delta(n_vertices, 0);
            while(!stack.empty())
            {
                uint32_t v = stack.top(); stack.pop();

                for(uint32_t u : pred[v])
                    delta[u] += (sigma[u] / sigma[v]) * (1 + delta[v]);

                if(v != s)
                    ranks[v] += delta[v];
            }
        }

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        size_t n_vertices = graph.n_vertices();

        std::vector<float> ranks(n_vertices, 0.f);
        float infinity = std::numeric_limits<float>::max();

        struct value
        {
            float dist = std::numeric_limits<float>::max();
            std::vector<uint32_t> pred;
            float sigma = 1.f;
        };

        struct message
        {
            uint32_t sender = 0;
            float dist = std::numeric_limits<float>::max();
            float sigma = 0.f;
        };

        for(size_t s = 0; s < n_vertices; ++s)
        {
            auto info = cppcoro::sync_wait(
                    engine.template run_compute<value, message>(
                // compute
                [s](auto&& control) {

                    auto& msgs = control.messages();
                    bool send_message = false;

                    // if i'm the source than set distance to myself to 0
                    // and send message to my neighbours
                    if(control.superstep() == 0 && control.id() == s)
                    {
                        control.value().dist = 0.f;
                        send_message = true;

                    }
                    // if there is message to me then i need to update
                    else if(!msgs.empty())
                    {
                        // find the message with minimum distance
                        auto it = std::min_element(
                            msgs.begin(), msgs.end(),
                            [](auto& lhs, auto& rhs) {
                                return lhs.dist < rhs.dist;
                            });

                        // if the message has smaller distance than what i know
                        // (there is a new shortest distance), then update my
                        // distance and reset info related to shortest paths
                        if(it->dist < control.value().dist)
                        {
                            control.value().dist = it->dist;
                            control.value().sigma = 0.f;
                            control.value().pred.clear();
                        }

                        // if message has smaller or equal distance (is shortest
                        // distance), than update info related to shortest
                        // paths and send message to my neighbours
                        if(detail::approximately_equal(
                                it->dist, control.value().dist))
                        {
                            control.value().sigma += it->sigma;
                            control.value().pred.push_back(it->sender);
                            send_message = true;
                        }
                    }

                    // send message to my neighbours actually
                    if(send_message)
                    {
                        message msg;
                        msg.sender = control.id();

                        for(auto n : control.out_neighbours())
                        {
                            msg.dist = control.value().dist + n.weight();
                            msg.sigma = n.weight() * control.value().sigma;

                            control.send_to(n.v, msg);
                        }
                    }

                    control.vote_to_halt();
                },
                // default state
                value{}));

            float max_dist = 0.f;
            for(auto& item : info)
                if(item.dist < infinity && item.dist > max_dist)
                    max_dist = item.dist;

            // TODO i think the bug is that a node needs to receive all other
            // nodes contribution before computing delta values for this
            // predecessors, i don't know if this is possible in parallel...
            auto deltas = cppcoro::sync_wait(
                    engine.template run_compute<float, float>(
                // compute
                [&info, max_dist](auto&& control) {

                    // sum my delta values if there are any
                    for(auto& msg : control.messages())
                        control.value() += msg;

                    bool have_max_dist = detail::approximately_equal(
                        info[control.id()].dist, max_dist);

                    // if i'm the farthest target in superstep 0 or there are
                    // messages to me, then calculate delta values for my
                    // predecessors (in shortest paths) send message to them
                    if((control.superstep() == 0 && have_max_dist)||
                       !control.messages().empty())
                    {
                        size_t v = control.id();
                        for(auto u : info[v].pred)
                        {
                            float u_delta =
                                (info[u].sigma / info[v].sigma) *
                                (1 + control.value());

                            control.send_to(u, u_delta);
                        }
                    }

                    control.vote_to_halt();
                },
                // default state
                0.f));

            // accumulate deltas related to paths starting from s
            for(uint32_t i = 0; i < n_vertices; ++i)
                if(i != s)
                    ranks[i] += deltas[i];
        }

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        size_t n_vertices = graph.n_vertices();

        std::vector<float> ranks(n_vertices, 0.f);
        float infinity = std::numeric_limits<float>::max();

        struct value
        {
            float dist = std::numeric_limits<float>::max();
            std::vector<uint32_t> pred;
            float sigma = 1.f;
        };

        struct message
        {
            uint32_t sender = 0;
            float dist = std::numeric_limits<float>::max();
            float sigma = 0.f;
        };

        for(size_t s = 0; s < n_vertices; ++s)
        {
            auto info = cppcoro::sync_wait(
                    engine.template run_scatter_gather<value, message>(
                // scatter
                [s](auto&& control) {

                    // initialization step
                    if(control.superstep() == 0 && control.id() == s)
                        control.value().dist = 0.f;

                    if(control.id() == s || control.superstep() > 0)
                    {
                        message msg;
                        msg.sender = control.id();

                        for(auto n : control.out_neighbours())
                        {
                            msg.dist = control.value().dist + n.weight();
                            msg.sigma = n.weight() * control.value().sigma;

                            control.send_to(n.v, msg);
                        }
                    }
                },
                // gather
                [](auto&& control) {

                    auto& msgs = control.messages();

                    if(msgs.empty())
                    {
                        control.vote_to_halt();
                        return;
                    }

                    auto it = std::min_element(
                        msgs.begin(), msgs.end(),
                        [](auto& lhs, auto& rhs) {
                            return lhs.dist < rhs.dist;
                        });

                    if(it->dist < control.value().dist)
                    {
                        control.value().dist = it->dist;
                        control.value().sigma = 0.f;
                        control.value().pred.clear();
                    }

                    if(detail::approximately_equal(
                            it->dist, control.value().dist))
                    {
                        control.value().sigma += it->sigma;
                        control.value().pred.push_back(it->sender);
                    }
                    else
                    {
                        control.vote_to_halt();
                    }
                },
                // default state
                value{}));

            float max_dist = 0.f;
            for(auto& item : info)
                if(item.dist < infinity && item.dist > max_dist)
                    max_dist = item.dist;

            auto deltas = cppcoro::sync_wait(engine.template run_scatter_gather<float, float>(
                // scatter
                [&info, max_dist](auto&& control) {

                    bool have_max_dist = detail::approximately_equal(
                        info[control.id()].dist, max_dist);

                    if((control.superstep() == 0 && have_max_dist) ||
                        control.superstep() > 0)
                    {
                        size_t v = control.id();
                        for(auto u : info[v].pred)
                        {
                            float u_delta =
                                (info[u].sigma / info[v].sigma) *
                                (1 + control.value());

                            control.send_to(u, u_delta);
                        }

                        info[v].pred.clear();
                    }
                },
                // gather
                [](auto&& control) {

                    auto& msgs = control.messages();

                    for(auto& msg : msgs)
                        control.value() += msg;

                    if(msgs.empty())
                        control.vote_to_halt();
                },
                // default state
                0.f));

            // accumulate deltas related to paths starting from s
            for(uint32_t i = 0; i < n_vertices; ++i)
                if(i != s)
                    ranks[i] += deltas[i];
        }

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    pagerank(const Graph& graph, size_t iterations, float dump, execution exec)
{
    if(exec == execution::seq)
    {
        size_t n_vertices = graph.n_vertices();
        std::vector<float> ranks(n_vertices, 0);

        for(size_t i = 0; i < iterations; ++i)
        {
            std::vector<float> rank_new(n_vertices);

            // compute contributions (sum)
            //this way we need only out neighbours
            //(in neighbours should be slower)
            for(uint32_t u = 0; u < n_vertices; ++u)
                for(auto n : graph.out_neighbours(u))
                    rank_new[n.v] += ranks[u] / graph.out_degree(u);

            // reuse rank_new as sum in the formula for each vertex
            for(uint32_t u = 0; u < n_vertices; ++u)
                rank_new[u] = (1.f - dump) / n_vertices + dump * rank_new[u];

            // update ranks
            ranks = rank_new;
        }

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_compute<float, float>(
            // compute
            [&](auto&& control) {

                auto& msgs = control.messages();

                // compute my pagerank using the formula
                // messages have the contribution of each incoming node
                // skip superstep 0
                if(control.superstep() > 0)
                {
                    float sum = std::accumulate(msgs.begin(), msgs.end(), 0.f);
                    control.value() = (1.f - dump)/control.n_vertices() + dump*sum;
                }

                // send my contribution to my neighbours
                if(control.superstep() < iterations)
                {
                    float my_contribution = control.value() / control.out_degree();
                    for(auto n : control.out_neighbours())
                        control.send_to(n.v, my_contribution);
                }
                // i'm done
                else
                {
                    control.vote_to_halt();
                }
            },
            // default state (uniform distribution)
            1.f / graph.n_vertices()));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto ranks = cppcoro::sync_wait(
                engine.template run_scatter_gather<float, float>(
            // scatter
            [&](auto&& control) {

                float my_contribution = control.value() / control.out_degree();
                for(auto n : control.out_neighbours())
                    control.send_to(n.v, my_contribution);
            },
            // gather
            [&](auto&& control) {

                if(control.superstep() < iterations)
                {
                    auto& msgs = control.messages();
                    float sum = std::accumulate(msgs.begin(), msgs.end(), 0.f);

                    control.value() = (1.f - dump) / control.n_vertices()
                            + dump * sum;
                }
                else
                {
                    control.vote_to_halt();
                }
            },
            // default state (uniform distribution)
            1.f / graph.n_vertices()));

        for(size_t i = 0; i < ranks.size(); ++i)
            co_yield { i, ranks[i] };
    }
    else if(exec == execution::ecsg)
    {
        struct value
        {
            float rank;
            size_t degree;
        };

        edge_centric_engine engine(graph);

        size_t n_vertices = graph.n_vertices();

        auto info = cppcoro::sync_wait(engine.template run_scatter_gather<value, float>(
            // scatter
            [&](auto&& control) {

                if(control.superstep() == 0)
                    ++control.source_value().degree;
                else
                    control.send(control.source_value().rank /
                                 control.source_value().degree);
            },
            // gather
            [&](auto&& control) {

                // lost 1 iteration computing degree
                if(control.superstep() < iterations + 1)
                {
                    auto& msgs = control.messages();
                    float sum = std::accumulate(msgs.begin(), msgs.end(), 0.f);

                    control.value().rank=(1.f-dump)/control.n_vertices() + dump*sum;
                }
                else
                {
                    control.vote_to_halt();
                }
            },
            // default state (uniform distribution)
            { .rank = 1.f / n_vertices, .degree = 0 }));

        for(size_t i = 0; i < n_vertices; ++i)
            co_yield { i, info[i].rank };
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}


using r_item = std::pair<uint32_t, float>;
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<r_item>,
        outdegree, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<r_item>,
        indegree, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<r_item>,
        closeness, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<r_item>,
        betweenness, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<r_item>,
        pagerank, size_t, float, execution)

} /* stg */
