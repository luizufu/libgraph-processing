#pragma once

#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/macro/algofunc.hxx>
#include <cppcoro/generator.hpp>
#include <utility>
#include <cstdint>

namespace stg
{

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    indegree(const Graph& graph, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    outdegree(const Graph& graph, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    closeness(const Graph& graph, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    betweenness(const Graph& graph, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    pagerank(const Graph& graph, size_t iterations = 30, float dump = .85f,
             execution exec = execution::seq);


} /* stg */

using r_item = std::pair<uint32_t, float>;
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<r_item>,
        outdegree, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<r_item>,
        indegree, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<r_item>,
        closeness, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<r_item>,
        betweenness, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<r_item>,
        pagerank, size_t, float, execution)

