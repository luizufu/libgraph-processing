#pragma once

#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/macro/algofunc.hxx>
#include <cppcoro/generator.hpp>
#include <utility>
#include <cstdint>

namespace stg
{

template <typename Graph>
cppcoro::generator<uint32_t>
    bfs(const Graph& graph, uint32_t s, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<uint32_t>
    dfs(const Graph& graph, uint32_t s, execution exec = execution::seq);

}

INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<uint32_t>,
        bfs, uint32_t, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<uint32_t>,
        dfs, uint32_t, execution)

