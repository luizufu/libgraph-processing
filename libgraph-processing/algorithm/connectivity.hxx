#pragma once

#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/macro/algofunc.hxx>
#include <cppcoro/generator.hpp>
#include <utility>
#include <cstdint>

namespace stg
{

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, uint8_t>>
    cc(const Graph& graph, execution exec = execution::seq);

}

using cc_item = std::pair<uint32_t, uint8_t>;
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<cc_item>,
        cc, execution)

