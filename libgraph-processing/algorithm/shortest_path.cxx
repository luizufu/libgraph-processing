#include <libgraph-processing/algorithm/shortest_path.hxx>

#include <libgraph-processing/algorithm/vertex-centric-engine.hxx>
#include <libgraph-processing/algorithm/edge-centric-engine.hxx>
#include <libgraph-processing/detail/fixed-matrix.hxx>
#include <cppcoro/sync_wait.hpp>
#include <queue>
#include <vector>
#include <limits>
#include <numeric>
#include <iostream>

namespace stg
{
    namespace detail
    {
        struct dist_prev
        {
            float dist;
            uint32_t prev;
        };

    } /* detail */

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    spsp(const Graph& graph, uint32_t s, uint32_t t, execution exec)
{
    if(exec == execution::seq)
    {
        std::vector<float> dist(
                graph.n_vertices(),
                std::numeric_limits<float>::max());
        std::vector<bool> visited(dist.size(), false);
        std::vector<uint32_t> prev(dist.size());

        dist[s] = 0.f;
        visited[s] = true;
        std::iota(prev.begin(), prev.end(), 0);

        // for min heap
        auto comparator = [](const auto& lhs, const auto& rhs) {
            return lhs.second > rhs.second;
        };

        std::priority_queue<
            std::pair<uint32_t, float>,
            std::vector<std::pair<uint32_t, float>>,
            decltype(comparator)> q(comparator);

        q.push({ s, 0.f });
        while(!q.empty())
        {
            uint32_t u = q.top().first; q.pop();

            for(auto n : graph.out_neighbours(u))
            {
                if(!visited[n.v] && dist[u] + n.weight() < dist[n.v])
                {
                    dist[n.v] = dist[u] + n.weight();
                    prev[n.v] = u;
                    q.push({ n.v, dist[n.v] });
                }
            }

            visited[u] = true;

            if(u == t)
                break;
        }

        if(visited[t])
        {
            co_yield { t, dist[t] };

            while(prev[t] != t)
            {
                t = prev[t];
                co_yield { t , dist[t] };
            }
        }
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        auto info = cppcoro::sync_wait(
                engine.template run_compute<detail::dist_prev, detail::dist_prev>(
            // compute
            [s, t](auto&& control) {

                auto& msgs = control.messages();

                // if i'm the source than my_dist is 0 and I point to myself
                detail::dist_prev my_info = {
                    .dist = control.id() == s ?
                        0 : std::numeric_limits<float>::max(),
                    .prev = control.id()
                };


                // update my_dist and my_prev looking at the messages that come
                for(auto& msg : msgs)
                    if(msg.dist < my_info.dist)
                        my_info = msg;

                // if my_dist is lower than what I had
                if(my_info.dist < control.value().dist)
                {
                    // update what I had
                    control.set_value(my_info);

                    // only send to neighbours if i'm not t
                    if(control.id() != t)
                    {
                        detail::dist_prev msg = { .prev = control.id() };

                        // send a msg to my neighbours telling they new dist
                        for(auto n : control.out_neighbours())
                        {
                            msg.dist = my_info.dist + n.weight();
                            control.send_to(n.v, msg);
                        }
                    }
                }

                control.vote_to_halt();
            },
            // default state
            { .dist = std::numeric_limits<float>::max(), .prev = 0 }));

        if(info[t].dist < std::numeric_limits<float>::max() &&
                info[t].prev != t)
        {
            co_yield { t, info[t].dist };

            while(info[t].prev != t)
            {
                t = info[t].prev;
                co_yield { t , info[t].dist };
            }
        }
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto info = cppcoro::sync_wait(
                engine.template run_scatter_gather<detail::dist_prev, detail::dist_prev>(
            // scatter
            [s, t](auto&& control) {

                // initialization step
                if(control.superstep() == 0 && control.id() == s)
                    control.value() = { 0.f, control.id() };

                if(control.value().dist < std::numeric_limits<float>::max())
                {
                    if(control.id() != t)
                    {
                        detail::dist_prev msg = { .prev = control.id() };

                        for(auto n : control.out_neighbours())
                        {
                            msg.dist = control.value().dist + n.weight();
                            control.send_to(n.v, msg);
                        }
                    }
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                auto it = std::min_element(
                    msgs.begin(), msgs.end(),
                    [] (auto& lhs, auto& rhs) {
                        return lhs.dist < rhs.dist;
                    });

                if(it != msgs.end() && it->dist < control.value().dist)
                    control.value() = *it;
                else
                    control.vote_to_halt();
            },
            // default state
            { .dist = std::numeric_limits<float>::max(), .prev = 0 }));

        if(info[t].dist < std::numeric_limits<float>::max() &&
                info[t].prev != t)
        {
            co_yield { t, info[t].dist };

            while(info[t].prev != t)
            {
                t = info[t].prev;
                co_yield { t , info[t].dist };
            }
        }
    }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        auto info = cppcoro::sync_wait(
                engine.template run_scatter_gather<detail::dist_prev, detail::dist_prev>(
            // scatter
            [s, t](auto&& control) {

                // initialization step
                if(control.superstep() == 0 && control.source_id() == s)
                    control.source_value() = { 0.f, control.source_id() };

                if(control.source_value().dist <
                        std::numeric_limits<float>::max())
                {
                    if(control.source_id() != t)
                    {
                        detail::dist_prev msg {
                            .dist = control.source_value().dist
                                + control.weight(),
                            .prev = control.source_id()
                        };

                        control.send(msg);
                    }
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                auto it = std::min_element(
                    msgs.begin(), msgs.end(),
                    [] (auto& lhs, auto& rhs) {
                        return lhs.dist < rhs.dist;
                    });

                if(it != msgs.end() && it->dist < control.value().dist)
                    control.value() = *it;
                else
                    control.vote_to_halt();
            },
            // default state
            { .dist = std::numeric_limits<float>::max(), .prev = 0 }));

        if(info[t].dist < std::numeric_limits<float>::max() &&
                info[t].prev != t)
        {
            co_yield { t, info[t].dist };

            while(info[t].prev != t)
            {
                t = info[t].prev;
                co_yield { t, info[t].dist };
            }
        }
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}

template <typename Graph>
cppcoro::generator<std::vector<std::pair<uint32_t, float>>>
    sssp(const Graph& graph, uint32_t s, execution exec)
{
    if(exec == execution::seq)
    {
        std::vector<float> dist(
                graph.n_vertices(),
                std::numeric_limits<float>::max());
        std::vector<bool> visited(dist.size(), false);
        std::vector<uint32_t> prev(dist.size());

        dist[s] = 0.f;
        visited[s] = true;
        std::iota(prev.begin(), prev.end(), 0);

        // for min heap
        auto comparator = [](const auto& lhs, const auto& rhs) {
            return lhs.second > rhs.second;
        };

        std::priority_queue<
            std::pair<uint32_t, float>,
            std::vector<std::pair<uint32_t, float>>,
            decltype(comparator)> q(comparator);

        q.push({ s, 0.f });
        while(!q.empty())
        {
            uint32_t u = q.top().first; q.pop();

            for(auto n : graph.out_neighbours(u))
            {
                if(!visited[n.v] && dist[u] + n.weight() < dist[n.v])
                {
                    dist[n.v] = dist[u] + n.weight();
                    prev[n.v] = u;
                    q.push({ n.v, dist[n.v] });
                }
            }

            visited[u] = true;
        }

        for(size_t i = 0; i < visited.size(); ++i)
        {
            uint32_t t = i;
            if(visited[t])
            {
                std::vector<std::pair<uint32_t, float>> items;
                items.emplace_back(t, dist[t]);

                while(prev[t] != t)
                {
                    t = prev[t];
                    items.emplace_back(t, dist[t]);
                }

                co_yield items;
            }
        }
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        auto info = cppcoro::sync_wait(
                engine.template run_compute<detail::dist_prev, detail::dist_prev>(
            // compute
            [s](auto&& control) {

                auto& msgs = control.messages();

                // if i'm the source than my_dist is 0 and I point to myself
                detail::dist_prev my_info = {
                    .dist = control.id() == s ?
                        0 : std::numeric_limits<float>::max(),
                    .prev = control.id()
                };


                // update my_dist and my_prev looking at the messages that come
                for(auto& msg : msgs)
                    if(msg.dist < my_info.dist)
                        my_info = msg;

                // if my_dist is lower than what I had
                if(my_info.dist < control.value().dist)
                {
                    // update what I had
                    control.set_value(my_info);
                    float w = 1.f; // change this if weighted

                    // prepare msg to my neighbours
                    detail::dist_prev msg = { .prev = control.id() };

                    // send a msg to my neighbours telling they new distance
                    for(auto n : control.out_neighbours())
                    {
                        msg.dist = my_info.dist + n.weight();
                        control.send_to(n.v, msg);
                    }
                }

                control.vote_to_halt();
            },
            // default state
            { .dist = std::numeric_limits<float>::max(), .prev = 0 }));

        for(uint32_t i = 0; i < graph.n_vertices(); ++i)
        {
            uint32_t t = i;
            if(info[t].dist < std::numeric_limits<float>::max() &&
                    info[t].prev != t)
            {
                std::vector<std::pair<uint32_t, float>> items;
                items.emplace_back(t, info[t].dist);

                while(info[t].prev != t)
                {
                    t = info[t].prev;
                    items.emplace_back(t, info[t].dist);
                }

                co_yield items;
            }
        }
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto info = cppcoro::sync_wait(
                engine.template run_scatter_gather<detail::dist_prev, detail::dist_prev>(
            // scatter
            [s](auto&& control) {

                // initialization step
                if(control.superstep() == 0 && control.id() == s)
                    control.value() = { 0.f, control.id() };

                if(control.value().dist < std::numeric_limits<float>::max())
                {
                    detail::dist_prev msg = { .prev = control.id() };

                    for(auto n : control.out_neighbours())
                    {
                        msg.dist = control.value().dist + n.weight();
                        control.send_to(n.v, msg);
                    }
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                auto it = std::min_element(
                    msgs.begin(), msgs.end(),
                    [] (auto& lhs, auto& rhs) {
                        return lhs.dist < rhs.dist;
                    });

                if(it != msgs.end() && it->dist < control.value().dist)
                    control.value() = *it;
                else
                    control.vote_to_halt();
            },
            // default state
            { .dist = std::numeric_limits<float>::max(), .prev = 0 }));

        for(uint32_t i = 0; i < graph.n_vertices(); ++i)
        {
            uint32_t t = i;
            if(info[t].dist < std::numeric_limits<float>::max() &&
                    info[t].prev != t)
            {
                std::vector<std::pair<uint32_t, float>> items;
                items.emplace_back(t, info[t].dist);

                while(info[t].prev != t)
                {
                    t = info[t].prev;
                    items.emplace_back(t, info[t].dist);
                }

                co_yield items;
            }
        }
    }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        auto info = cppcoro::sync_wait(
                engine.template run_scatter_gather<detail::dist_prev, detail::dist_prev>(
            // scatter
            [s](auto&& control) {

                // initialization step
                if(control.superstep() == 0 && control.source_id() == s)
                    control.source_value() = { 0.f, control.source_id() };

                if(control.source_value().dist <
                        std::numeric_limits<float>::max())
                {
                    detail::dist_prev msg = {
                        .prev = control.source_id(),
                        .dist = control.source_value().dist + control.weight()
                    };

                    control.send(msg);
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                auto it = std::min_element(
                    msgs.begin(), msgs.end(),
                    [] (auto& lhs, auto& rhs) {
                        return lhs.dist < rhs.dist;
                    });

                if(it != msgs.end() && it->dist < control.value().dist)
                    control.value() = *it;
                else
                    control.vote_to_halt();
            },
            // default state
            { .dist = std::numeric_limits<float>::max(), .prev = 0 }));

        for(uint32_t i = 0; i < graph.n_vertices(); ++i)
        {
            uint32_t t = i;
            if(info[t].dist < std::numeric_limits<float>::max() &&
                    info[t].prev != t)
            {
                std::vector<std::pair<uint32_t, float>> items;
                items.emplace_back(t, info[t].dist);

                while(info[t].prev != t)
                {
                    t = info[t].prev;
                    items.emplace_back(t, info[t].dist);
                }

                co_yield items;
            }
        }
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}

template <typename Graph>
cppcoro::generator<std::vector<std::pair<uint32_t, float>>>
    apsp(const Graph& graph, execution exec)
{
    if(exec == execution::seq)
    {
        size_t n_vertices = graph.n_vertices();

        detail::fixed_matrix<float> dist(
                n_vertices, n_vertices,
                std::numeric_limits<float>::max());
        detail::fixed_matrix<uint32_t> prev(dist.n_rows(), dist.n_colums());

        for(auto e : graph.edges())
        {
            dist.write(e.u, e.v, e.weight());
            prev.write(e.u, e.v, e.u);
        }

        for(uint32_t u = 0; u < n_vertices; ++u)
        {
            dist.write(u, u, 0.f);
            prev.write(u, u, u);
        }

        for(uint32_t k = 0; k < n_vertices; ++k)
            for(uint32_t u = 0; u < n_vertices; ++u)
                for(uint32_t v = 0; v < n_vertices; ++v)
                    if(dist.read(u, k) + dist.read(k, v) < dist.read(u, v))
                    {
                        dist.write(u, v, dist.read(u, k) + dist.read(k, v));
                        prev.write(u, v, prev.read(k, v));
                    }

        for(uint32_t u = 0; u < n_vertices; ++u)
        {
            for(uint32_t v = 0; v < n_vertices; ++v)
            {
                if(dist.read(u, v) < std::numeric_limits<float>::max()
                        && prev.read(u, v) != v)
                {
                    std::vector<std::pair<uint32_t, float>> items;
                    items.emplace_back(v, dist.read(u, v));

                    while(prev.read(u, v) != v)
                    {
                        v = prev.read(u, v);
                        items.emplace_back(v, dist.read(u, v));
                    }

                    co_yield items;
                }
            }
        }
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        using message_t = std::pair<uint32_t, detail::dist_prev>;
        size_t n_vertices = graph.n_vertices();

        auto info = cppcoro::sync_wait(
                engine.template run_compute<std::vector<detail::dist_prev>, message_t>(
            // compute
            [](auto&& control) {

                // send message to my neighbours with source (msg.first) being me
                if(control.superstep() == 0)
                {
                    // set my initial data
                    control.value()[control.id()].dist = 0.f;
                    control.value()[control.id()].prev = control.id();

                    // prepare and send message to my neighbours
                    for(auto n : control.out_neighbours())
                    {
                        message_t msg = {
                            control.id(),
                            { .dist = n.weight(), .prev = control.id()
                        }};

                        control.send_to(n.v, msg);
                    }
                }
                else
                {
                    // aggregate messages by source using min()
                    std::unordered_map<uint32_t, detail::dist_prev> agg_map;
                    for(auto& msg : control.messages())
                        if(msg.second.dist < agg_map[msg.first].dist)
                            agg_map[msg.first] = msg.second;

                    // for each source message with minimal distance
                    for(auto [s, info] : agg_map)
                    {
                        // send message to my neighbours with source
                        // (msg.first) being s whether message dist
                        // is lesser than what I know
                        if(info.dist < control.value()[s].dist)
                        {
                            // update what I had
                            control.value()[s] = info;

                            // prepare and send msg to my neighbours
                            for(auto n : control.out_neighbours())
                            {
                                message_t msg = {
                                    s,
                                    {
                                        .dist = info.dist + n.weight(),
                                        .prev = control.id()
                                    }};

                                control.send_to(n.v, msg);
                            }
                        }
                    }
                }

                // i'm done
                control.vote_to_halt();
            },
            // default state
            std::vector<detail::dist_prev>(n_vertices)));

        for(uint32_t u = 0; u < n_vertices; ++u)
        {
            uint32_t s = u;
            for(uint32_t v = 0; v < n_vertices; ++v)
            {
                uint32_t t = v;
                if(info[v][u].dist < std::numeric_limits<float>::max() &&
                        info[v][u].prev != v)
                {
                    std::vector<std::pair<uint32_t, float>> items;
                    items.emplace_back(t, info[t][s].dist);

                    while(info[t][s].prev != t)
                    {
                        t = info[t][s].prev;
                        items.emplace_back(t, info[t][s].dist);
                    }

                    co_yield items;
                }
            }
        }
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        using message_t = std::pair<uint32_t, detail::dist_prev>;
        size_t n_vertices = graph.n_vertices();

        auto info = cppcoro::sync_wait(
                engine.template run_scatter_gather<std::vector<detail::dist_prev>, message_t>(
            // scatter
            [](auto&& control) {

                if(control.superstep() == 0)
                {
                    control.value()[control.id()].dist = 0.f;
                    control.value()[control.id()].prev = control.id();

                    message_t msg = {
                        control.id(),
                        { .prev = control.id() }
                    };

                    for(auto n : control.out_neighbours())
                    {
                        msg.second.dist =  n.weight();
                        control.send_to(n.v, msg);
                    }
                }
                else
                {
                    message_t msg = { 0, { .prev = control.id() }};

                    for(size_t s = 0; s < control.value().size(); ++s)
                    {
                        for(auto n : control.out_neighbours())
                        {
                            float neigh_dist =
                                control.value()[s].dist + n.weight();

                            if(neigh_dist <
                                    control.neighbour_value(n.v)[s].dist)
                            {
                                msg.first = s;
                                msg.second.dist = neigh_dist;

                                control.send_to(n.v, msg);
                            }
                        }
                    }
                }
            },
            // gather
            [](auto&& control) {

                std::unordered_map<uint32_t, detail::dist_prev> agg_map;
                for(auto& msg : control.messages())
                    if(msg.second.dist < agg_map[msg.first].dist)
                        agg_map[msg.first] = msg.second;

                bool updated = false;
                for(auto [s, info] : agg_map)
                {
                    if(info.dist < control.value()[s].dist)
                    {
                        control.value()[s] = info;
                        updated = true;
                    }
                }

                if(!updated)
                    control.vote_to_halt();
            },
            // default state
            std::vector<detail::dist_prev>(n_vertices)));

        for(uint32_t u = 0; u < n_vertices; ++u)
        {
            uint32_t s = u;
            for(uint32_t v = 0; v < n_vertices; ++v)
            {
                uint32_t t = v;
                if(info[v][u].dist < std::numeric_limits<float>::max() &&
                        info[v][u].prev != v)
                {
                    std::vector<std::pair<uint32_t, float>> items;
                    items.emplace_back(t, info[t][s].dist);

                    while(info[t][s].prev != t)
                    {
                        t = info[t][s].prev;
                        items.emplace_back(t, info[t][s].dist);
                    }

                    co_yield items;
                }
            }
        }
    }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        using message_t = std::pair<uint32_t, detail::dist_prev>;
        size_t n_vertices = graph.n_vertices();

        auto info = cppcoro::sync_wait(
                engine.template run_scatter_gather<std::vector<detail::dist_prev>, message_t>(
            // scatter
            [](auto&& control) {

                if(control.superstep() == 0)
                {
                    control.source_value()[control.source_id()].dist = 0.f;
                    control.source_value()[control.source_id()].prev =
                            control.source_id();

                    message_t msg = {
                        control.source_id(),
                        {
                            .prev = control.source_id(),
                            .dist = control.weight()
                        }};

                    control.send(msg);
                }
                else
                {
                    message_t msg = { 0, { .prev = control.source_id()}};

                    for(size_t s = 0; s < control.source_value().size(); ++s)
                    {
                        float w = control.weight();
                        float neigh_dist = control.source_value()[s].dist + w;

                        if(neigh_dist < control.target_value()[s].dist)
                        {
                            msg.first = s;
                            msg.second.dist = neigh_dist;

                            control.send(msg);
                        }
                    }
                }
            },
            // gather
            [](auto&& control) {

                std::unordered_map<uint32_t, detail::dist_prev> agg_map;
                for(auto& msg : control.messages())
                    if(msg.second.dist < agg_map[msg.first].dist)
                        agg_map[msg.first] = msg.second;

                bool updated = false;
                for(auto [s, info] : agg_map)
                {
                    if(info.dist < control.value()[s].dist)
                    {
                        control.value()[s] = info;
                        updated = true;
                    }
                }

                if(!updated)
                    control.vote_to_halt();
            },
            // default state
            std::vector<detail::dist_prev>(n_vertices)));

        for(uint32_t u = 0; u < n_vertices; ++u)
        {
            uint32_t s = u;
            for(uint32_t v = 0; v < n_vertices; ++v)
            {
                uint32_t t = v;
                if(info[v][u].dist < std::numeric_limits<float>::max() &&
                        info[v][u].prev != v)
                {
                    std::vector<std::pair<uint32_t, float>> items;
                    items.emplace_back(t, info[t][s].dist);

                    while(info[t][s].prev != t)
                    {
                        t = info[t][s].prev;
                        items.emplace_back(t, info[t][s].dist);
                    }

                    co_yield items;
                }
            }
        }
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}


using sp_item = std::pair<uint32_t, float>;
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<sp_item>,
        spsp, uint32_t, uint32_t, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<std::vector<sp_item>>,
        sssp, uint32_t, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<std::vector<sp_item>>,
        apsp, execution)

} /* stg */
