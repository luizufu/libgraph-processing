#include <libgraph-processing/algorithm/traverse.hxx>

#include <libgraph-processing/algorithm/vertex-centric-engine.hxx>
#include <libgraph-processing/algorithm/edge-centric-engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <queue>
#include <stack>
#include <vector>
#include <numeric>
#include <iostream>

namespace stg
{

template <typename Graph>
cppcoro::generator<uint32_t>
    bfs(const Graph& graph, uint32_t s, execution exec)
{
    if(exec == execution::seq)
    {
        std::queue<uint32_t> q;
        std::vector<bool> visited(graph.n_vertices(), false);
        q.push(s);

        while(!q.empty())
        {
            uint32_t u = q.front(); q.pop();
            visited[u] = true;
            co_yield u;

            for(auto n : graph.out_neighbours(u))
                if(!visited[n.v])
                    q.push(n.v);
        }
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);

        auto order = cppcoro::sync_wait(
                engine.template run_compute<uint32_t, uint32_t>(
            // compute
            [s](auto&& control) {

                // if not visited yet
                if(control.value() == 0)
                {
                    auto& msgs = control.messages();

                    // if it's the source vertex at first iteration or
                    // it's some vertex that received a new message
                    if((control.superstep() == 0 && control.id() == s)
                        || !msgs.empty())
                    {
                        // set my order to 1 + max element in messages
                        uint32_t my_order = 1;
                        if(!msgs.empty())
                            my_order += *std::max_element(
                                    msgs.begin(), msgs.end());

                        control.set_value(my_order);

                        // send my order to my neighbours
                        for(auto n : control.out_neighbours())
                            control.send_to(n.v, my_order);
                    }
                }

                // i'm done
                control.vote_to_halt();
            },
            //default_value
            0));

        std::vector<uint32_t> inds(order.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&order] (uint32_t lhs, uint32_t rhs) {
                return order[lhs] < order[rhs];
            });

        for(auto i : inds)
            if(order[i] > 0)
                co_yield i;
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto order = cppcoro::sync_wait(
                engine.template run_scatter_gather<uint32_t, uint32_t>(
            // scatter
            [s](auto&& control) {

                if(control.id() == s && control.superstep() == 0)
                    control.value() = 1;

                if(control.value() > 0)
                    for(auto n : control.out_neighbours())
                        control.send_to(n.v, control.value() + 1);
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();

                if(control.value() == 0 && !msgs.empty())
                    control.value() = *std::max_element(
                            msgs.begin(), msgs.end());
                else
                    control.vote_to_halt();
            },
            //default_value
            0));

        std::vector<uint32_t> inds(order.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&order] (uint32_t lhs, uint32_t rhs) {
                return order[lhs] < order[rhs];
            });

        for(auto i : inds)
            if(order[i] > 0)
                co_yield i;
    }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        auto order = cppcoro::sync_wait(
                engine.template run_scatter_gather<uint32_t, uint32_t>(
            // scatter
            [s](auto&& control) {

                if(control.source_id() == s && control.superstep() == 0)
                    control.source_value() = 1;

                if(control.source_value() > 0)
                    control.send(control.source_value() + 1);
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();

                if(control.value() == 0 && !msgs.empty())
                    control.value() = *std::max_element(
                            msgs.begin(), msgs.end());
                else
                    control.vote_to_halt();
            },
            //default_value
            0));

        std::vector<uint32_t> inds(order.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&order] (uint32_t lhs, uint32_t rhs) {
                return order[lhs] < order[rhs];
            });

        for(auto i : inds)
            if(order[i] > 0)
                co_yield i;
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}

template <typename Graph>
cppcoro::generator<uint32_t>
    dfs(const Graph& graph, uint32_t s, execution exec)
{
    if(exec == execution::seq)
    {
        std::stack<uint32_t> q;
        std::vector<bool> visited(graph.n_vertices(), false);
        q.push(s);

        while(!q.empty())
        {
            uint32_t u = q.top(); q.pop();
            visited[u] = true;
            co_yield u;

            for(auto n : graph.out_neighbours(u))
                if(!visited[n.v])
                    q.push(n.v);
        }
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);
        auto order = cppcoro::sync_wait(
                engine.template run_compute<uint32_t, uint32_t>(
            // compute
            [s](auto&& control) {

                // if not visited yet
                if(control.value() == 0)
                {
                    auto& msgs = control.messages();

                    // if it's the source vertex at first iteration or
                    // it's some vertex that received a new message
                    if((control.superstep() == 0 && control.id() == s)
                        || !msgs.empty())
                    {
                        // set my order to 1 + max element in messages
                        uint32_t my_order = 1;
                        if(!msgs.empty())
                            my_order+= *std::max_element(
                                    msgs.begin(), msgs.end());

                        control.value() = my_order;

                        // send orders to my neighbours that are proportional
                        // to the highest possible number a path could take
                        // aggregated stores the number of vertices not visited
                        size_t i = 0;
                        uint32_t highest =
                            control.aggregated() -
                            control.out_degree();

                        for(auto n : control.out_neighbours())
                            control.send_to(n.v, my_order + 2 + highest * i++);
                    }
                }

                // i'm done
                control.vote_to_halt();
            },
            // default state
            0,
            // aggregator
            [](auto& states) {
                return std::count_if(
                    states.begin(), states.end(),
                    [](uint32_t state) {
                        return state == 0;
                    });
            },
            // default aggregated
            graph.n_vertices()));

        std::vector<uint32_t> inds(order.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&order] (uint32_t lhs, uint32_t rhs) {
                return order[lhs] < order[rhs];
            });

        for(auto i : inds)
            if(order[i] > 0)
                co_yield i;
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);
        auto order = cppcoro::sync_wait(
                engine.template run_scatter_gather<uint32_t, uint32_t>(
            // scatter
            [s](auto&& control) {

                if(control.id() == s && control.superstep() == 0)
                    control.value() = 1;

                if(control.value() > 0)
                {
                    size_t i = 0;
                    uint32_t highest =
                        control.aggregated() -
                        control.out_degree();

                    for(auto n : control.out_neighbours())
                        control.send_to(n.v,
                                control.value() + 2 + highest * i++);
                }

            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();

                if(control.value() == 0 && !msgs.empty())
                    control.value() = *std::max_element(
                            msgs.begin(), msgs.end());
                else
                    control.vote_to_halt();
            },
            // default state
            0,
            // aggregator
            [](auto& states) {
                return std::count_if(
                    states.begin(), states.end(),
                    [](uint32_t state) {
                        return state == 0;
                    });
            },
            // default aggregated
            graph.n_vertices()));

        std::vector<uint32_t> inds(order.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&order] (uint32_t lhs, uint32_t rhs) {
                return order[lhs] < order[rhs];
            });

        for(auto i : inds)
            if(order[i] > 0)
                co_yield i;
    }
    else if(exec == execution::ecsg)
    {
        struct value
        {
            uint32_t order = 0;
            size_t degree = 0;
            size_t i = 0;
        };

        edge_centric_engine engine(graph);
        auto order = cppcoro::sync_wait(
                engine.template run_scatter_gather<value, uint32_t>(
            // scatter
            [s](auto&& control) {
                if(control.source_id() == s)
                    control.source_value().order = 1;

                if(control.superstep() == 0)
                {
                    ++control.source_value().degree;
                }
                else if(control.source_value().order > 0)
                {
                    uint32_t highest =
                        control.aggregated().order -
                        control.source_value().degree;

                    uint32_t shift = 2 + highest * control.source_value().i++;
                    control.send(control.source_value().order + shift );

                    if(control.source_value().i >control.source_value().degree)
                        control.source_value().i = 0;
                }
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();

                if(control.value().order == 0 && !msgs.empty())
                    control.value().order = *std::max_element(
                            msgs.begin(), msgs.end());
                else
                    control.vote_to_halt();
            },
            // default state
            value{},
            // aggregator
            [](auto& states) {
                value aggregated;

                aggregated.order = std::count_if(
                    states.begin(), states.end(),
                    [](auto& state) {
                        return state.order == 0;
                    });

                return aggregated;
            },
            // default aggregated
            { .order = (uint32_t) graph.n_vertices() }));

        std::vector<uint32_t> inds(order.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&order] (uint32_t lhs, uint32_t rhs) {
                return order[lhs].order < order[rhs].order;
            });

        for(auto i : inds)
            if(order[i].order > 0)
                co_yield i;
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}

INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<uint32_t>,
        bfs, uint32_t, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<uint32_t>,
        dfs, uint32_t, execution)

} /* stg */
