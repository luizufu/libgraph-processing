#include <libgraph-processing/algorithm/connectivity.hxx>

#include <libgraph-processing/algorithm/vertex-centric-engine.hxx>
#include <libgraph-processing/algorithm/edge-centric-engine.hxx>
#include <cppcoro/sync_wait.hpp>
#include <queue>
#include <stack>
#include <vector>
#include <numeric>
#include <iostream>

namespace stg
{

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, uint8_t>>
    cc(const Graph& graph, execution exec)
{
    if(exec == execution::seq)
    {
        std::stack<uint32_t> q;
        std::vector<bool> visited(graph.n_vertices(), false);
        size_t id = 0;

        for(size_t s = 0; s < graph.n_vertices(); ++s)
        {
            if(!visited[s])
            {
                q.push(s);

                while(!q.empty())
                {
                    uint32_t u = q.top(); q.pop();
                    visited[u] = true;

                    co_yield { u, id };

                    for(auto n : graph.out_neighbours(u))
                        if(!visited[n.v])
                            q.push(n.v);
                }

                ++id;
            }
        }
    }
    else if(exec == execution::vcc)
    {
        vertex_centric_engine engine(graph);
        auto colors = cppcoro::sync_wait(
                engine.template run_compute<uint32_t, uint32_t>(
            // compute
            [](auto&& control) {

                auto& msgs = control.messages();

                // set my color to be my id at first iteration
                if(control.superstep() == 0)
                    control.set_value(control.id());

                uint32_t other_color = !msgs.empty()
                    ? *std::min_element(msgs.begin(), msgs.end())
                    : control.value();

                // if it's the first iteration or it has arrived a different
                // color than set my new color and send to my neighbours
                // we get only a different value less than the current one
                // I think this will no work for directed graphs?
                if(control.superstep() == 0 ||  other_color < control.value())
                {
                    control.set_value(other_color);

                    for(auto n : control.out_neighbours())
                        control.send_to(n.v, other_color);
                }

                // i'm done
                control.vote_to_halt();
            },
            //default_value
            0));

        std::vector<uint32_t> inds(colors.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&colors] (uint32_t lhs, uint32_t rhs) {
                return colors[lhs] < colors[rhs];
            });

        for(auto i : inds)
            co_yield { i, colors[i] };
    }
    else if(exec == execution::vcsg)
    {
        vertex_centric_engine engine(graph);

        auto colors = cppcoro::sync_wait(
                engine.template run_scatter_gather<uint32_t, uint32_t>(
            // scatter
            [](auto&& control) {

                if(control.superstep() == 0)
                    control.value() = control.id();

                for(auto n : control.out_neighbours())
                    control.send_to(n.v, control.value());
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                auto it = std::min_element(msgs.begin(), msgs.end());

                if(it != msgs.end() && *it < control.value())
                    control.value() = *it;
                else
                    control.vote_to_halt();
            },
            //default_value
            0));

        std::vector<uint32_t> inds(colors.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&colors] (uint32_t lhs, uint32_t rhs) {
                return colors[lhs] < colors[rhs];
            });

        for(auto i : inds)
            co_yield { i, colors[i] };
    }
    else if(exec == execution::ecsg)
    {
        edge_centric_engine engine(graph);

        auto colors = cppcoro::sync_wait(
                engine.template run_scatter_gather<uint32_t, uint32_t>(
            // scatter
            [](auto&& control) {

                if(control.superstep() == 0)
                    control.source_value() = control.source_id();

                control.send(control.source_value());
            },
            // gather
            [](auto&& control) {

                auto& msgs = control.messages();
                auto it = std::min_element(msgs.begin(), msgs.end());

                if(it != msgs.end() && *it < control.value())
                    control.value() = *it;
                else
                    control.vote_to_halt();
            },
            //default_value
            0));

        std::vector<uint32_t> inds(colors.size());
        std::iota(inds.begin(), inds.end(), 0);

        std::sort(
            inds.begin(), inds.end(),
            [&colors] (uint32_t lhs, uint32_t rhs) {
                return colors[lhs] < colors[rhs];
            });

        for(auto i : inds)
            co_yield { i, colors[i] };
    }
    else
    {
        std::cout << "Not Implemented Yet" << std::endl;
    }
}


using cc_item = std::pair<uint32_t, uint8_t>;
INSTATIATE_ALGO_FUNC_TEMPLATES(
        cppcoro::generator<cc_item>,
        cc, execution)

} /* stg */
