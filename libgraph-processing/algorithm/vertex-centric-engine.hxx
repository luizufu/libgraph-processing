#pragma once

#include <cppcoro/task.hpp>
#include <cppcoro/static_thread_pool.hpp>
#include <cppcoro/sync_wait.hpp>
#include <cppcoro/when_all.hpp>
#include <cstddef>
#include <vector>
#include <unordered_map>
#include <mutex>
#include <atomic>
#include <iostream>


namespace stg
{

template <typename Graph>
class vertex_centric_engine
{
    const Graph& _graph;


    template <typename Value, typename Message>
    struct global_state;

    template <typename Value>
    struct vertex_state;

    template <typename Value, typename Message>
    class vertex_control;

    template <typename Value, typename Message>
    class sg_vertex_control;

public:

    vertex_centric_engine(const Graph& graph)
        : _graph(graph)
    {
    }

    template <typename Value,
              typename Message = Value,
              typename Compute>
    cppcoro::task<std::vector<Value>> run_compute(Compute compute,
             Value default_value = {},
             uint32_t n_threads = std::thread::hardware_concurrency())
    {
        size_t n_vertices = _graph.n_vertices();
        cppcoro::static_thread_pool thread_pool(n_threads);

        global_state<Value, Message> gstate(_graph, default_value);
        vertex_state<Value> vstate(n_vertices, default_value);

        std::mutex print;

        auto run_task =
            [&](uint32_t i)
            -> cppcoro::task<> {
                co_await thread_pool.schedule();

                // execute user-defined function
                compute(vertex_control<Value, Message>(i, gstate, vstate));
            };

        while(gstate.halt_count < n_vertices || gstate.has_new_message)
        {
            gstate.has_new_message = false;
            gstate.halt_count = 0;

            std::vector<cppcoro::task<>> tasks;
            tasks.reserve(n_vertices);

            for(size_t i = 0; i < n_vertices; ++i)
            {
                // deliver messages to inbox if there are any
                if(gstate.out_messages.count(i))
                {
                    gstate.in_messages[i] = std::move(gstate.out_messages[i]);
                    gstate.out_messages.erase(i);
                    vstate.is_active[i] = true;
                }

                if(vstate.is_active[i])
                    tasks.emplace_back(run_task(i));
                else
                    ++gstate.halt_count;
            }

            cppcoro::sync_wait(cppcoro::when_all(std::move(tasks)));
            ++gstate.superstep;
        }

        co_return vstate.values;
    }

    template <typename Value,
              typename Message = Value,
              typename Compute,
              typename Aggregator>
    cppcoro::task<std::vector<Value>> run_compute(
            Compute compute, Value default_state_value,
            Aggregator aggregator, Value default_aggregate_value,
            uint32_t n_threads = std::thread::hardware_concurrency())
    {
        size_t n_vertices = _graph.n_vertices();
        cppcoro::static_thread_pool thread_pool(n_threads);

        global_state<Value, Message> gstate(_graph, default_aggregate_value);
        vertex_state<Value> vstate(n_vertices, default_state_value);

        auto run_task =
            [&](uint32_t i)
            -> cppcoro::task<> {
                co_await thread_pool.schedule();

                compute(vertex_control<Value, Message>(i, gstate, vstate));
            };

        while(gstate.halt_count < n_vertices || gstate.has_new_message)
        {
            gstate.has_new_message = false;
            gstate.halt_count = 0;

            std::vector<cppcoro::task<>> tasks;
            tasks.reserve(n_vertices);

            for(size_t i = 0; i < n_vertices; ++i)
            {
                if(gstate.out_messages.count(i))
                {
                    gstate.in_messages[i] = std::move(gstate.out_messages[i]);
                    gstate.out_messages.erase(i);
                    vstate.is_active[i] = true;
                }

                if(vstate.is_active[i])
                    tasks.emplace_back(run_task(i));
                else
                    ++gstate.halt_count;
            }

            cppcoro::sync_wait(cppcoro::when_all(std::move(tasks)));
            gstate.aggregated = aggregator(vstate.values);
            ++gstate.superstep;
        }

        co_return vstate.values;
    }


    template <typename Value,
              typename Message,
              typename Scatter,
              typename Gather>
    cppcoro::task<std::vector<Value>> run_scatter_gather(
             Scatter scatter, Gather gather,
             Value default_value = {},
             uint32_t n_threads = std::thread::hardware_concurrency())
    {
        size_t n_vertices = _graph.n_vertices();
        cppcoro::static_thread_pool thread_pool(n_threads);

        global_state<Value, Message> gstate(_graph, default_value);
        vertex_state<Value> vstate(n_vertices, default_value);

        auto scatter_task =
            [&](uint32_t i)
            -> cppcoro::task<> {
                co_await thread_pool.schedule();

                scatter(sg_vertex_control<Value, Message>(i, gstate, vstate));
            };


        auto gather_task =
            [&](uint32_t i)
            -> cppcoro::task<> {
                co_await thread_pool.schedule();

                gather(sg_vertex_control<Value, Message>(i, gstate, vstate));
            };

        while(gstate.halt_count < n_vertices)
        {
            gstate.halt_count = 0;

            std::vector<cppcoro::task<>> scatter_tasks;
            scatter_tasks.reserve(n_vertices);

            for(size_t i = 0; i < n_vertices; ++i)
            {
                if(vstate.is_active[i])
                {
                    gstate.in_messages[i].clear();
                    scatter_tasks.emplace_back(scatter_task(i));
                }
            }

            cppcoro::sync_wait(cppcoro::when_all(std::move(scatter_tasks)));


            std::vector<cppcoro::task<>> gather_tasks;
            gather_tasks.reserve(n_vertices);

            for(size_t i = 0; i < n_vertices; ++i)
            {
                if(gstate.in_messages.count(i))
                    vstate.is_active[i] = true;

                if(vstate.is_active[i])
                    gather_tasks.emplace_back(gather_task(i));
                else
                    ++gstate.halt_count;
            }

            cppcoro::sync_wait(cppcoro::when_all(std::move(gather_tasks)));
            ++gstate.superstep;
        }

        co_return vstate.values;
    }

    template <typename Value,
              typename Message,
              typename Scatter,
              typename Gather,
              typename Aggregator>
    cppcoro::task<std::vector<Value>> run_scatter_gather(
             Scatter scatter, Gather gather, Value default_state_value,
             Aggregator aggregator, Value default_aggregate_value,
             uint32_t n_threads = std::thread::hardware_concurrency())
    {
        size_t n_vertices = _graph.n_vertices();
        cppcoro::static_thread_pool thread_pool(n_threads);

        global_state<Value, Message> gstate(_graph, default_aggregate_value);
        vertex_state<Value> vstate(n_vertices, default_state_value);

        auto scatter_task =
            [&](uint32_t i)
            -> cppcoro::task<> {
                co_await thread_pool.schedule();

                scatter(sg_vertex_control<Value, Message>(i, gstate, vstate));
            };


        auto gather_task =
            [&](uint32_t i)
            -> cppcoro::task<> {
                co_await thread_pool.schedule();

                gather(sg_vertex_control<Value, Message>(i, gstate, vstate));
            };

        while(gstate.halt_count < n_vertices)
        {
            gstate.halt_count = 0;

            std::vector<cppcoro::task<>> scatter_tasks;
            scatter_tasks.reserve(n_vertices);

            for(size_t i = 0; i < n_vertices; ++i)
            {
                if(vstate.is_active[i])
                {
                    gstate.in_messages[i].clear();
                    scatter_tasks.emplace_back(scatter_task(i));
                }
            }

            cppcoro::sync_wait(cppcoro::when_all(std::move(scatter_tasks)));


            std::vector<cppcoro::task<>> gather_tasks;
            gather_tasks.reserve(n_vertices);

            for(size_t i = 0; i < n_vertices; ++i)
            {
                if(gstate.in_messages.count(i))
                    vstate.is_active[i] = true;

                if(vstate.is_active[i])
                    gather_tasks.emplace_back(gather_task(i));
                else
                    ++gstate.halt_count;
            }

            cppcoro::sync_wait(cppcoro::when_all(std::move(gather_tasks)));
            gstate.aggregated = aggregator(vstate.values);
            ++gstate.superstep;
        }

        co_return vstate.values;
    }
};


template <typename Graph>
template <typename Value, typename Message>
struct vertex_centric_engine<Graph>::global_state
{
    size_t superstep = 0;
    std::atomic<size_t> halt_count = 0;//vertices send vote to halt asynchronously
    Value aggregated;
    const Graph& graph;
    std::unordered_map<uint32_t, std::vector<Message>> in_messages;

    // different vertices can update these asynchronously
    std::mutex out_messages_mutex;
    bool has_new_message = false;
    std::unordered_map<uint32_t, std::vector<Message>> out_messages;

    global_state(const Graph& g, Value default_value)
        : graph(g)
        , in_messages(g.n_vertices())
        , out_messages(in_messages.size())
        , aggregated(default_value)
    {
    }
};

template <typename Graph>
template <typename Value>
struct vertex_centric_engine<Graph>::vertex_state
{
    std::vector<Value> values;
    std::vector<bool> is_active;

    vertex_state(size_t size, Value default_value)
        : values(size, default_value)
        , is_active(size, true)
    {
    }
};



template <typename Graph>
template <typename Value, typename Message>
class vertex_centric_engine<Graph>::vertex_control
{
    template <typename>
    friend class vertex_centric_engine;

    uint32_t _i;
    global_state<Value, Message>& _gstate;
    vertex_state<Value>& _vstate;

    vertex_control(uint32_t i,
                   global_state<Value, Message>& gstate,
                   vertex_state<Value>& vstate)
        : _i(i)
        , _gstate(gstate)
        , _vstate(vstate)
    {
    }

public:

    uint32_t id() const
    {
        return _i;
    }

    Value& value()
    {
        return _vstate.values[_i];
    }

    void set_value(Value value)
    {
        _vstate.values[_i] = value;
    }


    std::vector<Message>& messages()
    {
        return _gstate.in_messages[_i];
    }

    void send_to(uint32_t j, const Message& message)
    {
        std::lock_guard lock(_gstate.out_messages_mutex);
        _gstate.has_new_message = true;
        _gstate.out_messages[j].push_back(message);
    }

    auto out_neighbours() const
    {
        return _gstate.graph.out_neighbours(_i);
    }

    size_t out_degree() const
    {
        return _gstate.graph.out_degree(_i);
    }

    size_t n_vertices() const
    {
        return _gstate.graph.n_vertices();
    }

    size_t superstep() const
    {
        return _gstate.superstep;
    }

    Value aggregated() const
    {
        return _gstate.aggregated;
    }

    void vote_to_halt()
    {
        ++_gstate.halt_count;
        _vstate.is_active[_i] = false;
    }
};

template <typename Graph>
template <typename Value, typename Message>
class vertex_centric_engine<Graph>::sg_vertex_control
{
    template <typename>
    friend class vertex_centric_engine;

    uint32_t _i;
    global_state<Value, Message>& _gstate;
    vertex_state<Value>& _vstate;

    sg_vertex_control(uint32_t i,
                   global_state<Value, Message>& gstate,
                   vertex_state<Value>& vstate)
        : _i(i)
        , _gstate(gstate)
        , _vstate(vstate)
    {
    }

public:

    uint32_t id() const
    {
        return _i;
    }

    Value& value()
    {
        return _vstate.values[_i];
    }

    // maybe useful for scatter-gather?
    // check graph-reader/algo/vcsg-apsp.txx
    Value neighbour_value(uint32_t neigh)
    {
        return _vstate.values[neigh];
    }

    void set_value(Value value)
    {
        _vstate.values[_i] = value;
    }


    std::vector<Message>& messages()
    {
        return _gstate.in_messages[_i];
    }

    void send_to(uint32_t j, const Message& message)
    {
        std::lock_guard lock(_gstate.out_messages_mutex);
        _gstate.in_messages[j].push_back(message);
    }


    auto out_neighbours() const
    {
        return _gstate.graph.out_neighbours(_i);
    }

    size_t out_degree() const
    {
        return _gstate.graph.out_degree(_i);
    }

    size_t n_vertices() const
    {
        return _gstate.graph.n_vertices();
    }

    size_t superstep() const
    {
        return _gstate.superstep;
    }

    Value aggregated() const
    {
        return _gstate.aggregated;
    }

    void vote_to_halt()
    {
        ++_gstate.halt_count;
        _vstate.is_active[_i] = false;
    }
};

} /* stg */
