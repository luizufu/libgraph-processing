#pragma once

#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/detail/macro/algofunc.hxx>
#include <cppcoro/generator.hpp>
#include <vector>
#include <utility>
#include <cstdint>

namespace stg
{

template <typename Graph>
cppcoro::generator<std::pair<uint32_t, float>>
    spsp(const Graph& graph, uint32_t s, uint32_t t, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<std::vector<std::pair<uint32_t, float>>>
    sssp(const Graph& graph, uint32_t s, execution exec = execution::seq);

template <typename Graph>
cppcoro::generator<std::vector<std::pair<uint32_t, float>>>
    apsp(const Graph& graph, execution exec = execution::seq);

}


using sp_item = std::pair<uint32_t, float>;
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<sp_item>,
        spsp, uint32_t, uint32_t, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<std::vector<sp_item>>,
        sssp, uint32_t, execution)
INSTATIATE_ALGO_FUNC_TEMPLATES_EXTERN(
        cppcoro::generator<std::vector<sp_item>>,
        apsp, execution)

