#pragma once

#include <cstdint>
#include <type_traits>
#include <sstream>


namespace stg
{

struct edge
{
    union
    {
        uint64_t e;
        struct {
            uint32_t v;
            uint32_t u;
        };
    };

    float weight() const
    {
        return 1.f;
    }

} __attribute__ ((packed));

struct weighted_edge
{
    union
    {
        uint64_t e;
        struct {
            uint32_t v;
            uint32_t u;
        };
    };

    float w;

    float weight() const
    {
        return w;
    }

} __attribute__ ((packed));

struct neighbor
{
    uint32_t v;

    float weight() const
    {
        return 1.f;
    }

} __attribute__ ((packed));

struct weighted_neighbor
{
    uint32_t v;
    float w;

    float weight() const
    {
        return 1.f;
    }

} __attribute__ ((packed));


template <typename Edge>
using neighbor_from_t = std::conditional_t<std::is_same_v<Edge, edge>, neighbor, weighted_neighbor>;

template <typename Neighbour>
using edge_from_t = std::conditional_t<std::is_same_v<Neighbour, neighbor>, edge, weighted_edge>;

inline neighbor edge_to_out_neighbor(edge e)
{
    neighbor n;
    n.v = e.v;

    return n;
}

inline neighbor edge_to_in_neighbor(edge e)
{
    neighbor n;
    n.v = e.u;

    return n;
}


inline weighted_neighbor edge_to_out_neighbor(weighted_edge e)
{
    weighted_neighbor n;
    n.v = e.v;
    n.w = e.w;

    return n;
}

inline weighted_neighbor edge_to_in_neighbor(weighted_edge e)
{
    weighted_neighbor n;
    n.v = e.u;
    n.w = e.w;

    return n;
}

inline edge make_edge(uint32_t u, neighbor n)
{
    edge e;
    e.u = u;
    e.v = n.v;

    return e;
}

inline edge make_edge(neighbor n, uint32_t v)
{
    edge e;
    e.u = n.v;
    e.v = v;

    return e;
}

inline weighted_edge make_edge(uint32_t u, weighted_neighbor n)
{
    weighted_edge e;
    e.u = u;
    e.v = n.v;
    e.w = n.w;

    return e;
}

inline weighted_edge make_edge(weighted_neighbor n, uint32_t v)
{
    weighted_edge e;
    e.u = n.v;
    e.v = v;
    e.w = n.w;

    return e;
}

inline bool operator==(const edge& lhs, const edge& rhs)
{
    return lhs.u == rhs.u && lhs.v == rhs.v;
}

inline bool operator!=(const edge& lhs, const edge& rhs)
{
    return !(lhs == rhs);
}

inline bool operator==(const weighted_edge& lhs, const weighted_edge& rhs)
{
    return lhs.u == rhs.u && lhs.v == rhs.v;
}

inline bool operator!=(const weighted_edge& lhs, const weighted_edge& rhs)
{
    return !(lhs == rhs);
}

inline bool operator==(const neighbor& lhs, const neighbor& rhs)
{
    return lhs.v == rhs.v;
}

inline bool operator!=(const neighbor& lhs, const neighbor& rhs)
{
    return !(lhs == rhs);
}

inline bool operator==(const weighted_neighbor& lhs, const weighted_neighbor& rhs)
{
    return lhs.v == rhs.v;
}

inline bool operator!=(const weighted_neighbor& lhs, const weighted_neighbor& rhs)
{
    return !(lhs == rhs);
}

inline bool operator<(const edge& lhs, const edge& rhs)
{
    auto plhs = std::make_pair(lhs.u, lhs.v);
    auto prhs = std::make_pair(rhs.u, rhs.v);

    return plhs < prhs;
}

inline bool operator<(const weighted_edge& lhs, const weighted_edge& rhs)
{
    auto plhs = std::make_pair(lhs.u, lhs.v);
    auto prhs = std::make_pair(rhs.u, rhs.v);

    return plhs < prhs;
}

inline bool operator<(const neighbor& lhs, const neighbor& rhs)
{
    return lhs.v < rhs.v;
}

inline bool operator<(const weighted_neighbor& lhs, const weighted_neighbor& rhs)
{
    return lhs.v < rhs.v;
}

inline std::ostream& operator<<(std::ostream &out, const edge &e)
{
    out << e.u << ' ' <<  e.v;
    return out;
}

inline std::istream & operator>>(std::istream &in,  edge &e)
{
    in >> e.u >> e.v;
    return in;
}

inline std::ostream& operator<<(std::ostream &out, const weighted_edge &e)
{
    out << e.u << ' ' <<  e.v << ' ' << e.w;
    return out;
}

inline std::istream & operator>>(std::istream &in,  weighted_edge &e)
{
    in >> e.u >> e.v >> e.w;
    return in;
}

inline std::ostream& operator<<(std::ostream &out, const neighbor &n)
{
    out << n.v;
    return out;
}

inline std::istream & operator>>(std::istream &in,  neighbor &n)
{
    in >> n.v;
    return in;
}

inline std::ostream& operator<<(std::ostream &out, const weighted_neighbor &n)
{
    out << n.v << ' ' << n.w;
    return out;
}

inline std::istream & operator>>(std::istream &in,  weighted_neighbor &n)
{
    in >> n.v >> n.w;
    return in;
}

} /* stg */
