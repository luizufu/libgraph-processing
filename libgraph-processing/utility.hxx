#pragma once

#include <libgraph-processing/detail/magic.hxx>
#include <optional>
#include <string>
#include <unistd.h>
#include <fcntl.h>


namespace stg
{

enum class execution
{
    seq,
    vcsg,
    vcc,
    ecsg,
    ecc,
};


using direction_t = uint32_t;

const direction_t OUTGOING = 0;
const direction_t INCOMING = 1;
const direction_t OUTGOING_INCOMING = 2;
const direction_t UNKNOWN = 3;

struct graph_props
{
    std::string format;
    std::string type;
    bool weighted;
    bool dynamic;
    bool compressed;
    bool sorted;
};

enum class graph_type : uint32_t
{
    direct_edge_list = 0,
    direct_adj_list,
    direct_edge_grid,
    undirect_edge_list,
    undirect_adj_list,
    undirect_edge_grid,
    weighted_direct_edge_list,
    weighted_direct_adj_list,
    weighted_direct_edge_grid,
    weighted_undirect_edge_list,
    weighted_undirect_adj_list,
    weighted_undirect_edge_grid,
    dynamic_direct_edge_list,
    dynamic_direct_adj_list,
    dynamic_direct_edge_grid,
    dynamic_undirect_edge_list,
    dynamic_undirect_adj_list,
    dynamic_undirect_edge_grid,
    dynamic_weighted_direct_edge_list,
    dynamic_weighted_direct_adj_list,
    dynamic_weighted_direct_edge_grid,
    dynamic_weighted_undirect_edge_list,
    dynamic_weighted_undirect_adj_list,
    dynamic_weighted_undirect_edge_grid,
    compressed_direct_edge_list,
    compressed_direct_adj_list,
    compressed_direct_edge_grid,
    compressed_undirect_edge_list,
    compressed_undirect_adj_list,
    compressed_undirect_edge_grid,
    compressed_weighted_direct_edge_list,
    compressed_weighted_direct_adj_list,
    compressed_weighted_direct_edge_grid,
    compressed_weighted_undirect_edge_list,
    compressed_weighted_undirect_adj_list,
    compressed_weighted_undirect_edge_grid,
    compressed_dynamic_direct_edge_list,
    compressed_dynamic_direct_adj_list,
    compressed_dynamic_direct_edge_grid,
    compressed_dynamic_undirect_edge_list,
    compressed_dynamic_undirect_adj_list,
    compressed_dynamic_undirect_edge_grid,
    compressed_dynamic_weighted_direct_edge_list,
    compressed_dynamic_weighted_direct_adj_list,
    compressed_dynamic_weighted_direct_edge_grid,
    compressed_dynamic_weighted_undirect_edge_list,
    compressed_dynamic_weighted_undirect_adj_list,
    compressed_dynamic_weighted_undirect_edge_grid,
    sorted_direct_edge_list,
    sorted_direct_adj_list,
    sorted_direct_edge_grid,
    sorted_undirect_edge_list,
    sorted_undirect_adj_list,
    sorted_undirect_edge_grid,
    sorted_weighted_direct_edge_list,
    sorted_weighted_direct_adj_list,
    sorted_weighted_direct_edge_grid,
    sorted_weighted_undirect_edge_list,
    sorted_weighted_undirect_adj_list,
    sorted_weighted_undirect_edge_grid,
    sorted_dynamic_direct_edge_list,
    sorted_dynamic_direct_adj_list,
    sorted_dynamic_direct_edge_grid,
    sorted_dynamic_undirect_edge_list,
    sorted_dynamic_undirect_adj_list,
    sorted_dynamic_undirect_edge_grid,
    sorted_dynamic_weighted_direct_edge_list,
    sorted_dynamic_weighted_direct_adj_list,
    sorted_dynamic_weighted_direct_edge_grid,
    sorted_dynamic_weighted_undirect_edge_list,
    sorted_dynamic_weighted_undirect_adj_list,
    sorted_dynamic_weighted_undirect_edge_grid,
    sorted_compressed_direct_edge_list,
    sorted_compressed_direct_adj_list,
    sorted_compressed_direct_edge_grid,
    sorted_compressed_undirect_edge_list,
    sorted_compressed_undirect_adj_list,
    sorted_compressed_undirect_edge_grid,
    sorted_compressed_weighted_direct_edge_list,
    sorted_compressed_weighted_direct_adj_list,
    sorted_compressed_weighted_direct_edge_grid,
    sorted_compressed_weighted_undirect_edge_list,
    sorted_compressed_weighted_undirect_adj_list,
    sorted_compressed_weighted_undirect_edge_grid,
    sorted_compressed_dynamic_direct_edge_list,
    sorted_compressed_dynamic_direct_adj_list,
    sorted_compressed_dynamic_direct_edge_grid,
    sorted_compressed_dynamic_undirect_edge_list,
    sorted_compressed_dynamic_undirect_adj_list,
    sorted_compressed_dynamic_undirect_edge_grid,
    sorted_compressed_dynamic_weighted_direct_edge_list,
    sorted_compressed_dynamic_weighted_direct_adj_list,
    sorted_compressed_dynamic_weighted_direct_edge_grid,
    sorted_compressed_dynamic_weighted_undirect_edge_list,
    sorted_compressed_dynamic_weighted_undirect_adj_list,
    sorted_compressed_dynamic_weighted_undirect_edge_grid,
    unknown
};


inline std::optional<std::string> extract_graph_format(const char* filename)
{

    int fd = open(filename, O_RDONLY);

    uint32_t magic;
    read(fd, reinterpret_cast<char*>(&magic), sizeof(magic));

    close(fd);

    if(magic == EDGE_LIST_MAGIC)
        return "edge-list";
    else if(magic == ADJ_LIST_MAGIC)
        return "adj-list";
    else if(magic == EDGE_GRID_MAGIC)
        return "edge-grid";
    else
        return {};
}

inline std::optional<graph_type> extract_graph_type(const char* filename)
{

    int fd = open(filename, O_RDONLY);

    uint32_t magic;
    read(fd, reinterpret_cast<char*>(&magic), sizeof(magic));

    if(!(magic == EDGE_LIST_MAGIC ||
       magic == ADJ_LIST_MAGIC ||
       magic == EDGE_GRID_MAGIC))
    {
        close(fd);
        return {};
    }


    graph_type type;
    read(fd, reinterpret_cast<char*>(&type), sizeof(type));
    close(fd);

    return type;
}

inline graph_type graph_props_to_graph_type(const std::string& format,
                                            const std::string& type,
                                            bool has_weight,
                                            bool is_dynamic,
                                            bool is_compressed,
                                            bool is_sorted)
{
    if(format == "edge-list")
    {
        if(type == "direct")
        {
            if(has_weight)
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_weighted_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_weighted_direct_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_weighted_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::dynamic_weighted_direct_edge_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_weighted_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_weighted_direct_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_weighted_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::weighted_direct_edge_list;
                        }
                    }
                }
            }
            else
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_direct_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::dynamic_direct_edge_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_direct_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_direct_edge_list;
                        }
                        else
                        {
                            return graph_type::direct_edge_list;
                        }
                    }
                }
            }
        }
        else if(type == "undirect")
        {
            if(has_weight)
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_weighted_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_weighted_undirect_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_weighted_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::dynamic_weighted_undirect_edge_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_weighted_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_weighted_undirect_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_weighted_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::weighted_undirect_edge_list;
                        }
                    }
                }
            }
            else
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_undirect_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::dynamic_undirect_edge_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::compressed_undirect_edge_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_undirect_edge_list;
                        }
                        else
                        {
                            return graph_type::undirect_edge_list;
                        }
                    }
                }
            }
        }
        else
        {
            return graph_type::unknown;
        }
    }
    else if(format == "adj-list")
    {
        if(type == "direct")
        {
            if(has_weight)
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_weighted_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_weighted_direct_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_weighted_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::dynamic_weighted_direct_adj_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_weighted_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_weighted_direct_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_weighted_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::weighted_direct_adj_list;
                        }
                    }
                }
            }
            else
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_direct_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::dynamic_direct_adj_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_direct_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_direct_adj_list;
                        }
                        else
                        {
                            return graph_type::direct_adj_list;
                        }
                    }
                }
            }
        }
        else if(type == "undirect")
        {
            if(has_weight)
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_weighted_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_weighted_undirect_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_weighted_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::dynamic_weighted_undirect_adj_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_weighted_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_weighted_undirect_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_weighted_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::weighted_undirect_adj_list;
                        }
                    }
                }
            }
            else
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_undirect_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::dynamic_undirect_adj_list;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::compressed_undirect_adj_list;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_undirect_adj_list;
                        }
                        else
                        {
                            return graph_type::undirect_adj_list;
                        }
                    }
                }
            }
        }
        else
        {
            return graph_type::unknown;
        }
    }
    else if(format == "edge-grid")
    {
        if(type == "direct")
        {
            if(has_weight)
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_weighted_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_weighted_direct_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_weighted_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::dynamic_weighted_direct_edge_grid;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_weighted_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_weighted_direct_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_weighted_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::weighted_direct_edge_grid;
                        }
                    }
                }
            }
            else
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_direct_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::dynamic_direct_edge_grid;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_direct_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_direct_edge_grid;
                        }
                        else
                        {
                            return graph_type::direct_edge_grid;
                        }
                    }
                }
            }
        }
        else if(type == "undirect")
        {
            if(has_weight)
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_weighted_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_weighted_undirect_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_weighted_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::dynamic_weighted_undirect_edge_grid;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_weighted_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_weighted_undirect_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_weighted_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::weighted_undirect_edge_grid;
                        }
                    }
                }
            }
            else
            {
                if(is_dynamic)
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_dynamic_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_dynamic_undirect_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_dynamic_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::dynamic_undirect_edge_grid;
                        }
                    }
                }
                else
                {
                    if(is_compressed)
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_compressed_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::compressed_undirect_edge_grid;
                        }
                    }
                    else
                    {
                        if(is_sorted)
                        {
                            return graph_type::sorted_undirect_edge_grid;
                        }
                        else
                        {
                            return graph_type::undirect_edge_grid;
                        }
                    }
                }
            }
        }
        else
        {
            return graph_type::unknown;
        }
    }
    else
    {
        return graph_type::unknown;
    }
}

inline graph_type graph_props_to_graph_type(graph_props p)
{
    return graph_props_to_graph_type(
        p.format, p.type, p.weighted, p.dynamic, p.compressed, p.sorted);
}

inline graph_props graph_type_to_graph_props(graph_type t)
{
    switch(t)
    {
        case graph_type::direct_edge_list:
            return { "edge-list", "direct", false, false, false, false };
        case graph_type::direct_adj_list:
            return { "adj-list", "direct", false, false, false, false };
        case graph_type::direct_edge_grid:
            return { "edge-grid", "direct", false, false, false, false };
        case graph_type::undirect_edge_list:
            return { "edge-list", "undirect", false, false, false, false };
        case graph_type::undirect_adj_list:
            return { "adj-list", "undirect", false, false, false, false };
        case graph_type::undirect_edge_grid:
            return { "edge-grid", "undirect", false, false, false, false };
        case graph_type::weighted_direct_edge_list:
            return { "edge-list", "direct", true, false, false, false };
        case graph_type::weighted_direct_adj_list:
            return { "adj-list", "direct", true, false, false, false };
        case graph_type::weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, false, false, false };
        case graph_type::weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, false, false, false };
        case graph_type::weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, false, false, false };
        case graph_type::weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, false, false, false };
        case graph_type::dynamic_direct_edge_list:
            return { "edge-list", "direct", false, true, false, false };
        case graph_type::dynamic_direct_adj_list:
            return { "adj-list", "direct", false, true, false, false };
        case graph_type::dynamic_direct_edge_grid:
            return { "edge-grid", "direct", false, true, false, false };
        case graph_type::dynamic_undirect_edge_list:
            return { "edge-list", "undirect", false, true, false, false };
        case graph_type::dynamic_undirect_adj_list:
            return { "adj-list", "undirect", false, true, false, false };
        case graph_type::dynamic_undirect_edge_grid:
            return { "edge-grid", "undirect", false, true, false, false };
        case graph_type::dynamic_weighted_direct_edge_list:
            return { "edge-list", "direct", true, true, false, false };
        case graph_type::dynamic_weighted_direct_adj_list:
            return { "adj-list", "direct", true, true, false, false };
        case graph_type::dynamic_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, true, false, false };
        case graph_type::dynamic_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, true, false, false };
        case graph_type::dynamic_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, true, false, false };
        case graph_type::dynamic_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, true, false, false };
        case graph_type::compressed_direct_edge_list:
            return { "edge-list", "direct", false, false, true, false };
        case graph_type::compressed_direct_adj_list:
            return { "adj-list", "direct", false, false, true, false };
        case graph_type::compressed_direct_edge_grid:
            return { "edge-grid", "direct", false, false, true, false };
        case graph_type::compressed_undirect_edge_list:
            return { "edge-list", "undirect", false, false, true, false };
        case graph_type::compressed_undirect_adj_list:
            return { "adj-list", "undirect", false, false, true, false };
        case graph_type::compressed_undirect_edge_grid:
            return { "edge-grid", "undirect", false, false, true, false };
        case graph_type::compressed_weighted_direct_edge_list:
            return { "edge-list", "direct", true, false, true, false };
        case graph_type::compressed_weighted_direct_adj_list:
            return { "adj-list", "direct", true, false, true, false };
        case graph_type::compressed_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, false, true, false };
        case graph_type::compressed_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, false, true, false };
        case graph_type::compressed_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, false, true, false };
        case graph_type::compressed_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, false, true, false };
        case graph_type::compressed_dynamic_direct_edge_list:
            return { "edge-list", "direct", false, true, true, false };
        case graph_type::compressed_dynamic_direct_adj_list:
            return { "adj-list", "direct", false, true, true, false };
        case graph_type::compressed_dynamic_direct_edge_grid:
            return { "edge-grid", "direct", false, true, true, false };
        case graph_type::compressed_dynamic_undirect_edge_list:
            return { "edge-list", "undirect", false, true, true, false };
        case graph_type::compressed_dynamic_undirect_adj_list:
            return { "adj-list", "undirect", false, true, true, false };
        case graph_type::compressed_dynamic_undirect_edge_grid:
            return { "edge-grid", "undirect", false, true, true, false };
        case graph_type::compressed_dynamic_weighted_direct_edge_list:
            return { "edge-list", "direct", true, true, true, false };
        case graph_type::compressed_dynamic_weighted_direct_adj_list:
            return { "adj-list", "direct", true, true, true, false };
        case graph_type::compressed_dynamic_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, true, true, false };
        case graph_type::compressed_dynamic_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, true, true, false };
        case graph_type::compressed_dynamic_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, true, true, false };
        case graph_type::compressed_dynamic_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, true, true, false };
        case graph_type::sorted_direct_edge_list:
            return { "edge-list", "direct", false, false, false, true };
        case graph_type::sorted_direct_adj_list:
            return { "adj-list", "direct", false, false, false, true };
        case graph_type::sorted_direct_edge_grid:
            return { "edge-grid", "direct", false, false, false, true };
        case graph_type::sorted_undirect_edge_list:
            return { "edge-list", "undirect", false, false, false, true };
        case graph_type::sorted_undirect_adj_list:
            return { "adj-list", "undirect", false, false, false, true };
        case graph_type::sorted_undirect_edge_grid:
            return { "edge-grid", "undirect", false, false, false, true };
        case graph_type::sorted_weighted_direct_edge_list:
            return { "edge-list", "direct", true, false, false, true };
        case graph_type::sorted_weighted_direct_adj_list:
            return { "adj-list", "direct", true, false, false, true };
        case graph_type::sorted_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, false, false, true };
        case graph_type::sorted_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, false, false, true };
        case graph_type::sorted_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, false, false, true };
        case graph_type::sorted_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, false, false, true };
        case graph_type::sorted_dynamic_direct_edge_list:
            return { "edge-list", "direct", false, true, false, true };
        case graph_type::sorted_dynamic_direct_adj_list:
            return { "adj-list", "direct", false, true, false, true };
        case graph_type::sorted_dynamic_direct_edge_grid:
            return { "edge-grid", "direct", false, true, false, true };
        case graph_type::sorted_dynamic_undirect_edge_list:
            return { "edge-list", "undirect", false, true, false, true };
        case graph_type::sorted_dynamic_undirect_adj_list:
            return { "adj-list", "undirect", false, true, false, true };
        case graph_type::sorted_dynamic_undirect_edge_grid:
            return { "edge-grid", "undirect", false, true, false, true };
        case graph_type::sorted_dynamic_weighted_direct_edge_list:
            return { "edge-list", "direct", true, true, false, true };
        case graph_type::sorted_dynamic_weighted_direct_adj_list:
            return { "adj-list", "direct", true, true, false, true };
        case graph_type::sorted_dynamic_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, true, false, true };
        case graph_type::sorted_dynamic_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, true, false, true };
        case graph_type::sorted_dynamic_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, true, false, true };
        case graph_type::sorted_dynamic_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, true, false, true };
        case graph_type::sorted_compressed_direct_edge_list:
            return { "edge-list", "direct", false, false, true, true };
        case graph_type::sorted_compressed_direct_adj_list:
            return { "adj-list", "direct", false, false, true, true };
        case graph_type::sorted_compressed_direct_edge_grid:
            return { "edge-grid", "direct", false, false, true, true };
        case graph_type::sorted_compressed_undirect_edge_list:
            return { "edge-list", "undirect", false, false, true, true };
        case graph_type::sorted_compressed_undirect_adj_list:
            return { "adj-list", "undirect", false, false, true, true };
        case graph_type::sorted_compressed_undirect_edge_grid:
            return { "edge-grid", "undirect", false, false, true, true };
        case graph_type::sorted_compressed_weighted_direct_edge_list:
            return { "edge-list", "direct", true, false, true, true };
        case graph_type::sorted_compressed_weighted_direct_adj_list:
            return { "adj-list", "direct", true, false, true, true };
        case graph_type::sorted_compressed_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, false, true, true };
        case graph_type::sorted_compressed_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, false, true, true };
        case graph_type::sorted_compressed_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, false, true, true };
        case graph_type::sorted_compressed_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, false, true, true };
        case graph_type::sorted_compressed_dynamic_direct_edge_list:
            return { "edge-list", "direct", false, true, true, true };
        case graph_type::sorted_compressed_dynamic_direct_adj_list:
            return { "adj-list", "direct", false, true, true, true };
        case graph_type::sorted_compressed_dynamic_direct_edge_grid:
            return { "edge-grid", "direct", false, true, true, true };
        case graph_type::sorted_compressed_dynamic_undirect_edge_list:
            return { "edge-list", "undirect", false, true, true, true };
        case graph_type::sorted_compressed_dynamic_undirect_adj_list:
            return { "adj-list", "undirect", false, true, true, true };
        case graph_type::sorted_compressed_dynamic_undirect_edge_grid:
            return { "edge-grid", "undirect", false, true, true, true };
        case graph_type::sorted_compressed_dynamic_weighted_direct_edge_list:
            return { "edge-list", "direct", true, true, true, true };
        case graph_type::sorted_compressed_dynamic_weighted_direct_adj_list:
            return { "adj-list", "direct", true, true, true, true };
        case graph_type::sorted_compressed_dynamic_weighted_direct_edge_grid:
            return { "edge-grid", "direct", true, true, true, true };
        case graph_type::sorted_compressed_dynamic_weighted_undirect_edge_list:
            return { "edge-list", "undirect", true, true, true, true };
        case graph_type::sorted_compressed_dynamic_weighted_undirect_adj_list:
            return { "adj-list", "undirect", true, true, true, true };
        case graph_type::sorted_compressed_dynamic_weighted_undirect_edge_grid:
            return { "edge-grid", "undirect", true, true, true, true };
        case graph_type::unknown:
            return {};
    }
}


inline std::string graph_type_to_string(graph_type t)
{
    switch(t)
    {
        case graph_type::direct_edge_list:
            return "direct_edge_list";
        case graph_type::direct_adj_list:
            return "direct_adj_list";
        case graph_type::direct_edge_grid:
            return "direct_edge_grid";
        case graph_type::undirect_edge_list:
            return "undirect_edge_list";
        case graph_type::undirect_adj_list:
            return "undirect_adj_list";
        case graph_type::undirect_edge_grid:
            return "undirect_edge_grid";
        case graph_type::weighted_direct_edge_list:
            return "weighted_direct_edge_list";
        case graph_type::weighted_direct_adj_list:
            return "weighted_direct_adj_list";
        case graph_type::weighted_direct_edge_grid:
            return "weighted_direct_edge_grid";
        case graph_type::weighted_undirect_edge_list:
            return "weighted_undirect_edge_list";
        case graph_type::weighted_undirect_adj_list:
            return "weighted_undirect_adj_list";
        case graph_type::weighted_undirect_edge_grid:
            return "weighted_undirect_edge_grid";
        case graph_type::dynamic_direct_edge_list:
            return "dynamic_direct_edge_list";
        case graph_type::dynamic_direct_adj_list:
            return "dynamic_direct_adj_list";
        case graph_type::dynamic_direct_edge_grid:
            return "dynamic_direct_edge_grid";
        case graph_type::dynamic_undirect_edge_list:
            return "dynamic_undirect_edge_list";
        case graph_type::dynamic_undirect_adj_list:
            return "dynamic_undirect_adj_list";
        case graph_type::dynamic_undirect_edge_grid:
            return "dynamic_undirect_edge_grid";
        case graph_type::dynamic_weighted_direct_edge_list:
            return "dynamic_weighted_direct_edge_list";
        case graph_type::dynamic_weighted_direct_adj_list:
            return "dynamic_weighted_direct_adj_list";
        case graph_type::dynamic_weighted_direct_edge_grid:
            return "dynamic_weighted_direct_edge_grid";
        case graph_type::dynamic_weighted_undirect_edge_list:
            return "dynamic_weighted_undirect_edge_list";
        case graph_type::dynamic_weighted_undirect_adj_list:
            return "dynamic_weighted_undirect_adj_list";
        case graph_type::dynamic_weighted_undirect_edge_grid:
            return "dynamic_weighted_undirect_edge_grid";
        case graph_type::compressed_direct_edge_list:
            return "compressed_direct_edge_list";
        case graph_type::compressed_direct_adj_list:
            return "compressed_direct_adj_list";
        case graph_type::compressed_direct_edge_grid:
            return "compressed_direct_edge_grid";
        case graph_type::compressed_undirect_edge_list:
            return "compressed_undirect_edge_list";
        case graph_type::compressed_undirect_adj_list:
            return "compressed_undirect_adj_list";
        case graph_type::compressed_undirect_edge_grid:
            return "compressed_undirect_edge_grid";
        case graph_type::compressed_weighted_direct_edge_list:
            return "compressed_weighted_direct_edge_list";
        case graph_type::compressed_weighted_direct_adj_list:
            return "compressed_weighted_direct_adj_list";
        case graph_type::compressed_weighted_direct_edge_grid:
            return "compressed_weighted_direct_edge_grid";
        case graph_type::compressed_weighted_undirect_edge_list:
            return "compressed_weighted_undirect_edge_list";
        case graph_type::compressed_weighted_undirect_adj_list:
            return "compressed_weighted_undirect_adj_list";
        case graph_type::compressed_weighted_undirect_edge_grid:
            return "compressed_weighted_undirect_edge_grid";
        case graph_type::compressed_dynamic_direct_edge_list:
            return "compressed_dynamic_direct_edge_list";
        case graph_type::compressed_dynamic_direct_adj_list:
            return "compressed_dynamic_direct_adj_list";
        case graph_type::compressed_dynamic_direct_edge_grid:
            return "compressed_dynamic_direct_edge_grid";
        case graph_type::compressed_dynamic_undirect_edge_list:
            return "compressed_dynamic_undirect_edge_list";
        case graph_type::compressed_dynamic_undirect_adj_list:
            return "compressed_dynamic_undirect_adj_list";
        case graph_type::compressed_dynamic_undirect_edge_grid:
            return "compressed_dynamic_undirect_edge_grid";
        case graph_type::compressed_dynamic_weighted_direct_edge_list:
            return "compressed_dynamic_weighted_direct_edge_list";
        case graph_type::compressed_dynamic_weighted_direct_adj_list:
            return "compressed_dynamic_weighted_direct_adj_list";
        case graph_type::compressed_dynamic_weighted_direct_edge_grid:
            return "compressed_dynamic_weighted_direct_edge_grid";
        case graph_type::compressed_dynamic_weighted_undirect_edge_list:
            return "compressed_dynamic_weighted_undirect_edge_list";
        case graph_type::compressed_dynamic_weighted_undirect_adj_list:
            return "compressed_dynamic_weighted_undirect_adj_list";
        case graph_type::compressed_dynamic_weighted_undirect_edge_grid:
            return "compressed_dynamic_weighted_undirect_edge_grid";
        case graph_type::sorted_direct_edge_list:
            return "sorted_direct_edge_list";
        case graph_type::sorted_direct_adj_list:
            return "sorted_direct_adj_list";
        case graph_type::sorted_direct_edge_grid:
            return "sorted_direct_edge_grid";
        case graph_type::sorted_undirect_edge_list:
            return "sorted_undirect_edge_list";
        case graph_type::sorted_undirect_adj_list:
            return "sorted_undirect_adj_list";
        case graph_type::sorted_undirect_edge_grid:
            return "sorted_undirect_edge_grid";
        case graph_type::sorted_weighted_direct_edge_list:
            return "sorted_weighted_direct_edge_list";
        case graph_type::sorted_weighted_direct_adj_list:
            return "sorted_weighted_direct_adj_list";
        case graph_type::sorted_weighted_direct_edge_grid:
            return "sorted_weighted_direct_edge_grid";
        case graph_type::sorted_weighted_undirect_edge_list:
            return "sorted_weighted_undirect_edge_list";
        case graph_type::sorted_weighted_undirect_adj_list:
            return "sorted_weighted_undirect_adj_list";
        case graph_type::sorted_weighted_undirect_edge_grid:
            return "sorted_weighted_undirect_edge_grid";
        case graph_type::sorted_dynamic_direct_edge_list:
            return "sorted_dynamic_direct_edge_list";
        case graph_type::sorted_dynamic_direct_adj_list:
            return "sorted_dynamic_direct_adj_list";
        case graph_type::sorted_dynamic_direct_edge_grid:
            return "sorted_dynamic_direct_edge_grid";
        case graph_type::sorted_dynamic_undirect_edge_list:
            return "sorted_dynamic_undirect_edge_list";
        case graph_type::sorted_dynamic_undirect_adj_list:
            return "sorted_dynamic_undirect_adj_list";
        case graph_type::sorted_dynamic_undirect_edge_grid:
            return "sorted_dynamic_undirect_edge_grid";
        case graph_type::sorted_dynamic_weighted_direct_edge_list:
            return "sorted_dynamic_weighted_direct_edge_list";
        case graph_type::sorted_dynamic_weighted_direct_adj_list:
            return "sorted_dynamic_weighted_direct_adj_list";
        case graph_type::sorted_dynamic_weighted_direct_edge_grid:
            return "sorted_dynamic_weighted_direct_edge_grid";
        case graph_type::sorted_dynamic_weighted_undirect_edge_list:
            return "sorted_dynamic_weighted_undirect_edge_list";
        case graph_type::sorted_dynamic_weighted_undirect_adj_list:
            return "sorted_dynamic_weighted_undirect_adj_list";
        case graph_type::sorted_dynamic_weighted_undirect_edge_grid:
            return "sorted_dynamic_weighted_undirect_edge_grid";
        case graph_type::sorted_compressed_direct_edge_list:
            return "sorted_compressed_direct_edge_list";
        case graph_type::sorted_compressed_direct_adj_list:
            return "sorted_compressed_direct_adj_list";
        case graph_type::sorted_compressed_direct_edge_grid:
            return "sorted_compressed_direct_edge_grid";
        case graph_type::sorted_compressed_undirect_edge_list:
            return "sorted_compressed_undirect_edge_list";
        case graph_type::sorted_compressed_undirect_adj_list:
            return "sorted_compressed_undirect_adj_list";
        case graph_type::sorted_compressed_undirect_edge_grid:
            return "sorted_compressed_undirect_edge_grid";
        case graph_type::sorted_compressed_weighted_direct_edge_list:
            return "sorted_compressed_weighted_direct_edge_list";
        case graph_type::sorted_compressed_weighted_direct_adj_list:
            return "sorted_compressed_weighted_direct_adj_list";
        case graph_type::sorted_compressed_weighted_direct_edge_grid:
            return "sorted_compressed_weighted_direct_edge_grid";
        case graph_type::sorted_compressed_weighted_undirect_edge_list:
            return "sorted_compressed_weighted_undirect_edge_list";
        case graph_type::sorted_compressed_weighted_undirect_adj_list:
            return "sorted_compressed_weighted_undirect_adj_list";
        case graph_type::sorted_compressed_weighted_undirect_edge_grid:
            return "sorted_compressed_weighted_undirect_edge_grid";
        case graph_type::sorted_compressed_dynamic_direct_edge_list:
            return "sorted_compressed_dynamic_direct_edge_list";
        case graph_type::sorted_compressed_dynamic_direct_edge_grid:
            return "sorted_compressed_dynamic_direct_edge_grid";
        case graph_type::sorted_compressed_dynamic_undirect_edge_list:
            return "sorted_compressed_dynamic_undirect_edge_list";
        case graph_type::sorted_compressed_dynamic_undirect_adj_list:
            return "sorted_compressed_dynamic_undirect_adj_list";
        case graph_type::sorted_compressed_dynamic_undirect_edge_grid:
            return "sorted_compressed_dynamic_undirect_edge_grid";
        case graph_type::sorted_compressed_dynamic_weighted_direct_edge_list:
            return "sorted_compressed_dynamic_weighted_direct_edge_list";
        case graph_type::sorted_compressed_dynamic_weighted_direct_adj_list:
            return "sorted_compressed_dynamic_weighted_direct_adj_list";
        case graph_type::sorted_compressed_dynamic_weighted_direct_edge_grid:
            return "sorted_compressed_dynamic_weighted_direct_edge_grid";
        case graph_type::sorted_compressed_dynamic_weighted_undirect_edge_list:
            return "sorted_compressed_dynamic_weighted_undirect_edge_list";
        case graph_type::sorted_compressed_dynamic_weighted_undirect_adj_list:
            return "sorted_compressed_dynamic_weighted_undirect_adj_list";
        case graph_type::sorted_compressed_dynamic_weighted_undirect_edge_grid:
            return "sorted_compressed_dynamic_weighted_undirect_edge_grid";
        case graph_type::sorted_compressed_dynamic_direct_adj_list:
            return "sorted_compressed_dynamic_direct_edge_grid";
        case graph_type::unknown:
            return "unknown";
    }
}

} /* stg */
