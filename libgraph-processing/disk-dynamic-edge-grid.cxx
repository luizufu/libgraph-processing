#include <libgraph-processing/disk-dynamic-edge-grid.hxx>
#include <libgraph-processing/utility.hxx>
#include <iostream>
#include <cassert>

#define CALL(f, ...) \
    _props.sorted && _props.compressed ?  \
        zmultimap_t::f(__VA_ARGS__) : \
    _props.sorted && !_props.compressed ? \
        multimap_t::f(__VA_ARGS__) : \
    !_props.sorted && _props.compressed ? \
        zumultimap_t::f(__VA_ARGS__) : \
        umultimap_t::f(__VA_ARGS__)


namespace stg
{

static uint32_t get_grid_id(uint32_t u, uint32_t v,
                            uint32_t max, uint32_t grid_size);


template <typename Edge, typename Neighbor>
disk_dynamic_edge_grid<Edge, Neighbor>::disk_dynamic_edge_grid(const std::string& in, graph_type type, uint32_t grid_size, uint32_t max_vertex_id)
    : _allocator(in.c_str(), _super_block.free_list)
{

    if(_allocator.get().empty())
    {
        _super_block.header.type = type;
        _super_block.header.n_vertices = 0;
        _super_block.header.n_edges = 0;
        _super_block.header.grid_size = grid_size;
        _super_block.header.max = max_vertex_id;

        _props = graph_type_to_graph_props(
                _super_block.header.type);
        assert(_props.format == "edge-grid" && _props.dynamic);

        _super_block.key_list1 = CALL(create, _allocator);

        _super_block.free_list = 0;
        _allocator.get().push_back(to_bytes(_super_block));
    }
    else
    {
        _allocator.get().read(0, to_bytes(_super_block));

        assert(_super_block.header.magic == EDGE_GRID_MAGIC);

        _props = graph_type_to_graph_props(
                _super_block.header.type);
        assert(_props.format == "edge-grid" && _props.dynamic);
    }

    _root = _super_block.key_list1;
}

template <typename Edge, typename Neighbor>
disk_dynamic_edge_grid<Edge, Neighbor>::~disk_dynamic_edge_grid()
{
    _allocator.get().write(0, to_bytes(_super_block));
}


template <typename Edge, typename Neighbor>
void disk_dynamic_edge_grid<Edge, Neighbor>::debug() const
{
    std::cout << "SUPER BLOCK" << '\n';
    std::cout << "magic: " << _super_block.header.magic << '\n';
    std::cout << "type: " << graph_type_to_string(_super_block.header.type) << '\n';
    std::cout << "n_vertices: " << _super_block.header.n_vertices << '\n';
    std::cout << "n_edges: " << _super_block.header.n_edges << '\n';
    std::cout << "grid_size: " << _super_block.header.grid_size << '\n';
    std::cout << "max: " << _super_block.header.max << '\n';
    std::cout << "key_list1: " << _super_block.key_list1 << '\n';
    std::cout << "free_list: " << _super_block.free_list << "\n\n";

    CALL(debug, _allocator, _root);
}


template <typename Edge, typename Neighbor>
void disk_dynamic_edge_grid<Edge, Neighbor>::insert_edge(Edge e)
{
    uint32_t key = get_grid_id(e.u, e.v,
        _super_block.header.max,
        _super_block.header.grid_size);

    if(CALL(insert, _allocator, _root, key, e))
    {
        uint32_t max = std::max(e.u, e.v);
        max = std::max(max + 1, _super_block.header.n_vertices);
        _super_block.header.n_vertices = max;
        ++_super_block.header.n_edges;
    }
}


template <typename Edge, typename Neighbor>
void disk_dynamic_edge_grid<Edge, Neighbor>::remove_edge(Edge e)
{
    uint32_t key = get_grid_id(e.u, e.v,
        _super_block.header.max,
        _super_block.header.grid_size);

    if(CALL(remove, _allocator, _root, key, e))
        --_super_block.header.n_edges;
}

template <typename Edge, typename Neighbor>
void disk_dynamic_edge_grid<Edge, Neighbor>::remove_out_neighbours(uint32_t u)
{
    uint32_t lid = get_grid_id(u, 0,
        _super_block.header.max,
        _super_block.header.grid_size);

    uint32_t rid = get_grid_id(u, _super_block.header.max,
        _super_block.header.max,
        _super_block.header.grid_size);

    auto predicate = [u] (const Edge& e) { return e.u == u ;};

    for(size_t key = lid; key <= rid; ++key)
    {
        auto removed = CALL(remove_if, _allocator, _root, key, predicate);
        _super_block.header.n_edges -= removed.size();
    }
}

template <typename Edge, typename Neighbor>
void disk_dynamic_edge_grid<Edge, Neighbor>::remove_in_neighbours(uint32_t v)
{
    uint32_t lid = get_grid_id(0, v,
        _super_block.header.max,
        _super_block.header.grid_size);

    uint32_t rid = get_grid_id(_super_block.header.max, v,
        _super_block.header.max,
        _super_block.header.grid_size);

    auto predicate = [v] (const Edge& e) { return e.v == v ;};

    for(size_t key = lid; key <= rid; ++key)
    {
        auto removed = CALL(remove_if, _allocator, _root, key,predicate);
        _super_block.header.n_edges -= removed.size();
    }
}


template <typename Edge, typename Neighbor>
bool disk_dynamic_edge_grid<Edge, Neighbor>::has_edge(uint32_t u, uint32_t v) const
{
    return CALL(exists, _allocator, _root, u, { v });
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_dynamic_edge_grid<Edge, Neighbor>::out_neighbours(uint32_t u) const
{
    size_t lid = get_grid_id(u, 0,
            _super_block.header.max,
            _super_block.header.grid_size);

    size_t rid = get_grid_id(u, _super_block.header.max,
            _super_block.header.max,
            _super_block.header.grid_size);

    for(size_t key = lid; key <= rid; ++key)
        for(auto e : CALL(find, _allocator, _root, key))
            if(e.u == u)
                co_yield edge_to_out_neighbor(e);
}

// TODO which is faster, row-wise or column-wise? do benchmark
template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_dynamic_edge_grid<Edge, Neighbor>::in_neighbours(uint32_t v) const
{
    size_t lid =get_grid_id(0, v,
            _super_block.header.max,
            _super_block.header.grid_size);

    size_t rid =get_grid_id(_super_block.header.max, v,
            _super_block.header.max,
            _super_block.header.grid_size);

    for(size_t key = lid; key <= rid; ++key)
        for(auto e : CALL(find, _allocator, _root, key))
            if(e.v == v)
                co_yield edge_to_in_neighbor(e);
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Edge> disk_dynamic_edge_grid<Edge, Neighbor>::edges() const
{
    for(auto [id, e] : CALL(all, _allocator, _root))
        co_yield e;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_edge_grid<Edge, Neighbor>::out_degree(uint32_t u) const
{
    size_t count = 0;
    for(auto v : out_neighbours(u))
        ++count;

    return count;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_edge_grid<Edge, Neighbor>::in_degree(uint32_t u) const
{
    size_t count = 0;
    for(auto v : in_neighbours(u))
        ++count;

    return count;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_edge_grid<Edge, Neighbor>::n_vertices() const
{
    return _super_block.header.n_vertices;
}

template <typename Edge, typename Neighbor>
size_t disk_dynamic_edge_grid<Edge, Neighbor>::n_edges() const
{
    return _super_block.header.n_edges;
}

template <typename Edge, typename Neighbor>
const graph_props& disk_dynamic_edge_grid<Edge, Neighbor>::props() const
{
    return _props;
}


uint32_t get_grid_id(uint32_t u, uint32_t v, uint32_t max, uint32_t grid_size)
{
    uint32_t piece = (max + 1) / grid_size;
    uint32_t row = u / piece;
    uint32_t column = v / piece;

    return row * grid_size + column;
}


INSTATIATE_GRAPH_TEMPLATES(disk_dynamic_edge_grid)

} /* stg */

#undef CALL
