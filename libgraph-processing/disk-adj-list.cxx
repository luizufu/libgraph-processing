#include <libgraph-processing/disk-adj-list.hxx>
#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/traverse.hxx>
#include <filesystem>
#include <cassert>
#include <unistd.h>
#include <fcntl.h>

namespace stg
{

template <typename Edge, typename Neighbor>
disk_adj_list<Edge, Neighbor>::disk_adj_list(const std::string& in)
{
    assert(std::filesystem::exists(in));

    _fd = open(in.c_str(), O_RDONLY);
    uint32_t magic;
    read(_fd, reinterpret_cast<char*>(&magic), sizeof(magic));
    assert(magic == ADJ_LIST_MAGIC);

    read(_fd, reinterpret_cast<char*>(&_header), sizeof(_header));
    assert(_header.direction != UNKNOWN);
    _props = graph_type_to_graph_props(_header.type);
    assert(_props.format == "adj-list" && !_props.dynamic);
}

template <typename Edge, typename Neighbor>
disk_adj_list<Edge, Neighbor>::~disk_adj_list()
{
    close(_fd);
}

template <typename Edge, typename Neighbor>
bool disk_adj_list<Edge, Neighbor>::has_edge(uint32_t u, uint32_t v) const
{
    if(_header.direction == OUTGOING || _header.direction == OUTGOING_INCOMING)
    {
        for(Neighbor n : out_neighbours(u))
        {
            if(_props.sorted && n.v > v)
                break;

            if(n.v == v)
                return true;
        }
    }
    else if(_header.direction == INCOMING)
    {
        for(Neighbor n : in_neighbours(v))
        {
            if(_props.sorted && n.v > u)
                break;

            if(n.v == u)
                return true;
        }
    }

    return false;
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_adj_list<Edge, Neighbor>::out_neighbours(uint32_t u) const
{
    if(_header.direction == OUTGOING || _header.direction == OUTGOING_INCOMING)
    {
        auto [pos_begin, pos_end] = adj_range(u, OUTGOING);

        if(pos_begin != pos_end)
        {
            auto gen =  detail::traverse<Neighbor>(
                _fd, pos_begin, pos_end, _props);

            for(Neighbor n : gen)
                co_yield n;
        }
    }
    else if(_header.direction == INCOMING)
    {
        for(Edge e : edges())
            if(e.u == u)
                co_yield edge_to_out_neighbor(e);
    }
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_adj_list<Edge, Neighbor>::in_neighbours(uint32_t v) const
{
    if(_header.direction == INCOMING || _header.direction == OUTGOING_INCOMING)
    {
        auto [pos_begin, pos_end] = adj_range(v, INCOMING);

        if(pos_begin != pos_end)
        {
            auto gen =  detail::traverse<Neighbor>(
                _fd, pos_begin, pos_end, _props);

            for(Neighbor n : gen)
                co_yield n;
        }
    }
    else if(_header.direction == OUTGOING)
    {
        for(Edge e : edges())
            if(e.v == v)
                co_yield edge_to_in_neighbor(e);
    }
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Edge> disk_adj_list<Edge, Neighbor>::edges() const
{
    if(_header.direction == OUTGOING || _header.direction == OUTGOING_INCOMING)
    {
        auto begin = sizeof(ADJ_LIST_MAGIC) + sizeof(_header);
        auto end = begin + (_header.n_vertices * sizeof(off_t));
        detail::source<off_t> s(_fd, begin, end);

        off_t pos_begin = sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + 2 * _header.n_vertices * sizeof(off_t);
        off_t pos_end = -1;
        uint32_t u = 0;

        while(s.read(pos_end))
        {
            if(pos_end != -1)
            {
                auto gen = detail::traverse<Neighbor>(
                    _fd, pos_begin, pos_end, _props);

                for(Neighbor n : gen)
                    co_yield make_edge(u, n);

                pos_begin = pos_end;
            }

            ++u;
        }
    }
    else if(_header.direction == INCOMING)
    {
        auto begin = sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + (_header.n_vertices * sizeof(off_t));
        auto end = begin + (_header.n_vertices * sizeof(off_t));
        detail::source<off_t> s(_fd, begin, end);

        off_t pos_begin = sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + 2 * _header.n_vertices * sizeof(off_t);

        off_t pos_last_out;
        for(size_t i = _header.n_vertices; i > 0; --i)
        {
            lseek(_fd,
                sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + (i * sizeof(off_t)), SEEK_SET);
             read(_fd,
                reinterpret_cast<char*>(&pos_last_out), sizeof(pos_last_out));

             if(pos_last_out != -1)
             {
                 pos_begin = pos_last_out;
                 break;
             }
        }

        off_t pos_end = -1;
        uint32_t v = 0;

        while(s.read(pos_end))
        {
            if(pos_end != -1)
            {
                auto gen = detail::traverse<Neighbor>(
                    _fd, pos_begin, pos_end, _props);

                for(Neighbor n : gen)
                    co_yield make_edge(n, v);

                pos_begin = pos_end;
            }

            ++v;
        }
    }
}

template <typename Edge, typename Neighbor>
size_t disk_adj_list<Edge, Neighbor>::out_degree(uint32_t u) const
{
    if(_header.direction == OUTGOING || _header.direction == OUTGOING_INCOMING)
    {
        auto [pos_begin, pos_end] = adj_range(u, OUTGOING);

        if(pos_begin != pos_end)
        {
            if(!_props.compressed)
                return (pos_end - pos_begin) / sizeof(Neighbor);

            auto gen = detail::traverse<Neighbor>(
                _fd, pos_begin, pos_end, _props);

            size_t degree = 0;
            for(Neighbor n : gen)
                ++degree;

            return degree;
        }
    }
    else if(_header.direction == INCOMING)
    {
        size_t degree = 0;
        for(auto v : out_neighbours(u))
            ++degree;

        return degree;
    }

    return 0;
}

template <typename Edge, typename Neighbor>
size_t disk_adj_list<Edge, Neighbor>::in_degree(uint32_t v) const
{
    if(_header.direction == INCOMING || _header.direction == OUTGOING_INCOMING)
    {
        auto [pos_begin, pos_end] = adj_range(v, INCOMING);

        if(pos_begin != pos_end)
        {
            if(!_props.compressed)
                return (pos_end - pos_begin) / sizeof(Neighbor);

            auto gen = detail::traverse<Neighbor>(
                _fd, pos_begin, pos_end, _props);

            size_t degree = 0;
            for(Neighbor n : gen)
                ++degree;

            return degree;
        }

    }
    else if(_header.direction == OUTGOING)
    {
        size_t degree = 0;
        for(auto u : in_neighbours(v))
            ++degree;

        return degree;
    }

    return 0;
}

template <typename Edge, typename Neighbor>
size_t disk_adj_list<Edge, Neighbor>::n_vertices() const
{
    return _header.n_vertices;
}

template <typename Edge, typename Neighbor>
size_t disk_adj_list<Edge, Neighbor>::n_edges() const
{
    return _header.n_edges;
}

template <typename Edge, typename Neighbor>
const graph_props& disk_adj_list<Edge, Neighbor>::props() const
{
    return _props;
}

template <typename Edge, typename Neighbor>
auto disk_adj_list<Edge, Neighbor>::adj_range(size_t u, direction_t direction) const
    -> std::pair<off_t, off_t>
{
    off_t pos_begin = sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + 2 * _header.n_vertices * sizeof(off_t);

    if(direction == INCOMING)
    {
        off_t pos_last_out;

        for(size_t i = _header.n_vertices; i > 0; --i)
        {
            lseek(_fd,
                sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + (i * sizeof(off_t)), SEEK_SET);
             read(_fd,
                reinterpret_cast<char*>(&pos_last_out), sizeof(pos_last_out));

             if(pos_last_out != -1)
             {
                 pos_begin = pos_last_out;
                 break;
             }
        }
    }

    off_t pos_end = -1;

    off_t begin = direction == OUTGOING ?
        sizeof(ADJ_LIST_MAGIC) + sizeof(_header) : sizeof(ADJ_LIST_MAGIC) + sizeof(_header) + (_header.n_vertices * sizeof(off_t));


    lseek(_fd, begin + (u * sizeof(off_t)), SEEK_SET);
    read(_fd, reinterpret_cast<char*>(&pos_end), sizeof(pos_end));

    if(pos_end == -1)
        return { -1, -1 };

    if(u > 0)
    {
        for(; u > 0; --u)
        {
            off_t current = begin + ((u - 1) * sizeof(pos_begin));
            off_t pos_cur;
            lseek(_fd, current, SEEK_SET);
            read(_fd,
                reinterpret_cast<char*>(&pos_cur), sizeof(pos_cur));

            if(pos_cur != -1)
            {
                pos_begin = pos_cur;
                break;
            }
        }
    }

    return { pos_begin, pos_end };
}

INSTATIATE_GRAPH_TEMPLATES(disk_adj_list)

} /* stg */
