#pragma once

#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/utility.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/macro/graph.hxx>
#include <cppcoro/generator.hpp>
#include <cstdint>
#include <ios>

namespace stg
{

template <typename Edge, typename Neighbor = neighbor_from_t<Edge>>
class disk_adj_list
{
    int _fd;
    adj_list_header _header;
    graph_props _props;

    auto adj_range(size_t u, direction_t direction) const
        -> std::pair<off_t, off_t>;

public:

    disk_adj_list(const std::string& in);
    ~disk_adj_list();

    bool has_edge(uint32_t u, uint32_t v) const;

    cppcoro::generator<Neighbor> out_neighbours(uint32_t u) const;
    cppcoro::generator<Neighbor> in_neighbours(uint32_t v) const;
    cppcoro::generator<Edge> edges() const;

    size_t out_degree(uint32_t u) const;
    size_t in_degree(uint32_t v) const;

    size_t n_vertices() const;
    size_t n_edges() const;

    const graph_props& props() const;
};

} /* stg */

INSTATIATE_GRAPH_TEMPLATES_EXTERN(disk_adj_list)
