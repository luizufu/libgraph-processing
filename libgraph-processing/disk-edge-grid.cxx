#include <libgraph-processing/disk-edge-grid.hxx>
#include <libgraph-processing/detail/static/source.hxx>
#include <libgraph-processing/detail/static/traverse.hxx>
#include <filesystem>
#include <cassert>
#include <unistd.h>
#include <fcntl.h>


namespace stg
{

static uint32_t get_grid_id(uint32_t u, uint32_t v,
                          uint32_t max, uint32_t grid_size);


template <typename Edge, typename Neighbor>
disk_edge_grid<Edge, Neighbor>::disk_edge_grid(const std::string& in)
{
    assert(std::filesystem::exists(in));

    _fd = open(in.c_str(), O_RDONLY);

    uint32_t magic;
    read(_fd, reinterpret_cast<char*>(&magic), sizeof(magic));
    assert(magic == EDGE_GRID_MAGIC);

    read(_fd, reinterpret_cast<char*>(&_header), sizeof(_header));
    _props = graph_type_to_graph_props(_header.type);
    assert(_props.format == "edge-grid" && !_props.dynamic);
}

template <typename Edge, typename Neighbor>
disk_edge_grid<Edge, Neighbor>::~disk_edge_grid()
{
    close(_fd);
}

template <typename Edge, typename Neighbor>
bool disk_edge_grid<Edge, Neighbor>::has_edge(uint32_t u, uint32_t v) const
{
    for(Neighbor n : out_neighbours(u))
    {
        if(_props.sorted && n.v > v)
            break;

        if(n.v == v)
            return true;
    }

    return false;
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_edge_grid<Edge, Neighbor>::out_neighbours(uint32_t u) const
{
    uint32_t max = _header.n_vertices - 1;
    uint32_t lid = get_grid_id(u, 0, max, _header.grid_size);
    uint32_t rid = get_grid_id(u, max, max, _header.grid_size);

    for(uint32_t id = lid; id <= rid; ++id)
    {
        auto [pos_begin, pos_end] = grid_range(id);

        if(pos_begin != pos_end)
        {
            auto gen =  detail::traverse<Edge>(
                _fd, pos_begin, pos_end, _props,
                [u] (Edge e) { return e.u == u; },
                [] (Edge e) { return edge_to_out_neighbor(e); }
            );

            for(Neighbor n : gen)
                co_yield n;
        }
    }
}

// TODO which is faster, row-wise or column-wise? do benchmark
template <typename Edge, typename Neighbor>
cppcoro::generator<Neighbor> disk_edge_grid<Edge, Neighbor>::in_neighbours(uint32_t v) const
{
    uint32_t max = _header.n_vertices - 1;
    uint32_t lid = get_grid_id(0, v, max, _header.grid_size);
    uint32_t rid = get_grid_id(max, v, max, _header.grid_size);

    for(uint32_t id = lid; id <= rid; id += _header.grid_size)
    {
        auto [pos_begin, pos_end] = grid_range(id);

        if(pos_begin != pos_end)
        {
            auto gen =  detail::traverse<Edge>(
                _fd, pos_begin, pos_end, _props,
                [v] (Edge e) { return e.v == v; },
                [ ] (Edge e) { return edge_to_in_neighbor(e); }
            );

            for(Neighbor n : gen)
                co_yield n;
        }
    }
}

template <typename Edge, typename Neighbor>
cppcoro::generator<Edge> disk_edge_grid<Edge, Neighbor>::edges() const
{
    auto start = sizeof(EDGE_GRID_MAGIC) + sizeof(_header);
    auto end = start + (_header.grid_size * _header.grid_size * sizeof(off_t));
    detail::source<off_t> s(_fd, start, end);

    off_t pos_begin = sizeof(EDGE_GRID_MAGIC) + sizeof(_header) + _header.grid_size * _header.grid_size * sizeof(off_t);
    off_t pos_end = -1;

    while(s.read(pos_end))
    {
        if(pos_end == -1)
            continue;

        for(Edge e : detail::traverse<Edge>(_fd, pos_begin, pos_end, _props))
            co_yield e;

        pos_begin = pos_end;
    }
}

template <typename Edge, typename Neighbor>
size_t disk_edge_grid<Edge, Neighbor>::out_degree(uint32_t u) const
{
    size_t count = 0;
    for(auto v : out_neighbours(u))
        ++count;

    return count;
}

template <typename Edge, typename Neighbor>
size_t disk_edge_grid<Edge, Neighbor>::in_degree(uint32_t u) const
{
    size_t count = 0;
    for(auto v : in_neighbours(u))
        ++count;

    return count;
}

template <typename Edge, typename Neighbor>
size_t disk_edge_grid<Edge, Neighbor>::n_vertices() const
{
    return _header.n_vertices;
}

template <typename Edge, typename Neighbor>
size_t disk_edge_grid<Edge, Neighbor>::n_edges() const
{
    return _header.n_edges;
}

template <typename Edge, typename Neighbor>
const graph_props& disk_edge_grid<Edge, Neighbor>::props() const
{
    return _props;
}

template <typename Edge, typename Neighbor>
auto disk_edge_grid<Edge, Neighbor>::grid_range(uint32_t id) const
    -> std::pair<off_t, off_t>
{
    off_t pos_begin = sizeof(EDGE_GRID_MAGIC) + sizeof(_header) + _header.grid_size * _header.grid_size * sizeof(off_t);
    off_t pos_end = -1;

    off_t begin = sizeof(EDGE_GRID_MAGIC) + sizeof(_header);

    lseek(_fd, begin + (id * sizeof(pos_end)), SEEK_SET);
    read(_fd, reinterpret_cast<char*>(&pos_end), sizeof(pos_end));

    if(pos_end != -1)
        return { -1, -1 };

    if(id > 0)
    {
        for(; id > 0; --id)
        {
            off_t current = begin + ((id - 1) * sizeof(pos_begin));
            off_t pos_cur;
            lseek(_fd, current, SEEK_SET);
            read(_fd, reinterpret_cast<char*>(&pos_cur), sizeof(pos_cur));

            if(pos_cur != -1)
            {
                pos_begin = pos_cur;
                break;
            }
        }
    }

    return { pos_begin, pos_end };
}


uint32_t get_grid_id(uint32_t u, uint32_t v, uint32_t max, uint32_t grid_size)
{
    uint32_t piece = (max + 1) / grid_size;
    uint32_t row = u / piece;
    uint32_t column = v / piece;

    return row * grid_size + column;
}

INSTATIATE_GRAPH_TEMPLATES(disk_edge_grid)

} /* stg */

