#include <tests/basics/data.hxx>

#include <cstdint>
#include <random>

static std::vector<uint32_t> init_numbers();
std::vector<stg::edge> init_edges();
std::vector<stg::neighbor> init_neighbors();


static std::vector<uint32_t> numbers =  init_numbers();

std::vector<stg::edge> edges = init_edges();
std::vector<stg::neighbor> neighbors = init_neighbors();

std::vector<uint32_t> init_numbers()
{
    std::random_device rng;
    std::mt19937 urng(rng());
    urng.seed(2019);

    std::vector<uint32_t> numbers(1000000);
    std::iota(numbers.begin(), numbers.end(), 0);
    std::shuffle(numbers.begin(), numbers.end(), urng);

    /* std::vector< */
    /* std::uniform_int_distribution<uint32_t> dist(1, 100000000); */

    /* for(size_t i = 0; i < 1000000; ++i) */
    /*     numbers.push_back(dist(urng)); */

    return numbers;
}

std::vector<stg::edge> init_edges()
{
    std::vector<stg::edge> edges;
    edges.reserve(numbers.size() / 2);

    for(size_t i = 0; i < numbers.size() - 1; i += 2)
    {
        stg::edge e;
        e.u = numbers[i];
        e.v = numbers[i + 1];
        edges.push_back(e);
    }

    return edges;
}

std::vector<stg::neighbor> init_neighbors()
{
    std::vector<stg::neighbor> neighbors;
    neighbors.reserve(numbers.size());

    for(size_t i = 0; i < numbers.size() - 1; i += 2)
    {
        stg::neighbor n;
        n.v = numbers[i];
        neighbors.push_back(n);
    }

    return neighbors;
}
