#include <libgraph-processing/edge.hxx>
#include <libgraph-processing/headers.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-allocator.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-list.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-list.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-btreeset.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreeset.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-btreemap.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-btreemap.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-unordered-multimap.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-unordered-multimap.hxx>
#include <libgraph-processing/detail/dynamic/disk-block-multimap.hxx>
#include <libgraph-processing/detail/dynamic/zlib-disk-block-multimap.hxx>
#include <tests/basics/data.hxx>
#include <iostream>
#include <cstdio>
#include <vector>
#include <map>
#include <cassert>
#include <filesystem>

#include <iostream>
#include <algorithm>
#include <random>

using namespace stg::detail;
using namespace stg;

uint32_t free_list = 0;

#define massert(Expr, Msg) massert_impl(#Expr, Expr, __FILE__, __LINE__, Msg)

void massert_impl(const char* expr_str, bool expr, const char* file, int line, const char* msg)
{
    if (!expr)
    {
        std::cerr << "Assert failed:\t" << msg << "\n"
            << "Expected:\t" << expr_str << "\n"
            << "Source:\t\t" << file << ", line " << line << "\n";
        abort();
    }
}

template <typename Val>
std::string message(size_t i, const Val& val) {
    std::ostringstream iss;
    iss << "at value container[" << i << "] = " << val;
    return iss.str();
};

template <typename It>
void test_list(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using list_t = disk_block_list<typename It::value_type, decltype(alloc)>;

        uint32_t block = list_t::create(alloc);

        massert(list_t::empty(alloc, block), "not empty after creation");

        std::map<typename It::value_type, size_t> map;

        for(auto it = begin1; it != end1; ++it)
            ++map[*it];

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            list_t::push(alloc, block, *it);
            massert(list_t::exists(alloc, block, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(list_t::exists(alloc, block, *it),
                    message(i, *it).c_str());

        massert(list_t::size(alloc, block) == std::distance(begin1, end1),
                "do not have the right size after insertion");


        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            massert(list_t::remove(alloc, block, *it),
                    message(i, *it).c_str());
            --map[*it];

            if(map[*it] > 0)
                massert(list_t::exists(alloc, block, *it),
                        message(i, *it).c_str());
            else
                massert(!list_t::exists(alloc, block, *it),
                        message(i, *it).c_str());
        }

        massert(list_t::empty(alloc, block), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}


template <typename It>
void test_zlib_list(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using list_t = zlib_disk_block_list<typename It::value_type, decltype(alloc)>;

        uint32_t block = list_t::create(alloc);

        massert(list_t::empty(alloc, block), "not empty after creation");

        std::map<typename It::value_type, size_t> map;

        for(auto it = begin1; it != end1; ++it)
            ++map[*it];

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            list_t::push(alloc, block, *it);
            massert(list_t::exists(alloc, block, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(list_t::exists(alloc, block, *it),
                    message(i, *it).c_str());

        massert(list_t::size(alloc, block) == std::distance(begin1, end1),
                "do not have the right size after insertion");


        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            massert(list_t::remove(alloc, block, *it),
                    message(i, *it).c_str());
            --map[*it];

            if(map[*it] > 0)
                massert(list_t::exists(alloc, block, *it),
                        message(i, *it).c_str());
            else
                massert(!list_t::exists(alloc, block, *it),
                        message(i, *it).c_str());
        }

        massert(list_t::empty(alloc, block), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}

template <typename It>
void test_btreeset(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using set_t = disk_block_btreeset<typename It::value_type, decltype(alloc)>;

        uint32_t root = set_t::create(alloc);
        uint32_t leaf = root;

        massert(set_t::empty(alloc, leaf), "not empty after creation");

        std::map<typename It::value_type, size_t> map;

        for(auto it = begin1; it != end1; ++it)
            ++map[*it];

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            set_t::insert(alloc, root, *it);
            massert(set_t::exists(alloc, root, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
            massert(set_t::exists(alloc, root, *it),
                    message(i, *it).c_str());

        massert(set_t::size(alloc, leaf) == std::distance(begin1, end1),
                "do not have the right size after insertion");


        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            massert(set_t::remove(alloc, root, *it),
                    message(i, *it).c_str());
            --map[*it];

            if(map[*it] > 0)
                massert(set_t::exists(alloc, root, *it),
                        message(i, *it).c_str());
            else
                massert(!set_t::exists(alloc, root, *it),
                        message(i, *it).c_str());
        }

        massert(set_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}

template <typename It>
void test_zlib_btreeset(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using set_t = zlib_disk_block_btreeset<typename It::value_type, decltype(alloc)>;

        uint32_t root = set_t::create(alloc);
        uint32_t leaf = root;

        massert(set_t::empty(alloc, leaf), "not empty after creation");

        std::map<typename It::value_type, size_t> map;

        for(auto it = begin1; it != end1; ++it)
            ++map[*it];

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            set_t::insert(alloc, root, *it);
            massert(set_t::exists(alloc, root, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(set_t::exists(alloc, root, *it),
                    message(i, *it).c_str());

        massert(set_t::size(alloc, leaf) == std::distance(begin1, end1),
                "do not have the right size after insertion");


        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            massert(set_t::remove(alloc, root, *it),
                    message(i, *it).c_str());
            --map[*it];

            if(map[*it] > 0)
                massert(set_t::exists(alloc, root, *it),
                        message(i, *it).c_str());
            else
                massert(!set_t::exists(alloc, root, *it),
                        message(i, *it).c_str());
        }

        massert(set_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}

template <typename It>
void test_btreemap(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using map_t = disk_block_btreemap<uint32_t, typename It::value_type, decltype(alloc)>;

        uint32_t root = map_t::create(alloc);
        uint32_t leaf = root;

        massert(map_t::empty(alloc, leaf), "not empty after creation");

        std::map<uint32_t, typename It::value_type> map;

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            map_t::insert(alloc, root, it->v, *it);
            map.insert({it->v, *it});
            auto res = map_t::find(alloc, root, it->v);
            massert(res.has_value(), message(i, it->v).c_str());
            massert(*res == map[it->v], message(i, map[it->v]).c_str());
        }

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            auto res = map_t::find(alloc, root, it->v);
            massert(res.has_value(), message(i, it->v).c_str());
            massert(*res == map[it->v], message(i, map[it->v]).c_str());
        }

        massert(map_t::size(alloc, leaf) == map.size(),
                "do not have the right size after insertion");

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            auto mv = map.find(it->v);
            auto value = map_t::remove(alloc, root, it->v);

            if(mv != map.end())
            {
                massert(value.has_value(), message(i, it->v).c_str());
                massert(*value == mv->second, message(i, *it).c_str());
            }
            else
            {
                massert(!value.has_value(), message(i, it->v).c_str());
            }

            massert(!map_t::exists(alloc, root, it->v),
                    message(i, it->v).c_str());
            map.erase(it->v);
        }

        massert(map_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}


template <typename It>
void test_zlib_btreemap(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using map_t = zlib_disk_block_btreemap<uint32_t, typename It::value_type, decltype(alloc)>;

        uint32_t root = map_t::create(alloc);
        uint32_t leaf = root;

        massert(map_t::empty(alloc, leaf), "not empty after creation");

        std::map<uint32_t, typename It::value_type> map;

        /* std::cout << "inserting" << std::endl; */

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            /* std::cout << i << std::endl; */
            auto edge = *it;
            map_t::insert(alloc, root, edge.v, edge);
            map.insert({it->v, *it});
            auto res = map_t::find(alloc, root, it->v);
            massert(res.has_value(), message(i, it->v).c_str());
            massert(*res == map[it->v], message(i, map[it->v]).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            auto res = map_t::find(alloc, root, it->v);
            massert(res.has_value(), message(i, it->v).c_str());
            massert(*res == map[it->v], message(i, map[it->v]).c_str());
        }

        massert(map_t::size(alloc, leaf) == map.size(),
                "do not have the right size after insertion");


        /* std::cout << "deleting" << std::endl; */
        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            std::cout << i << std::endl;
            auto mv = map.find(it->v);
            auto key = it->v;
            auto value = map_t::remove(alloc, root, key);

            if(mv != map.end())
            {
                massert(value.has_value(), message(i, it->v).c_str());
                massert(*value == mv->second, message(i, *it).c_str());
            }
            else
            {
                massert(!value.has_value(), message(i, it->v).c_str());
            }

            massert(!map_t::exists(alloc, root, it->v),
                    message(i, it->v).c_str());
            map.erase(it->v);
        }

        massert(map_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}


template <typename It>
void test_unordered_multimap(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using mmap_t = disk_block_unordered_multimap<uint32_t, typename It::value_type, decltype(alloc)>;

        uint32_t root = mmap_t::create(alloc);
        uint32_t leaf = root;

        massert(mmap_t::empty(alloc, leaf), "not empty after creation");

        std::map<uint32_t, std::vector<typename It::value_type>> mmap;

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            mmap_t::insert(alloc, root, it->v, *it);
            mmap[it->v].push_back(*it);
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());

        massert(mmap_t::size(alloc, leaf) == std::distance(begin1, end1),
                "do not have the right size after insertion");

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            auto mmv = mmap.find(it->v);
            massert(mmap_t::remove(alloc, root, it->v, *it),
                    message(i, *it).c_str());

            if(mmv != mmap.end())
            {
                auto mv = std::find(mmv->second.begin(), mmv->second.end(),*it);
                size_t count = std::count(mmv->second.begin(),
                        mmv->second.end(), *it);

                if(count > 1)
                {
                    massert(mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }
                else
                {
                    massert(!mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }

                if(mv != mmv->second.end())
                {
                    mmv->second.erase(mv);
                    if(mmv->second.empty())
                        mmap.erase(mmv);
                }

            }
            else
            {
                massert(!mmap_t::exists(alloc, root, it->v, *it),
                        message(i, *it).c_str());
            }
        }

        massert(mmap_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}


template <typename It>
void test_zlib_unordered_multimap(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using mmap_t = zlib_disk_block_unordered_multimap<uint32_t, typename It::value_type, decltype(alloc)>;

        uint32_t root = mmap_t::create(alloc);
        uint32_t leaf = root;

        massert(mmap_t::empty(alloc, leaf), "not empty after creation");

        std::map<uint32_t, std::vector<typename It::value_type>> mmap;

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            mmap_t::insert(alloc, root, it->v, *it);
            mmap[it->v].push_back(*it);
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());

        massert(mmap_t::size(alloc, leaf) == std::distance(begin1, end1),
                "do not have the right size after insertion");

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            auto mmv = mmap.find(it->v);
            massert(mmap_t::remove(alloc, root, it->v, *it),
                    message(i, *it).c_str());

            if(mmv != mmap.end())
            {
                auto mv = std::find(mmv->second.begin(), mmv->second.end(), *it);
                size_t count = std::count(mmv->second.begin(), mmv->second.end(), *it);

                if(count > 1)
                {
                    massert(mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }
                else
                {
                    massert(!mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }

                if(mv != mmv->second.end())
                {
                    mmv->second.erase(mv);
                    if(mmv->second.empty())
                        mmap.erase(mmv);
                }

            }
            else
            {
                massert(!mmap_t::exists(alloc, root, it->v, *it),
                        message(i, *it).c_str());
            }
        }

        massert(mmap_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}


template <typename It>
void test_multimap(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using mmap_t = disk_block_multimap<uint32_t, typename It::value_type, decltype(alloc)>;

        uint32_t root = mmap_t::create(alloc);
        uint32_t leaf = root;

        massert(mmap_t::empty(alloc, leaf), "not empty after creation");

        std::map<uint32_t, std::vector<typename It::value_type>> mmap;

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            mmap_t::insert(alloc, root, it->v, *it);
            mmap[it->v].push_back(*it);
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());

        massert(mmap_t::size(alloc, leaf) == std::distance(begin1, end1),
                "do not have the right size after insertion");

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            auto mmv = mmap.find(it->v);

            if(mmv != mmap.end())
            {
                massert(mmap_t::remove(alloc, root, it->v, *it),
                        message(i, *it).c_str());

                auto mv = std::find(mmv->second.begin(), mmv->second.end(), *it);
                size_t count = std::count(mmv->second.begin(), mmv->second.end(), *it);

                if(count > 1)
                {
                    massert(mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }
                else
                {
                    massert(!mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }

                if(mv != mmv->second.end())
                {
                    mmv->second.erase(mv);
                    if(mmv->second.empty())
                        mmap.erase(mmv);
                }

            }
            else
            {
                massert(!mmap_t::remove(alloc, root, it->v, *it),
                        message(i, *it).c_str());
                massert(!mmap_t::exists(alloc, root, it->v, *it),
                        message(i, *it).c_str());
            }
        }

        massert(mmap_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}


template <typename It>
void test_zlib_multimap(It begin1, It end1, It begin2, It end2)
{
    std::filesystem::remove("testfile");
    {
        disk_block_allocator alloc("testfile", free_list);
        using mmap_t = zlib_disk_block_multimap<uint32_t, typename It::value_type, decltype(alloc)>;

        uint32_t root = mmap_t::create(alloc);
        uint32_t leaf = root;

        massert(mmap_t::empty(alloc, leaf), "not empty after creation");

        std::map<uint32_t, std::vector<typename It::value_type>> mmap;

        size_t i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
        {
            mmap_t::insert(alloc, root, it->v, *it);
            mmap[it->v].push_back(*it);
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());
        }

        i = 0;
        for(auto it = begin1; it != end1; ++it, ++i)
            massert(mmap_t::exists(alloc, root, it->v, *it),
                    message(i, *it).c_str());

        massert(mmap_t::size(alloc, leaf) == std::distance(begin1, end1),
                "do not have the right size after insertion");

        i = 0;
        for(auto it = begin2; it != end2; ++it, ++i)
        {
            auto mmv = mmap.find(it->v);
            massert(mmap_t::remove(alloc, root, it->v, *it),
                    message(i, *it).c_str());

            if(mmv != mmap.end())
            {
                auto mv = std::find(mmv->second.begin(), mmv->second.end(), *it);
                size_t count = std::count(mmv->second.begin(), mmv->second.end(), *it);

                if(count > 1)
                {
                    massert(mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }
                else
                {
                    massert(!mmap_t::exists(alloc, root, it->v, *it),
                            message(i, *it).c_str());
                }

                if(mv != mmv->second.end())
                {
                    mmv->second.erase(mv);
                    if(mmv->second.empty())
                        mmap.erase(mmv);
                }

            }
            else
            {
                massert(!mmap_t::exists(alloc, root, it->v, *it),
                        message(i, *it).c_str());
            }
        }

        massert(mmap_t::empty(alloc, leaf), "not empty after detetion");
    }
    std::filesystem::remove("testfile");
}



template <typename It>
void test_func(const std::string& label, void (*f)(It, It, It, It), It begin, It end)
{
    std::cout << "Testing " << label << " ("
        << std::distance(begin, end)
        << " = "
        << std::distance(begin, end) * sizeof(typename It::value_type)
        << " bytes)" << std::endl;

    std::random_device rng;
    std::mt19937 urng(rng());
    urng.seed(2019);

    std::vector<typename It::value_type> shuffled(begin, end);
    std::shuffle(shuffled.begin(), std::end(shuffled), urng);

    f(begin, end, shuffled.begin(), shuffled.end());
}

void test(size_t n)
{
    std::cout << "~~~~~~~~TESTING " << n << " NUMBERS~~~~~~~" << std::endl;

    test_func("block-list (edges)",
            &test_list,
            edges.begin(), edges.begin() + n/2);
    test_func("block-list (neighbors)",
            &test_list,
            neighbors.begin(), neighbors.begin() + n);

    test_func("zlib-block-list (edges)",
            &test_zlib_list,
            edges.begin(), edges.begin() + n/2);
    test_func("zlib-block-list (neighbors)",
            &test_zlib_list,
            neighbors.begin(), neighbors.begin() + n);

    std::cout << std::endl;

    test_func("block-btreeset (edges)",
            &test_btreeset,
            edges.begin(), edges.begin() + n/2);
    test_func("block-btreeset (neighbors)",
            &test_btreeset,
            neighbors.begin(), neighbors.begin() + n);

    test_func("zlib-block-btreeset (edges)",
            &test_zlib_btreeset,
            edges.begin(), edges.begin() + n/2);
    test_func("zlib-block-btreeset (neighbors)",
            &test_zlib_btreeset,
            neighbors.begin(), neighbors.begin() + n);

    std::cout << std::endl;

    test_func("block-btreemap (edges)",
            &test_btreemap,
            edges.begin(), edges.begin() + n/2);
    test_func("block-btreemap (neighbors)",
            &test_btreemap,
            neighbors.begin(), neighbors.begin() + n);

    test_func("zlib-block-btreemap (edges)",
            &test_zlib_btreemap,
            edges.begin(), edges.begin() + n/2);
    test_func("zlib-block-btreemap (neighbors)",
            &test_zlib_btreemap,
            neighbors.begin(), neighbors.begin() + n);

    std::cout << std::endl;

    test_func("block-unordered-multimap (edges)",
            &test_unordered_multimap,
            edges.begin(), edges.begin() + n/2);
    test_func("block-unordered-multimap (neighbors)",
            &test_unordered_multimap,
            neighbors.begin(), neighbors.begin() + n);

    test_func("zlib-block-unordered-multimap (edges)",
            &test_zlib_unordered_multimap,
            edges.begin(), edges.begin() + n/2);
    test_func("zlib-block-unordered-multimap (neighbors)",
            &test_zlib_unordered_multimap,
            neighbors.begin(), neighbors.begin() + n);

    std::cout << std::endl;

    test_func("block-multimap (edges)",
            &test_multimap,
            edges.begin(), edges.begin() + n/2);
    test_func("block-multimap (neighbors)",
            &test_multimap,
            neighbors.begin(), neighbors.begin() + n);

    test_func("zlib-block-multimap (edges)",
            &test_zlib_multimap,
            edges.begin(), edges.begin() + n/2);
    test_func("zlib-block-multimap (neighbors)",
            &test_zlib_multimap,
            neighbors.begin(), neighbors.begin() + n);

    std::cout << "\n\n" << std::endl;
}


int main ()
{
    std::cout << std::endl;

    test(500);
    test(1000);
    test(2000);
    test(4000);
    test(8000);
    test(16000);
    test(32000);
    test(64000);
    test(128000);
    test(256000);
    test(512000);
    test(1000000);
}
