#pragma once

#include <libgraph-processing/edge.hxx>
#include <vector>

extern std::vector<stg::edge> edges;
extern std::vector<stg::neighbor> neighbors;
