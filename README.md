# libgraph-processing - C++ library


This repository have basic data structures and algorithms for two softwares, [graph-indexer](https://bitbucket.org/luizufu/graph-indexer) and [graph-reader](https://bitbucket.org/luizufu/graph-reader).
Our main goal is to develop compact data structures for graph processing that spend minimal space while offering reasonable performance.
These structures should provide support for graphs with different characterists (direct/undirect, with weights, with timestamps, etc) in different environments (static, dynamic, streaming, etc).

Currently we have pre-alpha code with 3 basic disk-based graph structures: adjacency list, edge list, and edge grid.
Adjacency list maintain outgoing/ingoing neighbors indexed by source/target vertices, edge list simply stores a list of edges, and edge grid treats edges as two-dimentional points and split the space uniformly.
we also implemented basic compression support for them using zlib.
These structures are our baseline and we hope our novel data structures will be better in some way.
We will compare these structures running a very diverse set of algorithms implemented using sequential and parallel approaches to show pros/cons.



